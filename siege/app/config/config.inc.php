<?php
if ($_SERVER['SERVER_NAME'] == 'local.hnsport-stock.fr') {
    define('ENVIRONMENT', 'dev');
    define('DOMAINE', 'local.hnsport-stock.fr');

    define('DB_HOST_ADMIN', 'localhost');
    define('DB_USER_ADMIN', 'root');
    define('DB_PASS_ADMIN', '');
} else {
    define('ENVIRONMENT', 'prod');
    define('DOMAINE', 'siege-spass.hnsport.fr');

    define('DB_HOST_ADMIN', 'hnsportfxrspass.mysql.db');
    define('DB_USER_ADMIN', 'hnsportfxrspass');
    define('DB_PASS_ADMIN', 'Nico3f6a');
}


define('CLASS_DIR', 'classes' . DIRECTORY_SEPARATOR);
define('ENVIRONMENT_DEV', 'dev');
define('ENVIRONMENT_PROD', 'prod');

define('MODEL', 'model');
define('FILES_DIR', dirname($_SERVER['SCRIPT_FILENAME']) . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR);
define('LIB_DIR', 'libs' . DIRECTORY_SEPARATOR);
define('MODEL_DIR', 'model' . DIRECTORY_SEPARATOR);
define('MODEL_JSON', 'model/json' . DIRECTORY_SEPARATOR);
define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', 'http://');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
define('IMG_FILES', URL . 'dist/img/');
define('IMG_COMMUN_FILES', URL . 'dist/img/');
define('CSS_COMMUN_FILES', URL . 'dist/css/');
define('JS_COMMUN_FILES', URL . 'dist/js/');

define('DB_TYPE_ADMIN', 'mysql');
define('DB_NAME_ADMIN', 'hnsportfxrspass');
define('DB_CHARSET_ADMIN', 'utf8');


define('STATUT_COMMANDE_EN_ATTENTE_DE_PAIEMENT', '1');
define('STATUT_COMMANDE_A_COMMANDER', '2');
define('STATUT_COMMANDE_COMMANDEES', '3');
define('STATUT_COMMANDE_EXPEDIEES', '4');

define('DEBUG', false);                                         // Debug mode
define('PS_SHOP_PATH', 'http://boutique.hnsport.fr/ahbfc/');      // Root path of your PrestaShop store
define('PS_WS_AUTH_KEY', 'A8ED618GH128ZIBRX23I9F3D3JEE1JRF');   // Auth key (Get it in your Back Office)


function order_object_alpha(&$object, $column){
      uasort($object, function($a, $b) use ($column) {
            return strcmp($a->{$column}, $b->{$column});
      });
}

    error_reporting(0);
    ini_set("display_errors", 0);
if (ENVIRONMENT == ENVIRONMENT_DEV) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
/** Auto-loading classes * */
spl_autoload_register('autoload_pkp');

function autoload_pkp($class_name) {
    if (file_exists(APP . CLASS_DIR . $class_name . '.class.php')) $class_file = APP . CLASS_DIR . $class_name . '.class.php';
    elseif (file_exists(APP . LIB_DIR . $class_name . '.class.php')) $class_file = APP . LIB_DIR . $class_name . '.class.php';
    elseif (file_exists(APP . MODEL_DIR . lcfirst($class_name) . '.class.php')) $class_file = APP . MODEL_DIR . lcfirst($class_name) . '.class.php';

    if (isset($class_file)) require_once $class_file;
}
