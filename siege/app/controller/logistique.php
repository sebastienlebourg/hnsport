<?php


class Logistique extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {


        $cDroits = new Droit();
        if(!$cDroits->verifDroits("logistique")){
            header('Location: /');
            exit();
        }



        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Logistique', 'url' => 'logistique', 'active' => true
            )
        );

        $cPiscine = new Piscine();
        $cOrders = new Commandes();
        $articleModel = new Article();
        $produits = array();

        $piscines = $cPiscine->getAll();
        $orders = $cOrders->getAllOrdersByStatus(Commandes::STATUT_COMMANDE_A_COMMANDER);
        $i = 0;
        foreach($orders as $id_order => $order){
            $order_details = $cOrders->getProduitCommandeByCommandeId($order->id_commande);

            foreach($order_details as $key => $produit){
                $produits[$i]['reference'] = $produit->ref;
                $produits[$i]['nom'] = $produit->designation . " " . $produit->genre;
                $produits[$i]['quantite'] = $produit->qte;
                $produits[$i]['id_commande'] = $order->id_commande;
                $produits[$i]['salarie'] = $produit->salarie;
                $produits[$i]['taille'] = $produit->taille;
                $produits[$i]['piscine'] = $order->nom_piscine;
                $produits[$i]['id_article_commande'] = $produit->id_commande_produit;


                $produits[$i]['isCommandee'] = false;
                if($articleModel->hasCommandee($produit->id_commande_produit)){
                    $produits[$i]['isCommandee'] = true;
                }

                $i++;
            }
        }


        $this->assigns['active_page'] = 'LOGISTIQUE';
        $this->assigns['produits'] = $produits;
        $this->applyView('logistique/index', 'header', 'footer');
    }


    public function commande_article() {

        $cDroits = new Droit();
        if(!$cDroits->verifDroits("logistique")){
            header('Location: /');
            exit();
        }

        $quantite_commandee = $_POST["quantite"];
        $quantite = $_POST["quantite"];
        $designation = $_POST["designation"];
        $ref_article = $_POST["ref_article"];
        $ref_commande = $_POST["ref_commande"];
        $piscine = $_POST["piscine"];
        $taille = $_POST["taille"];
        $id_commande_produit = $_POST["id_commande_produit"];

        $articleModel = new Article();
        if($articleModel->hasCommandee($id_commande_produit)){
            echo "Ce produit a déjà été commandé."; die;
        }

        $articleModel->insert($id_commande_produit, $piscine, $ref_commande, $ref_article, $designation, $taille, $quantite, $quantite_commandee);



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // VERIFICATION DE LA COMMANDE SI COMPLETE OU PAS --> SI OUI ALORS ON PASSE LA COMMANDE A EN COURS DE PREPARATION,  SI NON ALORS ON LAISSE LA COMMANDE COMME ACTUELLEMENT //
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $cPiscine = new Piscine();
        $cOrders = new Commandes();
        $articleModel = new Article();

        $piscine = $cPiscine->getAll();
        $order = $cOrders->getProduitCommandeByCommandeId($ref_commande);
        $i = 0;
        $Completed = true;

        foreach($order as $key => $produit){
            if(!$articleModel->hasCommandee($produit->id_commande_produit)){
                $Completed = false;
            }

            $i++;
        }

        if($Completed){
            $cOrders->commandee($ref_commande);
        }

        echo "SUCCESS"; die;
    }

    public function test(){
        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $articleModel = new Article();

        $boutiques = $cBoutiques->getAll();
        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_A_COMMANDER);
        $i = 0;
        $Completed = true;
        foreach($orders as $id_order => $order_url){
            $order = $cOrders->getOrderByUrl($order_url);

            if($order['reference'] == "OSAZNRRWP"){
                foreach($order['details'] as $key => $produit){

                    if(!$articleModel->hasCommandee($key)){
                        $Completed = false;
                    }

                    $i++;
                }
                $my_order = $order;
            }
        }
        if($Completed && !empty($my_order)){
            $cOrders->commandee($my_order['id'], $boutiques[$my_order['id_shop']]['url'], $my_order['id_shop'], $my_order['reference']);
        }
    }

}
