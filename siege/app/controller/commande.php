<?php



class Commande extends Controller {

    public function __construct() {
        parent::__construct();


        $cDroits = new Droit();
        if(!$cDroits->verifDroits("commande")){
            header('Location: /');
            exit();
        }
    }

    public function a_commander() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cPiscine = new Piscine();
        $cOrders = new Commandes();
        $aOrder = array();
        $aDetailsOrder = array();
        $nbProduitTotal = 0;

        $orders = $cOrders->getAllOrdersByStatus(Commandes::STATUT_COMMANDE_A_COMMANDER);
        foreach($orders as $id_order => $order){
            $aOrder[] = $order;
            $detailsOrder = $cOrders->getProduitCommandeByCommandeId($order->id_commande);
            $aDetailsOrder[$order->id_commande] = $detailsOrder;
        }



        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'En attente de préparation';
        $this->assigns['piscines'] = $cPiscine->getAll();
        $this->assigns['orders'] = $aOrder;
        $this->assigns['ordersDetails'] = $aDetailsOrder;
        $this->applyView('commandes/a_commander', 'header', 'footer');
    }




    public function commandees() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cPiscine = new Piscine();
        $cOrders = new Commandes();
        $aOrder = array();
        $aDetailsOrder = array();
        $nbProduitTotal = 0;

        $orders = $cOrders->getAllOrdersByStatus(Commandes::STATUT_COMMANDE_COMMANDE_FOURNISSEUR_PASSEE);
        foreach($orders as $id_order => $order){
            $aOrder[] = $order;
            $detailsOrder = $cOrders->getProduitCommandeByCommandeId($order->id_commande);
            $aDetailsOrder[$order->id_commande] = $detailsOrder;
        }


        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'En cours de préparation';
        $this->assigns['piscines'] = $cPiscine->getAll();
        $this->assigns['orders'] = $aOrder;
        $this->assigns['ordersDetails'] = $aDetailsOrder;

        $cDroits = new Droit();
        $gestionnaire = true;
        if(!$cDroits->verifDroits("logistique")){
            $gestionnaire = false;
        }
        $this->assigns['gestionnaire'] = $gestionnaire;

        $this->applyView('commandes/commandees', 'header', 'footer');
    }



    public function expedition() {

        $id_commande = $_POST["id_commande"];

        $cOrders = new Commandes();
        
        $cOrders->expediee($id_commande);

        echo "SUCCESS"; die;
    }

    public function dispo() {

        $id_commande = $_POST["id_commande"];

        $cOrders = new CommandeDispo();
        $cOrders->insert($id_commande);

        echo "SUCCESS"; die;
    }


    public function expediees() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cPiscine = new Piscine();
        $cOrders = new Commandes();
        $aOrder = array();
        $aDetailsOrder = array();
        $nbProduitTotal = 0;

        $orders = $cOrders->getAllOrdersByStatus(Commandes::STATUT_COMMANDE_EXPEDIEE);
        foreach($orders as $id_order => $order){
            $aOrder[] = $order;
            $detailsOrder = $cOrders->getProduitCommandeByCommandeId($order->id_commande);
            $aDetailsOrder[$order->id_commande] = $detailsOrder;
        }



        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'Expediées';
        $this->assigns['piscines'] = $cPiscine->getAll();
        $this->assigns['orders'] = $aOrder;
        $this->assigns['ordersDetails'] = $aDetailsOrder;

        $this->applyView('commandes/expediees', 'header', 'footer');
    }

}
