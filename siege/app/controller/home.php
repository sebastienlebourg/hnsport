<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Home extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $_SESSION["one_boutique"] = "all";
        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Home', 'url' => 'home', 'active' => true
            )
        );


        $cPiscine = new Piscine();
        $cOrders = new Commandes();
        $cCommandeDispo = new CommandeDispo();
        $articleModel = new Article();

        $piscines = $cPiscine->getAll();

        $i = 0;
        $total = 0;
        foreach($piscines as $piscine){
            $i++;

            $orders = $cOrders->getCommandeByPiscineId($piscine->id_piscine);
            $totalPiscine = 0;
            foreach($orders as $id_order => $order){

                $orders_club[] = $order;
                $total += $order->total;
                $totalPiscine += $order->total;
            }

            $arrPiscine[$piscine->nom_piscine] = $totalPiscine;

        }

        arsort($arrPiscine);
        $max = current($arrPiscine);
        foreach($arrPiscine as $keyBoutique => $boutiqueOrders){
            $pourcentage = (100*$boutiqueOrders)/$max;
            $arrPiscine[$keyBoutique] = array('ca'=>$boutiqueOrders, 'pourcentage' =>round($pourcentage, 2));
        }
        $this->assigns['arrCA'] = $arrPiscine;


        $this->assigns['CA_TOTAL'] = $total;

        /*$orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_EN_ATTENTE_DE_PAIEMENT);
        $this->assigns['nbOrdersEnAttentePaiement'] = count($orders);
        
        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_COMMANDEES);
        $this->assigns['nbOrdersCommandees'] = count($orders);
        $ordersDispoEnMag = $orders;
        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_EXPEDIEES);
        $this->assigns['nbOrdersExpediees'] = count($orders);

        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_A_COMMANDER);
        $this->assigns['nbOrdersACommander'] = count($orders);

        $orders = $cCommandeDispo->getAllOrdersDispo($ordersDispoEnMag);
        $this->assigns['nbOrdersDispoEnMag'] = count($orders);*/



    

            $this->assigns['piscines'] = $cPiscine->getAll();
            /*$this->assigns['statuts'] = $cOrders->getAllStatut();*/
            $this->assigns['orders_club'] = $orders_club;




        $this->assigns['active_page'] = 'DASHBOARD';
        $this->applyView('home/index', 'header', 'footer');
    }


    public function getCATotal() {
        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $boutiques = $cBoutiques->getAll();

        $i = 0;
        foreach($boutiques as $boutique){
            $i++;
            
            if($_SESSION['one_boutique'] != "all" && $boutique['abv'] != $_SESSION['one_boutique']){
                continue;
            }
            
            $orders = $cBoutiques->getAllOrdersByBoutique($boutique['url']);
            $total = 0;
            foreach($orders as $id_order => $order_url){
                $order = $cOrders->getOrderByUrl($order_url);

                if($order['valid'] == '1'){
                    $total += $order['total_paid'];
                }
            }

            if($total > 0){
                $arrBoutique[] = $boutique['abv'] . ' ('.$total.' €)';
            }else{
                $arrBoutique[] = $boutique['abv'];
            }
            $arrCA[] = $total;

        }

        echo implode($arrBoutique, '|').'||'.implode($arrCA, '|');
        die;
    }
}
