<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Pinkpepper
 */
class modelJson {
    public $a_data;
    
    public function __construct($type_data) {
	$this->a_data = $this->getDatas($type_data);
    }
    
    public function getDatas($type_data) {
	if (!isset($this->a_data)) {
            if(file_exists(APP.MODEL_JSON.COMPTE."/data_".$type_data.".json"))
                $json_url = APP.MODEL_JSON.COMPTE."/data_".$type_data.".json";
            else
                $json_url = APP.MODEL_JSON.COMMUN."/data_".$type_data.".json";
               
            $json = file_get_contents($json_url);
            $data = json_decode($json, TRUE);
            
            return $data;
	}        
    }
        
}
