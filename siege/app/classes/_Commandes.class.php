﻿<?php

require_once('../app/libs/PSWebServiceLibrary.php');

class Commandes {


    public function getOrderByUrl($order_url) {

        try
        {
            $webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
            $CommandeDispo = new CommandeDispo();

            $opt['url'] = $order_url;
            $order = $webService->get($opt);

            $myOrder = array();
            $myOrder['id'] = (string) $order->order->id;
            $myOrder['url'] = $order_url;
            $myOrder['id_customer'] = (string) $order->order->id_customer;
            $myOrder['payment'] = (string) $order->order->payment;
            $myOrder['date_commande'] = (string) $order->order->date_add;
            $myOrder['id_shop'] = (string) $order->order->id_shop;
            $myOrder['total_paid'] = (string) $order->order->total_paid;
            $myOrder['reference'] = (string) $order->order->reference;
            $myOrder['statut'] = (string) $order->order->current_state;
            $myOrder['valid'] = (string) $order->order->valid;
            if($myOrder['statut'] == STATUT_COMMANDE_EXPEDIEES){
                $myOrder['isDispo'] = false;
            }else{
                $myOrder['isDispo'] = $CommandeDispo->orderIsDispo($myOrder['id']);
            }
            $myOrder['nb_produits'] = 0;


            $opt['url'] = str_replace("orders/".$myOrder['id'], "customers/".$myOrder['id_customer'], $order_url);
            $customer = $webService->get($opt);
            $opt['url'] = str_replace("orders/".$myOrder['id'], "addresses/".(string) $order->order->id_address_delivery, $order_url);
            $phone = $webService->get($opt);
            $myOrder['customer'] = (string) $customer->customer->lastname . ' ' . (string) $customer->customer->firstname . '<br/>' . (string) $customer->customer->email;
            if(!empty((string) $phone->address->phone)){
                $myOrder['customer'] .= '<br/>' . (string) $phone->address->phone;
            }
            if(!empty((string) $phone->address->phone_mobile) && (string) $phone->address->phone_mobile != (string) $phone->address->phone){
                $myOrder['customer'] .= '<br/>' . (string) $phone->address->phone_mobile;
            }

            foreach($order->order->associations->order_rows->order_row as $order_row){
                $myOrder['details'][(string) $order_row->id]['product_id'] = (string) $order_row->product_id;
                $myOrder['details'][(string) $order_row->id]['product_name'] = (string) $order_row->product_name;
                $myOrder['details'][(string) $order_row->id]['product_price'] = (string) $order_row->product_price;
                $myOrder['details'][(string) $order_row->id]['product_quantity'] = (string) $order_row->product_quantity;
                $myOrder['details'][(string) $order_row->id]['product_reference'] = (string) $order_row->product_reference;
                $myOrder['nb_produits'] += (string) $order_row->product_quantity;
            }


        }
        catch (PrestaShopWebserviceException $e)
        {
            // Here we are dealing with errors
            $trace = $e->getTrace();
            if ($trace[0]['args'][0] == 404) echo 'Bad ID';
            else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
            else echo 'Other error';
        }

        return $myOrder;
    }


    public function getAllStatut() {

        $aStatuts = array();
        try
        {
            $webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);

            $opt['resource'] = "order_states";
            $state = $webService->get($opt);

            foreach($state->order_states->order_state as $order_state){

                $opt['resource'] = "order_states/".(string) $order_state['id'];
                $state = $webService->get($opt);

                foreach($state->order_state as $one_state){
                    $aStatuts[(string) $one_state->id] = (string) $one_state->name->language;
                }

            }
        }
        catch (PrestaShopWebserviceException $e)
        {
            // Here we are dealing with errors
            $trace = $e->getTrace();
            if ($trace[0]['args'][0] == 404) echo 'Bad ID';
            else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
            else echo 'Other error';
        }

        return $aStatuts;
    }

    public function getAllOrdersDispo($orders){

            foreach($orders as $id_order => $order_url){
                
            }
    }

    



    public function payee($id_commande, $boutique, $id_boutique, $ref_commande) {

        $url = $boutique."cron/updateOrderStatus.php?id_commande=".$id_commande."&id_boutique=".$id_boutique."&id_order_status=2";
        file_get_contents($url);

        return true;

    }


    public function commandee($id_commande, $boutique, $id_boutique, $ref_commande) {

        $url = $boutique."cron/updateOrderStatus.php?id_commande=".$id_commande."&id_boutique=".$id_boutique."&id_order_status=3";
        file_get_contents($url);

        return true;

    }

    public function expediee($id_commande, $boutique, $id_boutique, $ref_commande) {

        $url = $boutique."cron/updateOrderStatus.php?id_commande=".$id_commande."&id_boutique=".$id_boutique."&id_order_status=4";
        file_get_contents($url);

        return true;

    }

    public function livree($id_commande, $boutique, $id_boutique, $ref_commande) {

        $url = $boutique."cron/updateOrderStatus.php?id_commande=".$id_commande."&id_boutique=".$id_boutique."&id_order_status=5";
        file_get_contents($url);

        return true;

    }


}
