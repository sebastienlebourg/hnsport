<div class="row">

    <div class="col-md-12">
        <div class="card ">
            <div class="content">
                    <table id="datatable_logistique" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Salarie</th>
                                <th>Référence</th>
                                <th>Nom</th>
                                <th>Taille</th>
                                <th>Quantité</th>
                                <th>Réf Commande</th>
                                <th>piscine</th>
                                <th class="hidden">IdArticleCommande</th>
                                <th class="disabled-sorting text-center"></th>
                                <th class="hidden">Commande</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($produits as $produit) { ?>
                                <tr>
                                    <td><?=$produit['salarie']?></td>
                                    <td><?=$produit['reference']?></td>
                                    <td><?=$produit['nom']?></td>
                                    <td><?=$produit['taille']?></td>
                                    <td><?=$produit['quantite']?></td>
                                    <td><?=$produit['id_commande']?></td>
                                    <td><?=$produit['piscine']?></td>
                                    <td class="hidden"><?=$produit['id_article_commande']?></td>
                                    <td><?php if(!$produit['isCommandee']){ ?><center><button class="btn btn-success btn-sm btn-wd j_btn_commande_produit ">OK</button></center><?php } ?></td>
                                    <td class="hidden"><?=$produit['id_commande']?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                     </table>

                <div class="footer">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>