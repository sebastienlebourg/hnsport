
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <div class="fresh-datatables">
                    <table id="datatable_commande_a_commander" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>

                                <th>Référence</th>
                                <th>Piscine</th>
                                <th>Directeur</th>
                                <th>Date</th>
                                <th>Total</th>
                                <th>Nb Produits</th>
                                <th class="hidden">id</th>
                                <th class="disabled-sorting text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach($orders as $order) { ?>
                                <tr>
                                    <td><?=$order->id_commande?></td>
                                    <td><?=$order->nom_piscine?></td>
                                    <td><?=$order->utilisateur_name . ' ' . $order->utilisateur_firstname?></td>
                                    <td><?=$order->date?></td>
                                    <td><?=$order->total?> €</td>
                                    <td><?=$order->nbproduit?></td>
                                    <td class="hidden"><?=$order->id_commande?></td>
                                    <td><center><button class=" btn btn-success btn-sm btn-wd" data-toggle="modal" data-target="#detailCommande<?=$order->id_commande?>">Détails</button></center></td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->



<?php foreach($ordersDetails as $id_commande => $details) { ?>
    <!-- Modal -->
    <div class="modal fade" id="detailCommande<?=$id_commande?>" tabindex="-1" role="dialog" aria-labelledby="detailCommandeLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="detailCommandeLabel">Détail de la commande <?=$id_commande?></h4>
          </div>
          <div class="modal-body j_modal_body">

            <?php foreach($details as $detail) { ?>
                <?php echo $detail->qte; ?> X <?php echo $detail->designation . ', Taille: ' . $detail->taille . ', Sexe: ' . $detail->genre; ?> (<b><?php echo $detail->ref; ?></b>) pour le salarié: <?php echo $detail->salarie; ?><br/>
            <?php } ?>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Fermer</button>
          </div>
        </div>
      </div>
    </div>
<?php } ?>
