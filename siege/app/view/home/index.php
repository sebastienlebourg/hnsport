
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="content text-center">
                                        CHIFFRE D'AFFAIRE TOTAL: <?= $CA_TOTAL ?> €
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php if($_SESSION['one_boutique'] == "all"){ ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="content">
                                            <?php foreach($arrCA as $keyBoutique => $ca) { ?>

                                                <div class="row">
                                                    <div class="col-md-3"><?= $keyBoutique ?></div>
                                                    <div class="col-md-9">
                                                        <div class="progress" style="height: 20px;">
                                                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $ca['pourcentage'] ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?= $ca['pourcentage'] ?>%; color: black;"><?= $ca['ca'] ?> €</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
