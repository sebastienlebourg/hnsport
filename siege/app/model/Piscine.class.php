<?php

    /**
     * Description of User
     */
    class Piscine extends modelMySql {

        public function __construct() {
            $this->name = 'piscine';
            parent::__construct();
        }

        public function getPiscineById($id_piscine) {

            $sql = "SELECT * FROM piscine WHERE id_piscine = :id;";
            $query = $this->db_admin->prepare($sql);

            $query->execute(array(':id' => $id_piscine));

            return $query->fetch();
        }


    public function getAll() {

            $sql = "SELECT * FROM `piscine`;";

            $query = $this->db_admin->prepare($sql);
            $query->execute();
            return $query->fetchAll();
    }

}
