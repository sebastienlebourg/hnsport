<?php

session_start();

define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

require APP . 'config/config.inc.php';
require APP . 'core/application.php';
require APP . 'core/controller.php';

if(Helper::isLogin() && (!isset($_GET['url']) || (strpos($_GET['url'], 'login') !== false && strpos($_GET['url'], 'login/deconnexion') === false))){
    header('Location: /home');
    exit();
}

$app = new Application();
?>



