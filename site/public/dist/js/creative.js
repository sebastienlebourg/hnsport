/*!
 * Start Bootstrap - Creative Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Fit Text Plugin for Main Header
    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    // Initialize WOW.js Scrolling Animations
    new WOW().init();
	
	$('.actu_img img').each(function(){
		$(this).attr('style', 'display:none;');
	});		
	
	
	
	$( window ).scroll(function() {
		$('.actu_img').each(function(){
			
			var $t = $(this);
			var $w 				= $(window),
				viewTop			= $w.scrollTop(),
				viewBottom		= viewTop + $w.height(),
				_top			= $t.offset().top,
				_bottom			= _top + $t.height(),
				compareTop		= _bottom,
				compareBottom	= _top;
				
				console.log('WINDOW');
				console.log(viewTop);
				console.log(viewBottom);
				console.log($(this));
				console.log(_top);
				console.log(_bottom);
				
			if(!$(this).hasClass('j_visible_ok')){
				var visible = false;			
				
				
				visible = (_top != viewTop) && (_bottom != viewBottom) && (compareBottom <= viewBottom) && (compareTop >= viewTop);
	
				if(visible){
					$(this).find('img').fadeIn( 2500 );		
					$(this).addClass( 'j_visible_ok' );		
				}
			}
		});		
	});


})(jQuery); // End of use strict
