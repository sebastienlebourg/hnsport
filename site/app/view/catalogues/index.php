
    <header>
        <div class="header-content">
            <div class="header-content-inner">
            <br/>
                <h2 class='slogan'>CONSULTEZ NOS CATALOGUES</h2>
                    <hr class="light">
            </div>
        </div>
    </header>

    <section id="services">
        <div class="container">
            <div class="row">
                <a href="http://www.france-sport.fr/flip2017/download.php?mag=mag-fr-ss_px" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/francesport.jpg">
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="http://files.europeancatalog.fr/media/customer/outils-marketing/catalogues/2017/EC-FR-AP/files/assets/common/downloads/European%20Textile%20Catalogue%202017.pdf" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/toptex.jpg">
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="http://www.wobook.com/WBa98ST4Ae0_/CATALOGUE-TREMBLAY-2017.html" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/tremblay.png">
                            <p></p>
                        </div>
                    </div>
                </a>
            </div>
            <br/>
            <br/>

            <hr>
            
            <div class="row">
<a href="/public/dist/catalogue/CATALOGUE_hummel_TEAMSPORT_2018_2019_low_web.pdf" target="_blank">
<div class="col-lg-4 col-md-6 text-center">
<div class="service-box">
<img height="100px" src="/public/dist/img/hummel.jpg">
<p></p>
</div>
</div>
</a>


                <a href="/public/dist/catalogue/CATALOGUE-SAISON-2019.pdf" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/nike.jpg">
                            <p>NIKE 2019</p>
                        </div>
                    </div>
                </a>


                <a href="/public/dist/catalogue/CATALOGUE_JAKO_17_18.pdf" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/jako.jpg">
                            <p></p>
                        </div>
                    </div>
                </a>
            </div>
            <br/>

            <hr>
            
        </div>
    </section>
