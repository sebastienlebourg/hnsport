
    <header class="header-home">
        <div class="header-content">
            <div class="header-content-inner">
            <br/>
<!--<h2 class='slogan'>MATERIEL ET EQUIPEMENT<br/>POUR MANIFESTATIONS SPORTIVES</h2>-->
<h2 class='slogan'><br/></h2>
<!--<hr class="light">-->
<a href="/fff/" class="btn btn-primary btn-xl btn-fff-nike">Je suis intéressé(e)</a>
                <!--<a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>-->
            </div>
        </div>
    </header>

    <aside class="fais_toi_plaisir">
        <div class="container text-center">
            <div class="call-to-action" style="margin: 5% auto 0%;">
                <h2>Où chaque détail compte!</h2>
                <p class='hashtag'>#hnsport</p>
            </div>
        </div>
    </aside>
    
    <section class="bg-grey" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-center">
                    <h3 class="section-heading">Clubs sportifs</h3>
                    <hr class="light">
                    <p>
                        Nous travaillons directement avec les clubs sportifs qui équipent tout au long de l'année les sportifs du club à des prix imbattables.
                    </p>
                </div>
                <div class="col-md-4 text-center">
                    <h3 class="section-heading">Ecoles et collectivités</h3>
                    <hr class="light">
                    <p>
                        Ils nous font confiance et nous travaillons directement avec les collectivités et établissements scolaires à des prix compétitifs.
                    </p>
                </div>
                <div class="col-md-4 text-center">
                    <h3 class="section-heading">Entreprises</h3>
                    <hr class="light">
                    <p>
                        Un large choix d'objets et de supports de communication à vos couleurs. N'attendez-plus, devis totalement gratuit.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <aside class="bg-attachement">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>EQUIPEZ-VOUS A LA HAUTEUR DE VOS EVENEMENTS</h2>
                <hr class="violet">
                <p>HN Sport, vous assure une qualité d'équipements et d'accessoires à la hauteur de vos espérances !</p>
            </div>
        </div>
    </aside>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Nos marques</h2>
                    <hr class="dark">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <a href="/public/dist/catalogue/CATALOGUE_NIKE_TEAMSPORT_SAISON_17_18.pdf" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/nike.jpg">
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="/public/dist/catalogue/hummel_TEAMSPORT_2017_2018.pdf" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/hummel.jpg">
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="/public/dist/catalogue/CATALOGUE_JAKO_17_18.pdf" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100px" src="/public/dist/img/jako.jpg">
                            <p></p>
                        </div>
                    </div>
                </a>
            </div>
            <br/>
        </div>
    </section>
