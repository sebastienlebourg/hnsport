
    <header>
        <div class="header-content">
            <div class="header-content-inner">
            <br/>
                <h2 class='slogan'>UNE QUESTION, CONTACTEZ-NOUS</h2>
                    <hr class="light">
            </div>
        </div>
    </header>

    <section id="services">
        <div class="container">
            <div class="row">
                    <div class="col-lg-4 col-md-12 text-center">
                        <div class="service-box">
                            <h4>Rendez-nous visite:</h4>
                            <h3>83 bis Avenue des Canadiens<br/>76300 Sotteville les Rouen</h3>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <h4>Par mail en cliquant ci-dessous:</h4>
                            <a href="mailto:romain.hnsport@gmail.com, lucie.hnsport@gmail.com">
                                <img width="50px" src="/public/dist/img/mail.jpg">
                            </a>
                        </div>
                    </div>
 

                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <h4>Par téléphone au:</h4>
                            <h3>02 32 18 59 33</h3>
                        </div>
                    </div>

            </div>
        </div>
    </section>