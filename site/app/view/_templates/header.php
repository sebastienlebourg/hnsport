<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HN Sport - Équipementier sportif</title>
    <link rel="icon" type="image/png" href="<?= IMG_COMMUN_FILES ?>favicon.ico">

    <!-- Bootstrap core CSS     -->
    <link href="<?= CSS_COMMUN_FILES ?>bootstrap.min.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Animation library for notifications   -->
    <link href="<?= COMMUN_FILES ?>font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?= CSS_COMMUN_FILES ?>animate.min.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?= CSS_COMMUN_FILES ?>creative.css" rel="stylesheet" />

    <meta name="theme-color" content="#ffffff">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-103270683-1', 'auto');
      ga('send', 'pageview');

    </script>

    <script src='https://www.google.com/recaptcha/api.js'></script>


</head>

<body id="page-top">


    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/">
                    <img src="<?= IMG_COMMUN_FILES ?>header.png" style="
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 200px;
                    ">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <?php foreach ($pages as $p_title => $p_page) {
                        if (!empty($p_page['url'])) { ?>
                            <li>
                                <a <?= (isset($active_page) && $active_page == $p_title) ? 'class="active page-scroll"' : 'class="page-scroll"' ?> href = "<?= $p_page['url'] ?>">
                                    <?= ucfirst($p_title) ?>
                                </a>
                            </li>
                        <?php }
                    } ?>  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            <div class="navbar-social">
                <a href="https://www.facebook.com/hn.sport.90" target="_blank"><img src="<?= IMG_COMMUN_FILES ?>fb.png" width="30"><img class="hover" src="<?= IMG_COMMUN_FILES ?>fb_hover.png" width="30"></a>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>

