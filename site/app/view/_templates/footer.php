


</body>

    <footer class="text-center bg-grey" style="padding:10px 0;">
        Copyright <?php echo date('Y'); ?> &copy;  HN Sport - Réalisé par <a href="http://www.sl-creaweb.fr" target="_blank">SL Creaweb</a>
    </footer>

    <!-- jQuery -->
    <script src="<?=JS_COMMUN_FILES?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=JS_COMMUN_FILES?>bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?=JS_COMMUN_FILES?>jquery.easing.min.js"></script>
    <script src="<?=JS_COMMUN_FILES?>jquery.fittext.js"></script>
    <script src="<?=JS_COMMUN_FILES?>wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=JS_COMMUN_FILES?>creative.js"></script>

    <?php 
        if(!empty($js)){
            if(is_array($js) || is_object($js)){
                foreach($js as $j){
    ?>
    <script src="<?=JS_FILES.$j?>"></script>
    <?php
                }
            }else{ 
    ?>
    <script src="<?=JS_FILES.$js?>"></script>
    <?php
            }     
        }
    ?>

</html>
