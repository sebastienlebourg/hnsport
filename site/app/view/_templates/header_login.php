
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>HN Sport - CRM</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link rel="icon" type="image/png" href="<?= IMG_COMMUN_FILES ?>favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link href="<?= CSS_COMMUN_FILES ?>bootstrap.min.css" rel="stylesheet" />
        <link href="<?= CSS_COMMUN_FILES ?>animate.min.css" rel="stylesheet"/>
        <link href="<?= CSS_COMMUN_FILES ?>light-bootstrap-dashboard.css" rel="stylesheet"/>
        <link href="<?= CSS_COMMUN_FILES ?>hnsport.css" rel="stylesheet" />
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="<?= CSS_COMMUN_FILES ?>pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body class="login" >
    <div class=" container">