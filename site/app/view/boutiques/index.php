
    <header>
        <div class="header-content">
            <div class="header-content-inner">
            <br/>
                <h2 class='slogan'>DECOUVREZ LES BOUTIQUES DE NOS CLIENTS</h2>
                    <hr class="light">
            </div>
        </div>
    </header>

    <section id="services">
        <div class="container">
            <div class="row">
                <a href="https://boutique.hnsport.fr/ahbfc/" target="_blank">
                	<div class="col-lg-4 col-md-6 text-center">
	                    <div class="service-box">
	                        <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-de-l-amicale-houlmoise-bondevillaise-fc-logo-15023622364.jpg">
	                        <h4>AHBFC</h4>
	                        <p></p>
	                    </div>
	                </div>
            	</a>


                <a href="https://boutique.hnsport.fr/sgq/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-du-stade-de-grand-quevilly-logo-15005378265.jpg">
                            <h4>Stade de Grand Quevilly</h4>
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="https://boutique.hnsport.fr/esjanval/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-de-l-es-janval-logo-15040178506.jpg">
                            <h4>ES Janval</h4>
                            <p></p>
                        </div>
                    </div>
                </a>


            </div>
            <br/>
            <div class="row">
                <a href="https://boutique.hnsport.fr/esvo/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-de-l-entente-sportive-vallee-de-l-oison-logo-150399866910.jpg">
                            <h4>ESVO</h4>
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="https://boutique.hnsport.fr/fc-neufchatel/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-du-football-club-de-neufchatel-logo-150460548812.jpg">
                            <h4>FC NEUFCHATEL</h4>
                            <p></p>
                        </div>
                    </div>
                </a>


                <a href="https://boutique.hnsport.fr/fc-roumois-nord/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-du-fc-roumois-nord-logo-150710853813.jpg">
                            <h4>FC ROUMOIS NORD</h4>
                            <p></p>
                        </div>
                    </div>
                </a>


            </div>
            <br/>
            <div class="row">
                <a href="https://boutique.hnsport.fr/fa-roumois/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-du-fa-roumois-logo-150780323714.jpg">
                            <h4>FA ROUMOIS</h4>
                            <p></p>
                        </div>
                    </div>
                </a>

                <a href="https://boutique.hnsport.fr/assmb/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-de-l-as-saint-martin-de-boscherville-logo-15005378107.jpg">
                            <h4>ASSMB</h4>
                            <p></p>
                        </div>
                    </div>
                </a>



                <a href="https://boutique.hnsport.fr/asrucshop/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/asrucshop-logo-151091383018.jpg">
                            <h4>ASRUC SSE - ASRUC SHOP</h4>
                            <p></p>
                        </div>
                    </div>
                </a>





            </div>
            <br/>



            <div class="row">
                <a href="https://boutique.hnsport.fr/stade-sottevillais-76/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-du-stade-sottevillais-76-logo-151670379817.jpg">
                            <h4>Stade Sottevillais 76</h4>
                            <p></p>
                        </div>
                    </div>
                </a>

                <a href="https://boutique.hnsport.fr/us-gasny/" target="_blank">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="service-box">
                            <img height="100x" src="https://boutique.hnsport.fr/img/boutique-officielle-de-l-union-sportive-de-gasny-logo-150827968516.jpg">
                            <h4>US Gasny</h4>
                            <p></p>
                        </div>
                    </div>
                </a>




            </div>
            <br/>



        </div>
    </section>
