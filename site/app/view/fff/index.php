

    <section class="fff">
        <div class="container">
                <div class="row">
                        <div class="col-lg-12 col-md-12 text-center">
                            <h2>Renseignez vos coordonnées, nous vous recontacterons pour vous expliquer la démarche à suivre.</h2>
                        </div>

                </div>
            <form action="/fff/form" method="post">
                <div class="row">
                        <div class="col-lg-6 col-md-12 text-center">
                            <div class="service-box">
                                <h4>Nom du club</h4>
                                <input required type="text" name="club" class="form-control" >
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center">
                            <div class="service-box">
                                <h4>Numéro d'affiliation FFF</h4>
                                <input required type="text" name="affiliation" class="form-control" >
                            </div>
                        </div>

                </div>
                <div class="row">
                        <div class="col-lg-6 col-md-12 text-center">
                            <div class="service-box">
                                <h4>Nom</h4>
                                <input required type="text" name="nom" class="form-control" >
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center">
                            <div class="service-box">
                                <h4>Prénom</h4>
                                <input required type="text" name="prenom" class="form-control" >
                            </div>
                        </div>

                </div>
                <div class="row">
                        <div class="col-lg-6 col-md-12 text-center">
                            <div class="service-box">
                                <h4>Téléphone</h4>
                                <input required type="text" name="telephone" class="form-control" >
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center">
                            <div class="service-box">
                                <h4>Adresse mail</h4>
                                <input required type="email" name="mail" class="form-control" >
                            </div>
                        </div>

                </div>

                <div class="row">
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="service-box">
                                <div class="g-recaptcha" data-sitekey="6LfLbYwUAAAAALmKGNxrX20FpDjpnSfisgWBy_BI"></div>
                            </div>
                        </div>

                </div>

                <div class="row">
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="service-box">
                                <button style="width:100%;" type="submit" class="btn btn-primary btn-fff-nike btn-xl">Valider</button>
                            </div>
                        </div>

                </div>
            </form>

        </div>
    </section>