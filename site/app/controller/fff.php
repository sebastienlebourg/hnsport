<?php

class Fff extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'FFF / Nike', 'url' => 'fff', 'active' => true
            )
        );

        $this->applyView('fff/index', 'header', 'footer');
    }

    public function confirmation() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'FFF / Nike', 'url' => 'fff', 'active' => true
            )
        );

        $this->applyView('fff/confirmation', 'header', 'footer');
    }

    public function form() {


        // Ma clé privée
        $secret = "6LfLbYwUAAAAAKztIKix8hfbezvo3rPSA5ZDqh51";
        // Paramètre renvoyé par le recaptcha
        $response = $_POST['g-recaptcha-response'];
        // On récupère l'IP de l'utilisateur
        $remoteip = $_SERVER['REMOTE_ADDR'];
        
        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=" 
            . $secret
            . "&response=" . $response
            . "&remoteip=" . $remoteip ;
        
        $decode = json_decode(file_get_contents($api_url), true);
        
        if ($decode['success'] == true) {
            // C'est un humain
            $sql =  'INSERT INTO `clients`(`club`, `affiliation`, `nom`, `prenom`, `tel`, `mail`, `fff`) VALUES (
                    "'.$_POST["club"].'",
                    "'.$_POST["affiliation"].'",
                    "'.$_POST["nom"].'",
                    "'.$_POST["prenom"].'",
                    "'.$_POST["telephone"].'",
                    "'.$_POST["mail"].'",
                    "oui")';


            $serveur='hnsportfxrcrm.mysql.db'; // serveur
            $user='hnsportfxrcrm'; // nom d'utilisateur
            $password='Hnsport76'; // mot de passe
            $base='hnsportfxrcrm'; // nom de la base contenant les tables

            // Connexion à la base
            $conn = new mysqli($serveur, $user, $password);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
            $conn->select_db($base);

            $req = $conn->query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.$conn->mysqli_error());

            $message = "Club: \n ".$_POST["club"]."\n\n";
            $message .= "Affiliation: \n ".$_POST["affiliation"]."\n\n";
            $message .= "Nom: \n ".$_POST["nom"]."\n\n";
            $message .= "Prenom: \n ".$_POST["prenom"]."\n\n";
            $message .= "Telephone: \n ".$_POST["telephone"]."\n\n";
            $message .= "Email: \n ".$_POST["mail"];

            mail('romain.hnsport@gmail.com', 'Club interesse - Operation 2 etoiles FFF', $message);
            mail('lucie.hnsport@gmail.com', 'Club interesse - Operation 2 etoiles FFF', $message);
            mail('valentin.hnsport@gmail.com', 'Club interesse - Operation 2 etoiles FFF', $message);
            mail('michelnicolas76@hotmail.fr', 'Club interesse - Operation 2 etoiles FFF', $message);
            mail('sebastien@sl-creaweb.fr', 'Club interesse - Operation 2 etoiles FFF', $message);
                
            header("Location: /fff/confirmation");
            exit();

        }
        else {
            header("Location: /fff/home");
            exit();
        }

    }

}
