<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Boutiques extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Boutiques', 'url' => 'boutiques', 'active' => true
            )
        );

        $this->applyView('boutiques/index', 'header', 'footer');
    }

}
