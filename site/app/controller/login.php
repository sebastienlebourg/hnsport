<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Login extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function connexion() {
        /*         * **** RECUPERATION DES DONNEES ****** */
        $utilisateursModel = new Utilisateurs();
        $oUtilisateurs = $utilisateursModel->getAll();

        /*         * **** VERIFICATION DE LA CONNEXION AVEC LES FICHIERS PLATS ****** */
        foreach ($oUtilisateurs as $user) {

            if (empty($_POST['email_login']) || empty($_POST['password_login'])) die('ERROR');

            if ($user->utilisateur_email == $_POST['email_login'] && $user->utilisateur_password == md5($_POST['password_login'])) {
                $_SESSION['user_identifiant'] = $user->utilisateur_email;
                $_SESSION['user_login'] = $user->utilisateur_email;
                $_SESSION['user_password'] = $user->utilisateur_password;
                $_SESSION['user_nom'] = $user->utilisateur_name;
                $_SESSION['user_prenom'] = $user->utilisateur_firstname;
                $_SESSION['user_id'] = $user->utilisateur_id;

                //mail('sebastien@pinkpepper-studio.com', 'Active4Admin - Connexion', print_r($_SESSION, TRUE)."\n".print_r($_SERVER, TRUE));
                echo "SUCCESS";
                die;
            }
        }

        echo "ERROR";
        die;
    }

    public function deconnexion() {
        unset($_SESSION);
        session_destroy();
        header('Location: ' . URL);
        exit;
    }

    public function index() {

        $this->assigns['_menuVisible'] = false;
        $this->applyView('login/index', 'header_login', 'footer_login');
    }

}
