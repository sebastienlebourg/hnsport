<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Pinkpepper
 */
class modelMySql {

    protected $db_admin;
    protected $_arrAll = null;
    
    public $name;

    public function __construct() {
        if (!isset($this->name)) {
            $this->name = get_class($this);
        }
        try {
            $this->openDatabaseConnection(); //param
        } catch (Exception $ex) {
            echo $ex;
            if (ENVIRONMENT == ENVIRONMENT_DEV) {
                echo $ex;
            }
        }
    }

    private function openDatabaseConnection(/* param */) {
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        $this->db_admin = new PDO(DB_TYPE_ADMIN . ':host=' . DB_HOST_ADMIN . ';dbname=' . DB_NAME_ADMIN . ';charset=' . DB_CHARSET_ADMIN, DB_USER_ADMIN, DB_PASS_ADMIN, $options);
    }

    public function getAll() {
        if (empty($this->_arrAll)) {
            $sql = 'SELECT * FROM `' . $this->name . '`;';

            $query = $this->db_admin->prepare($sql);
            $query->execute();

            $this->_arrAll = $query->fetchAll();
        }
        return $this->_arrAll;
    }

    public function clearAll() {
        $this->_arrAll = null;
        return true;
    }

    public function getAllFromLike($field, $value) {

        $sql = 'SELECT * FROM `' . $this->name . '` WHERE ' . $field . ' LIKE "' . $value . '";';

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getOneFromLike($field, $value) {
        $sql = 'SELECT * FROM `' . $this->name . '` WHERE ' . $field . ' LIKE "' . $value . '" LIMIT 1;';

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return $query->fetch();
    }

}
