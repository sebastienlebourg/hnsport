<?php

class Application {

    /** @var null The controller */
    public $url_controller = null;
    public $url_controller_file = null;

    /** @var null The method (of the above controller), often also named "action" */
    public $url_action = null;

    /** @var array URL parameters */
    public $url_params = array();

    /**
     * "Start" the application:
     * Analyze the URL elements and calls the according controller/method or the fallback
     */
    public function __construct() {
        // create array with URL parts in $url
        $this->splitUrl();
        spl_autoload_register('autoload_pkp_domaine');
        
        require APP . 'libs' . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'EmailBuilder.class.php';
        //load db models
        require APP . 'core' . DIRECTORY_SEPARATOR . 'modelJson.php';
        require APP . 'core' . DIRECTORY_SEPARATOR . 'modelMySql.php';

        // check for controller: no controller given ? then load start-page
        if (!isset($this->url_controller_file) || !$this->url_controller_file) {
            require APP . 'controller' . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'home.php';
            require APP . 'controller' . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'home.php';
            $classe = 'Home' . ucfirst(COMPTE);
            $page = new $classe();
            $page->index();
        } elseif (file_exists(APP . 'controller' . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . $this->url_controller_file . '.php')) {

            // if so, then load this file and create this controller
            require APP . 'controller' . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . $this->url_controller_file . '.php';

            $this->url_controller = new $this->url_controller();

            if (method_exists($this->url_controller, $this->url_action)) {

                if (!empty($this->url_params)) {
                    // Call the method and pass arguments to it
                    call_user_func_array(array($this->url_controller, $this->url_action), $this->url_params);
                } else {
                    // If no parameters are given, just call the method without parameters, like $this->home->method();
                    $this->url_controller->{$this->url_action}();
                }
            } else {
                if (strlen($this->url_action) == 0) {
                    // no action defined: call the default index() method of a selected controller
                    $this->url_controller->index();
                } else {
                    header('location: ' . URL . 'home');
                }
            }
        } else {
            header('location: ' . URL . 'home');
        }
    }

    /**
     * Get and split the URL
     */
    private function splitUrl() {
        /** CHARGEMENT DE LA CONFIG CORRESPONDANT AU COMPTE COURANT * */
        require_once APP . 'config/config.inc.php';

        if(!isset($_GET['url'])){
            $_GET['url'] = "home";
        }
        
        if (isset($_GET['url'])) {

            /** SPLIT DE L'URL AFIN DE RECUPERER LA ROUTE * */
            $url = trim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            /** MISE EN PARAMETRE DE LA CLASSE LES DIFFERENTS ELEMENT DE L'URL * */
            $this->url_controller_file = isset($url[0]) ? $url[0] : null;
            $this->url_controller = isset($url[0]) ? $url[0] : null;
            $this->url_action = isset($url[1]) ? $url[1] : null;
            $this->url_get_params = isset($url[2]) ? $url[2] : null;
            $this->url_params = array_values($url);

            // Remove controller and action from the split URL
            unset($url[0], $url[1]);
        }
    }


}

function autoload_pkp_domaine($class_name) {

    if (file_exists(APP . CLASS_DIR . $class_name . '.class.php'))
        $class_file = APP . CLASS_DIR . $class_name . '.class.php';
    elseif (file_exists(APP . LIB_DIR  . $class_name . '.class.php'))
        $class_file = APP . LIB_DIR  . $class_name . '.class.php';
    elseif (file_exists(APP . CLASS_DIR . $class_name . '.class.php'))
        $class_file = APP . CLASS_DIR . $class_name . '.class.php';
    else if (file_exists(APP . MODEL_DIR . $class_name . '.class.php'))
        $class_file = APP . MODEL_DIR . $class_name . '.class.php';
    else if (file_exists(APP . MODEL_DIR . $class_name . '.class.php'))
        $class_file = APP . MODEL_DIR . $class_name . '.class.php';
    else if (file_exists(APP . CLASS_DIR .'email' .DIRECTORY_SEPARATOR. $class_name . '.class.php'))
        $class_file = APP . CLASS_DIR .'email' .DIRECTORY_SEPARATOR. $class_name . '.class.php';
    else if (file_exists(APP . CLASS_DIR .'import' .DIRECTORY_SEPARATOR. $class_name . '.class.php'))
        $class_file = APP . CLASS_DIR .'import' .DIRECTORY_SEPARATOR. $class_name . '.class.php';

    if (isset($class_file))
        require_once $class_file;
}
