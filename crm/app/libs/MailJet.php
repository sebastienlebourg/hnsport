<?php

/**
 * 
 */
class MailJet {

    const MAILJET_API_URL = 'https://api.mailjet.com/v3/';
    const FROM_EMAIL_DISCO = 'contact@discoverfrance.com';
    const FROM_NAME_DISCO = 'DiscoverFrance';
    const FROM_EMAIL_CYRPEO = 'services@cyrpeo.com';
    const FROM_NAME_CYRPEO = 'Cyrpeo';

    private $_accounts = array(
        'Contact' => 'contact@discoverfrance.com',
        'Support' => 'support@discoverfrance.com',
        'Chris' => 'chris@discoverfrance.com',
        'Maggie' => 'maggierose@discoverfrance.com',
        'David M.' => 'dmartin@discoverfrance.com',
        'David R.' => 'david@discoverfrance.com',
        'Rémi' => 'remi@discoverfrance.com',
        'Thomas' => 'thomas@discoverfrance.com',
    );
    private $_cc = array(
        'commande-clients@pinkpepper-studio.com',
    );

    public function send($email, $subject, $message, $files = array()) {

        $request = new RestRequest(self::MAILJET_API_URL, true);

        $subject = utf8_encode($subject);
        $message = utf8_encode($message);

        $dests = array();

        if (!empty($email)) {
            if (is_array($email)) {
                $dests = array_merge($email, $this->_cc);
            } else {
                $dests = $this->_cc;
                array_push($dests, $email);
            }
        } else {
            return false;
        }

        foreach ($dests as $k => $dest) {
            $dests[$k] = array('Email' => $dest);
        }

        if (COMPTE == 'disco') {
            $frommail = self::FROM_EMAIL_DISCO;
            $fromname = self::FROM_NAME_DISCO;
        } else if (COMPTE == 'cyrpeo') {
            $frommail = self::FROM_EMAIL_CYRPEO;
            $fromname = self::FROM_NAME_CYRPEO;
        }


        $mailjet = array(
            'FromEmail' => $frommail,
            'FromName' => $fromname,
            'Recipients' => $dests,
            'Subject' => $subject,
            'Text-part' => $message,
            'Html-part' => $message
        );

        if (!empty($files)) {
            $farr = array();
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            foreach ($files as $file) {
                $content = file_get_contents($file['path']);
                $attch = array(
                    'Content-type' => finfo_file($finfo, $file['path']),
                    'Filename' => $file['name'],
                    'content' => base64_encode($content),
                );

                array_push($farr, $attch);
            }
            finfo_close($finfo);

            $mailjet['Attachments'] = $farr;
        }


        $request->ressource('send')->method(RestRequest::POST);
        $request->headers(array('Content-Type' => 'application/json'));
        $request->auth('1dcac83b33f293728c07ed75fedf4a04:4cebc141b244e59c7f312a4268403999');
        $request->post($mailjet);

        $response = $request->execute();

        if (empty($response)) {
            mail('debug@pinkpepper-studio.com', 'Erreur envois mail', 'Request: ' . print_r($request, true) . ' CTNU: ' . print_r($mailjet, true) . ' Response: ' . print_r($response, true));

            return false;
        }

        //Copie du mail par PHP Mail pour ne pas fausser les stats mailjet
        // mail('debug@pinkpepper-studio.com', $subject, $message);

        return true;
    }

}
