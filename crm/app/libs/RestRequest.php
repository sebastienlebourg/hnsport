<?php

/**
 * RestRequest class. Able your PHP website to make RESTs request using CURL.
 * Compatible PKAPI.
 * 
 * @author Pinkpepper
 * @version 160205
 */
class RestRequest {

    const CREATE = 'PUT';
    const READ = 'GET';
    const UPDATE = 'PATCH';
    const DELETE = 'DELETE';
    const POST = 'POST';

    private $_apiurl = null;
    private $_verbose = false;
    private $_method = null;
    private $_ressource = null;
    private $_headers = null;
    private $_parameters = array();
    private $_content = array();
    private $_response_code = null;
    private $_response_body = null;
    private $_response_headers = null;
    private $_auth = null;

    public function __construct($url, $verbose = false) {
        $this->_apiurl = $url;
        $this->_verbose = $verbose;
    }

    /**
     * Select the ressource.
     * Eg: events, entries, or entry
     * Needed FIRST
     * 
     * @param string $res Name of the ressource
     * @return RestRequest This class in order to use a PHP Pattern
     */
    public function ressource($res) {
        $this->_response_headers = null;
        $this->_response_code = null;
        $this->_response_body = null;
        $this->_ressource = $res;
        return empty($this->_ressource) ? $this->_ressource = null : $this;
    }

    /**
     * Method to use for the request. Default is GET.
     * 
     * @param string $met Method
     * @return RestRequest This class in order to use a PHP Pattern
     */
    public function method($met = self::READ) {
        if ($met == self::CREATE || $met == self::READ || $met == self::UPDATE || $met == self::DELETE || $met == self::POST) {
            $this->_method = $met;
        }
        return empty($this->_ressource) ? null : $this;
    }

    /**
     * Method to use for the request. Default is GET.
     * 
     * @param string $met Method
     * @return RestRequest This class in order to use a PHP Pattern
     */
    public function auth($auth) {
        if (!empty($auth)) {
            $this->_auth = $auth;
        }
        return empty($this->_ressource) ? null : $this;
    }

    /**
     * Array containing the headers to send with the request
     * 
     * @param array $array K/V Array of headers
     * @return RestRequest This class in order to use a PHP Pattern
     */
    public function headers($array = array()) {
        $this->_headers = $array;
        return empty($this->_ressource) ? null : $this;
    }

    /**
     * Add GET parameters
     * 
     * @param array $array K/V array containing GET parameters
     * @return RestRequest This class in order to use a PHP Pattern
     */
    public function get($array = array()) {
        $this->_parameters = $array;
        return empty($this->_ressource) ? null : $this;
    }

    /**
     * Add POST contents
     * 
     * @param array $array K/V array containing POST parameters
     * @return RestRequest This class in order to use a PHP Pattern
     */
    public function post($array = array()) {
        $this->_content = $array;
        return empty($this->_ressource) ? null : $this;
    }

    /**
     * Execute the request and return the result
     * 
     * @return object REST Response
     */
    public function execute() {
        if (empty($this->_ressource)) {
            return null;
        }

        // Since method is facultative we use GET by default
        if (empty($this->_method)) {
            $this->_method = self::READ;
        }

        $url = $this->_ressource;

        // Manage GET parameters
        if (!empty($this->_parameters)) {
            $url .= '?';
            foreach ($this->_parameters as $key => $value) {
                $url .= urlencode($key) . '=' . urlencode($value) . '&';
            }
            $url = rtrim($url, '&');
        }

        // Initialize CURL
        $curl = curl_init($this->_apiurl . $url);

        // Manage POST content
        if ($this->_method != self::READ) {
            $curl_post_data = json_encode($this->_content);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        }

        // Manage headers
        if (!empty($this->_headers)) {
            $headarray = array();
            foreach ($this->_headers as $key => $value) {
                array_push($headarray, $key . ': ' . $value);
            }
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headarray);
        }

        if (!empty($this->_auth)) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($curl, CURLOPT_USERPWD, $this->_auth);
        }
        // Set CURL options
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_HTTP200ALIASES, (array) 400);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->_method);

        // Send request and get response
        $response = curl_exec($curl);
        if (empty($response)) {
            mail('debug@pinkpepper-studio.com', '[A4A][REST] Erreur Requete REST Externe', 'GET : ' . $url . ' POST : ' . $curl_post_data . ' Reponse : ' . $response);
        }
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $this->_response_headers = substr($response, 0, $header_size);
        $body = $response;
        $this->_response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        $this->_response_body = json_decode($response);

        // Return result object
        // Reset class
        $this->_method = null;
        $this->_ressource = null;
        $this->_headers = null;
        $this->_parameters = array();
        $this->_content = array();

        $result_value = null;
        if (!empty($body)) {
            $result_value = json_decode($body);
            if (!$this->_verbose) {
                if (!empty($result_value->error) || (!empty($result_value->status) && $result_value->status == 'None')) {
                    $result_value = new stdClass();
                    $result_value->status = 'ERROR';
                } else {
                    $result_value->status = 'SUCCESS';
                }
            }
        }
        return $result_value;
    }

    /**
     * Return the response code of the last request
     * 
     * @return integer Response code
     */
    public function getResponseCode() {
        return $this->_response_code;
    }

    /**
     * Return the response body
     * 
     * @return object response
     */
    public function getResponseBody() {
        return $this->_response_body;
    }

    /**
     * Return the response headers
     * 
     * @return object Response headers
     */
    public function getHeaders() {
        $headers = $this->_response_headers;
        $elems = explode(PHP_EOL, $headers);
        $parsed = array();
        foreach ($elems as $elem) {
            $kv = explode(':', $elem, 2);
            if (!empty($kv[0]) && $kv[0] != PHP_EOL) {
                $parsed[trim($kv[0])] = true;
                if (!empty($kv[1]) && $kv[1] != PHP_EOL) {
                    $parsed[trim($kv[0])] = trim($kv[1]);
                }
            }
        }

        return json_decode(json_encode($parsed));
    }

}
