<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Pinkpepper
 */
class Helper {

    static public function isPkpIp() {
        $PkpIP = array('46.218.36.250', '::1', '90.0.90.247'); //pour tester ajouter , 'son ip'
        return in_array($_SERVER['REMOTE_ADDR'], $PkpIP);
    }

    static public function isLogin() {
        if (isset($_SESSION['user_login']) && !empty($_SESSION['user_login'])) return true;
        else return false;
    }

    static public function getBreadCrumb($breadcrumb) {

        $out = '';
        if (isset($breadcrumb) && count($breadcrumb) > 0) {
            foreach ($breadcrumb as $entry) {
                if (!$entry['active']) $out .= '<a href="' . URL . $entry['url'] . '">' . $entry['label'] . '</a> > ';
                else $out .= $entry['label'] . ' > ';
            }

            $out = substr(trim($out), 0, -1);
        }else {
            $out .= '<a href="' . URL . 'home">Home</a>';
        }

        echo $out;
    }

    public static function str_lreplace($search, $replace, $subject) {
        $pos = strrpos($subject, $search);

        if ($pos !== false) {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }

    public static function removeAccents($str, $charset = 'utf-8') {
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }
    
    
    /**
     * debugPDO
     *
     * Shows the emulated SQL query in a PDO statement. What it does is just extremely simple, but powerful:
     * It combines the raw query and the placeholders. For sure not really perfect (as PDO is more complex than just
     * combining raw query and arguments), but it does the job.
     * 
     * @author Panique
     * @param string $raw_sql
     * @param array $parameters
     * @return string
     */
    public static function debugPDO($raw_sql, $parameters) {

        $keys = array();
        $values = $parameters;

        foreach ($parameters as $key => $value) {

            // check if named parameters (':param') or anonymous parameters ('?') are used
            if (is_string($key)) {
                $keys[] = '/' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }

            // bring parameter into human-readable format
            if (is_string($value)) {
                $values[$key] = "'" . $value . "'";
            } elseif (is_array($value)) {
                $values[$key] = implode(',', $value);
            } elseif (is_null($value)) {
                $values[$key] = 'NULL';
            }
        }

        /*
          echo "<br> [DEBUG] Keys:<pre>";
          print_r($keys);

          echo "\n[DEBUG] Values: ";
          print_r($values);
          echo "</pre>";
         */

        $raw_sql = preg_replace($keys, $values, $raw_sql, 1, $count);

        return $raw_sql;
    }

}
