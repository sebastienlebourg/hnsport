<?php


class Logistique extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {


        $cDroits = new Droit();
        if(!$cDroits->verifDroits("logistique")){
            header('Location: /');
            exit();
        }



        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Logistique', 'url' => 'logistique', 'active' => true
            )
        );

        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $articleModel = new Article();
        $produits = array();

        $boutiques = $cBoutiques->getAll();
        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_A_COMMANDER);
        $i = 0;
        foreach($orders as $id_order => $order_url){
            $order = $cOrders->getOrderByUrl($order_url);

            foreach($order['details'] as $key => $produit){
                $produits[$i]['reference'] = $produit['product_reference'];
                $produits[$i]['nom'] = $produit['product_name'];
                $produits[$i]['quantite'] = $produit['product_quantity'];
                $produits[$i]['commande'] = $order['reference'];
                $produits[$i]['customer'] = $order['customer'];
                $produits[$i]['boutique'] = $boutiques[$order['id_shop']]['abv'];
                $produits[$i]['id_boutique'] = $order['id_shop'];
                $produits[$i]['id_commande'] = $id_order;
                $produits[$i]['id_article_commande'] = $key;


                $produits[$i]['isCommandee'] = false;
                if($articleModel->hasCommandee($key)){
                    $produits[$i]['isCommandee'] = true;
                }

                $i++;
            }
        }


        $this->assigns['active_page'] = 'LOGISTIQUE';
        $this->assigns['produits'] = $produits;
        $this->applyView('logistique/index', 'header', 'footer');
    }


    public function commande_article() {

        $cDroits = new Droit();
        if(!$cDroits->verifDroits("logistique")){
            header('Location: /');
            exit();
        }


        $quantite_commandee = $_POST["quantite"];
        $quantite = $_POST["quantite"];
        $designation = $_POST["designation"];
        $ref_article = $_POST["ref_article"];
        $ref_commande = $_POST["ref_commande"];
        $boutique = $_POST["boutique"];
        $id_article_commande = $_POST["id_article_commande"];

        $articleModel = new Article();
        if($articleModel->hasCommandee($id_article_commande)){
            echo "Ce produit a déjà été commandé."; die;
        }

        $articleModel->insert($id_article_commande, $boutique, $ref_commande, $ref_article, $designation, $quantite, $quantite_commandee);



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // VERIFICATION DE LA COMMANDE SI COMPLETE OU PAS --> SI OUI ALORS ON PASSE LA COMMANDE A EN COURS DE PREPARATION,  SI NON ALORS ON LAISSE LA COMMANDE COMME ACTUELLEMENT //
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $articleModel = new Article();

        $boutiques = $cBoutiques->getAll();
        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_A_COMMANDER);
        $i = 0;
        $Completed = true;
        foreach($orders as $id_order => $order_url){
            $order = $cOrders->getOrderByUrl($order_url);

            if($order['reference'] == $_POST["ref_commande"]){
                foreach($order['details'] as $key => $produit){

                    if(!$articleModel->hasCommandee($key)){
                        $Completed = false;
                    }

                    $i++;
                }
                $my_order = $order;
            }
        }

        if($Completed){
            $cOrders->commandee($my_order['id'], $boutiques[$my_order['id_shop']]['url'], $my_order['id_shop'], $my_order['reference']);
        }

        echo "SUCCESS"; die;
    }

    public function test(){
        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $articleModel = new Article();

        $boutiques = $cBoutiques->getAll();
        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_A_COMMANDER);
        $i = 0;
        $Completed = true;
        foreach($orders as $id_order => $order_url){
            $order = $cOrders->getOrderByUrl($order_url);

            if($order['reference'] == "OSAZNRRWP"){
                foreach($order['details'] as $key => $produit){

                    if(!$articleModel->hasCommandee($key)){
                        $Completed = false;
                    }

                    $i++;
                }
                $my_order = $order;
            }
        }
        if($Completed && !empty($my_order)){
            $cOrders->commandee($my_order['id'], $boutiques[$my_order['id_shop']]['url'], $my_order['id_shop'], $my_order['reference']);
        }
    }

}
