<?php



class Commandes extends Controller {

    public function __construct() {
        parent::__construct();


        $cDroits = new Droit();
        if(!$cDroits->verifDroits("commande")){
            header('Location: /');
            exit();
        }
    }

    public function attente_paiement() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $aOrder = array();
        $nbProduitTotal = 0;

        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_EN_ATTENTE_DE_PAIEMENT);
        foreach($orders as $id_order => $order_url){
        	$order = $cOrders->getOrderByUrl($order_url);
            $aOrder[] = $order;
            $nbProduitTotal += $order['nb_produits'];
        }


        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'Attente paiement';
        $this->assigns['boutiques'] = $cBoutiques->getAll();
        $this->assigns['statuts'] = $cOrders->getAllStatut();
        $this->assigns['nbProduitTotal'] = $nbProduitTotal;
        $this->assigns['orders'] = $aOrder;
        $this->applyView('commandes/attente_paiement', 'header', 'footer');
    }



    public function paiement_commande() {

        $ref_commande = $_POST["ref_commande"];
        $id_boutique = $_POST["id_boutique"];
        $boutique = $_POST["boutique"];
        $id_commande = $_POST["id_commande"];

        $cOrders = new Commande();
        $cBoutiques = new Boutique();
        $boutiques = $cBoutiques->getAll();
        
        $cOrders->payee($id_commande, $boutiques[$id_boutique]['url'], $id_boutique, $ref_commande);

        echo "SUCCESS"; die;
    }



    public function a_commander() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $aOrder = array();
        $nbProduitTotal = 0;

        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_A_COMMANDER);
        foreach($orders as $id_order => $order_url){
        	$order = $cOrders->getOrderByUrl($order_url);
            $aOrder[] = $order;
            $nbProduitTotal += $order['nb_produits'];
        }


        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'A commander';
        $this->assigns['boutiques'] = $cBoutiques->getAll();
        $this->assigns['statuts'] = $cOrders->getAllStatut();
        $this->assigns['nbProduitTotal'] = $nbProduitTotal;
        $this->assigns['orders'] = $aOrder;
        $this->applyView('commandes/a_commander', 'header', 'footer');
    }




    public function commandees() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $aOrder = array();
        $nbProduitTotal = 0;

        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_COMMANDEES);
        foreach($orders as $id_order => $order_url){
            $order = $cOrders->getOrderByUrl($order_url);
            $aOrder[] = $order;
            $nbProduitTotal += $order['nb_produits'];
        }


        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'Commandées';
        $this->assigns['boutiques'] = $cBoutiques->getAll();
        $this->assigns['statuts'] = $cOrders->getAllStatut();
        $this->assigns['nbProduitTotal'] = $nbProduitTotal;
        $this->assigns['orders'] = $aOrder;
        $this->applyView('commandes/commandees', 'header', 'footer');
    }



    public function expedition() {

        $ref_commande = $_POST["ref_commande"];
        $id_boutique = $_POST["id_boutique"];
        $boutique = $_POST["boutique"];
        $id_commande = $_POST["id_commande"];

        $cOrders = new Commande();
        $cBoutiques = new Boutique();
        $boutiques = $cBoutiques->getAll();
        
        $cOrders->expediee($id_commande, $boutiques[$id_boutique]['url'], $id_boutique, $ref_commande);

        echo "SUCCESS"; die;
    }

    public function dispo() {

        $id_commande = $_POST["id_commande"];

        $cOrders = new CommandeDispo();
        $cOrders->insert($id_commande);

        echo "SUCCESS"; die;
    }


    public function expediees() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $cBoutiques = new Boutique();
        $cOrders = new Commande();
        $aOrder = array();
        $nbProduitTotal = 0;

        $orders = $cBoutiques->getAllOrdersByStatus(STATUT_COMMANDE_EXPEDIEES);
        foreach($orders as $id_order => $order_url){
            $order = $cOrders->getOrderByUrl($order_url);
            $aOrder[] = $order;
            $nbProduitTotal += $order['nb_produits'];
        }


        $this->assigns['active_page'] = 'COMMANDES';
        $this->assigns['active_sous_page'] = 'Expediées';
        $this->assigns['boutiques'] = $cBoutiques->getAll();
        $this->assigns['statuts'] = $cOrders->getAllStatut();
        $this->assigns['nbProduitTotal'] = $nbProduitTotal;
        $this->assigns['orders'] = $aOrder;
        $this->applyView('commandes/expediees', 'header', 'footer');
    }

}
