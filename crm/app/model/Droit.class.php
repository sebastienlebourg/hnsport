<?php

/**
 * Description of User
 *
 * @author Pinkpepper
 */

// SQL voir les droits SELECT * FROM `droits` LEFT JOIN `utilisateurs` ON `droits`.`utilisateur_id` = `utilisateurs`.`utilisateur_id` LEFT JOIN `permissions` ON  `droits`.`permission_id` = `permissions`.`permission_id` WHERE 1
class Droit extends modelMySql {

    public function __construct() {
        $this->name = 'roles';
        parent::__construct();
    }

    public function verifDroits($droit) {

        if(!empty($_SESSION['user_id'])){

            $sql = "SELECT ".$droit." AS `droit` FROM `roles` r, utilisateurs u WHERE `u`.`role` = r.role_id AND u.utilisateur_id = :uid;";

            $query = $this->db_admin->prepare($sql);
            $query->execute(array(
                ':uid' => $_SESSION['user_id']
            ));
            
            $result = $query->fetchAll();

            if (!empty($result) && !empty($result[0]->droit) && $result[0]->droit == '1'){
                return true;
            }
        }

        return false;
    }
}
