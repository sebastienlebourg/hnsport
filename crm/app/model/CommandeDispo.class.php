<?php

class CommandeDispo extends modelMySql {

        
    function __construct() {
        $this->name = 'commande_dispo';
        parent::__construct();
    }

    public function insert($id_order) {
            $sql = "INSERT INTO `commande_dispo`(`id_order`, `uid`) VALUES ('".$id_order."', '".$_SESSION['user_id']."')";

            $query = $this->db_admin->prepare($sql);
            $query->execute();

            return true;
    }

    public function orderIsDispo($id_order) {

        $sql = 'SELECT * FROM commande_dispo WHERE `id_order` = "'.$id_order.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        $total = $query->fetchAll();

        if(!empty($total)){
            return true;
        }

        return false;
    }


    public function getAllOrdersDispo($orders) {

        $ordersDispo = array();
        foreach($orders as $id_order => $order_url){
            if($this->orderIsDispo($id_order)){
                $ordersDispo[$id_order] = $order_url;
            }
        }

        return $ordersDispo;
    }


}
