<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>HN Sport Logistique</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?= CSS_COMMUN_FILES ?>bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?= CSS_COMMUN_FILES ?>light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?= CSS_COMMUN_FILES ?>hnsport.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?= CSS_COMMUN_FILES ?>pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="/public/dist/img/full-screen-image-3.jpg">
        <!--
            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        -->

        <div class="logo">
            <a href="http://www.hnsport.fr" class="logo-text">
                HN Sport
            </a>
        </div>
        <div class="logo logo-mini">
            <a href="http://www.hnsport.fr" class="logo-text">
                HN
            </a>
        </div>

        <div class="sidebar-wrapper">

            <div class="user">
                <div class="photo">
                    <?php if($_SESSION['one_boutique'] != "all"){ ?>
                        <img src="/public/dist/img/avatar-hnsport.png" />
                    <?php }else {  ?>
                        <img src="/public/dist/img/avatar-<?= $_SESSION['user_id'] ?>.png" />
                    <?php } ?>
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#" class="collapsed">
                        <?= ucfirst($_SESSION['user_prenom']) ?> <?= strtoupper($_SESSION['user_nom']) ?>
                    </a>
                </div>
            </div>

            <ul class="nav">


                <?php foreach ($pages as $p_title => $p_page) {
                    if (!empty($p_page['url'])) { ?>
                        <li <?= (isset($active_page) && $active_page == $p_title) ? 'class="active"' : '' ?>> 

                            <?php if (!isset($p_page['sous_pages'])) { ?>

                                <a href = "<?= $p_page['url'] ?>">
                                    <i class="<?= $p_page['icon'] ?>"></i>
                                    <p><?= ucfirst($p_title) ?></p>
                                </a>

                            <?php }else{ ?>  

                                <a data-toggle="collapse" href="#<?= $p_page['url'] ?>" <?= (isset($active_page) && $active_page == $p_title) ? 'aria-expanded="true"' : '' ?>>
                                    <i class="<?= $p_page['icon'] ?>"></i>
                                    <p><?= ucfirst($p_title) ?>
                                       <b class="caret"></b>
                                    </p>
                                </a>

                                <div class="collapse <?= (isset($active_page) && $active_page == $p_title) ? 'in' : '' ?>" id="<?= $p_page['url'] ?>">
                                    <ul class="nav">
                                        <?php foreach ($p_page['sous_pages'] as $sous_page_title => $sous_page) { ?>
                                            <li <?= (isset($active_sous_page) && $active_sous_page == $sous_page_title) ? 'class="active"' : '' ?>> <a href="<?= $sous_page['url'] ?>"><?= ucfirst($sous_page_title) ?></a></li>
                                        <?php } ?> 
                                    </ul>
                                </div>

                            <?php } ?>  

                        </li>
                    <?php }
                } ?>  


            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
                        <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                        <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?=$active_page?></a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Plus
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
                                <li>
                                    <a href="<?= URL ?>login/lock" class="text-danger">
                                        <i class="pe-7s-lock"></i>
                                        Vérouiller
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?= URL ?>login/deconnexion" class="text-danger">
                                        <i class="pe-7s-close-circle"></i>
                                        Deconnexion
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

