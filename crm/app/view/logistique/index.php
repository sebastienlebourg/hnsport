<div class="row">

    <div class="col-md-12">
        <div class="card ">
            <div class="content">
                    <table id="datatable_logistique" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Client</th>
                                <th>Référence</th>
                                <th>Nom</th>
                                <th>Quantité</th>
                                <th>Réf Commande</th>
                                <th>Boutique</th>
                                <th class="hidden">IdArticleCommande</th>
                                <th class="disabled-sorting text-center"></th>
                                <th class="hidden">Commande</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($produits as $produit) { ?>
                                <tr>
                                    <td><?=$produit['customer']?></td>
                                    <td><?=$produit['reference']?></td>
                                    <td><?=$produit['nom']?></td>
                                    <td><?=$produit['quantite']?></td>
                                    <td><a target="_blank" href="http://boutique.hnsport.fr/ahbfc/cron/getInvoiceOrder.php?id_boutique=<?=$produit['id_boutique']?>&id_commande=<?=$produit['id_commande']?>"><?=$produit['commande']?></a></td>
                                    <td><?=$produit['boutique']?></td>
                                    <td class="hidden"><?=$produit['id_article_commande']?></td>
                                    <td><?php if(!$produit['isCommandee']){ ?><center><button class="btn btn-success btn-sm btn-wd j_btn_commande_produit ">OK</button></center><?php } ?></td>
                                    <td class="hidden"><?=$produit['commande']?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                     </table>

                <div class="footer">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>