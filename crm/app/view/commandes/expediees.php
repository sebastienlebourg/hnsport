
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <div class="fresh-datatables">
                    <table id="datatable_commande_commandees" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>

                                <th>Client</th>
                                <th>Référence</th>
                                <th>Nb Produits</th>
                                <th>Date</th>
                                <th>Statut</th>
                                <th>Prix</th>
                                <th>Boutique</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach($orders as $order) { ?>
                                <tr>
                                    <td><?=$order['customer']?></td>
                                    <td><?=$order['reference']?></td>
                                    <td><?=$order['nb_produits']?></td>
                                    <td><?=$order['date_commande']?></td>
                                    <td><?=$statuts[$order['statut']]?></td>
                                    <td><?=$order['total_paid']?></td>
                                    <td><?=$boutiques[$order['id_shop']]['abv']?></td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->