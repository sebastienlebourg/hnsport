
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <div class="fresh-datatables">
                    <table id="datatable_commande_a_commander" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>

                                <th>Client</th>
                                <th>Référence</th>
                                <th>Nb Produits</th>
                                <th>Date</th>
                                <th>Statut</th>
                                <th>Prix</th>
                                <th>Boutique</th>
                                <th class="hidden">id</th>
                                <th class="hidden">id</th>
                                <th class="disabled-sorting text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach($orders as $order) { ?>
                                <tr>
                                    <td><?=$order['customer']?></td>
                                    <td><?=$order['reference']?></td>
                                    <td><?=$order['nb_produits']?></td>
                                    <td><?=$order['date_commande']?></td>
                                    <td><?=$statuts[$order['statut']]?></td>
                                    <td><?=$order['total_paid']?></td>
                                    <td><?=$boutiques[$order['id_shop']]['abv']?></td>
                                    <td class="hidden"><?=$order['id']?></td>
                                    <td class="hidden"><?=$order['id_shop']?></td>
                                    <td><center><button class="btn btn-success btn-sm btn-wd" data-toggle="modal" data-target="#detailCommande<?=$order['reference']?>">Détails</button></center></td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->



<?php foreach($orders as $order) { ?>
    <!-- Modal -->
    <div class="modal fade" id="detailCommande<?=$order['reference']?>" tabindex="-1" role="dialog" aria-labelledby="detailCommandeLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="detailCommandeLabel">Détail de la commande <?=$order['reference']?></h4>
          </div>
          <div class="modal-body j_modal_body">

            <?php foreach($order['details'] as $detail) { ?>
                <?php echo $detail['product_quantity']; ?> X <?php echo $detail['product_name']; ?> (<b><?php echo $detail['product_reference']; ?></b>)<br/>
            <?php } ?>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Fermer</button>
          </div>
        </div>
      </div>
    </div>
<?php } ?>
