  
<div class="error_modal modal fade j_error_modal" id="error_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">X</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="modal-body">
                        <!-- Project Details Go Here -->
                        <div class="form-group">
                            <p class="text-center j_error_message" id="error_message">Erreur inconnue !</p>       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
