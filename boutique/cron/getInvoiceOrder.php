<?php
    ini_set('max_execution_time', 7200);

    include('../config/config.inc.php');

    Context::getContext()->shop->setContext(Shop::CONTEXT_SHOP, $_GET['id_boutique']);

    $order = new Order($_GET['id_commande']);
    $order_invoice_list = $order->getInvoicesCollection();

    $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, Context::getContext()->smarty);
    $mypdf = $pdf->render(true);

?>