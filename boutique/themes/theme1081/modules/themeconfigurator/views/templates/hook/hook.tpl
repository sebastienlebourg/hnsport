{assign var="hookName" value={$hook|escape:'htmlall':'UTF-8'}}
{if $page_name =='index'}
{if isset($htmlitems) && $htmlitems}

	{if $hookName == 'top'}
    	<div class="container">
            <div class="htmlcontent_top_box row">
                {foreach name=items from=$htmlitems item=hItem}
                    {if $smarty.foreach.items.index < 3}
                      <div class="top-banner-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-sm-4">
                            {if $hItem.url}
                            <a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
                            {/if}
                                <div class="banBox">
                                    {if $hItem.image}
                                        <div class="imgBox"><img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/></div>
                                    {/if}
                                    {if $hItem.title && $hItem.title_use == 1}
                                        <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                                    {/if}
                                    {if $hItem.html}
                                        <div class="item-html">
                                            {$hItem.html|escape:'UTF-8'}
                                        </div>
                                    {/if}
                                </div>
                            {if $hItem.url}
                                </a>
                            {/if}
                        </div>
                    {/if}
            	{/foreach}
            </div>
            <div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}">
                <ul class="htmlcontent-home clearfix  banner-ul row">
                {foreach name=items from=$htmlitems item=hItem}
                    {if $smarty.foreach.items.iteration > 3 &&  $smarty.foreach.items.iteration < 6}
                        <li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-sm-6">
                    {if $hItem.url}
                        <a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
                    {/if}
                        <div class="banBox">
                            {if $hItem.image}
                                <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
                            {/if}
                            {if $hItem.title && $hItem.title_use == 1}
                                <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                            {/if}
                            {if $hItem.html}
                                <div class="item-html">
                                    {$hItem.html|escape:'UTF-8'}
                                </div>
                            {/if}
                        </div>
                    {if $hItem.url}
                        </a>
                    {/if}
                </li>
                    {/if}
                {/foreach}
                </ul>
            </div>
            <div id="htmlcontent_logo">
                <ul class="htmlcontent-home clearfix logo-ul">
                {foreach name=items from=$htmlitems item=hItem}
                    {if $smarty.foreach.items.iteration > 5}
                        <li class="logo-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'}">
                    {if $hItem.url}
                        <a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
                    {/if}
                        <div class="banBox">
                            {if $hItem.image}
                                <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
                            {/if}
                            {if $hItem.title && $hItem.title_use == 1}
                                <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                            {/if}
                            {if $hItem.html}
                                <div class="item-html">
                                    {$hItem.html|escape:'UTF-8'}
                                </div>
                            {/if}
                        </div>
                    {if $hItem.url}
                        </a>
                    {/if}
                </li>
                    {/if}
                {/foreach}
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    {else}
    <div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}">
        <ul class="htmlcontent-home clearfix row">
            {foreach name=items from=$htmlitems item=hItem}
                <li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-3">
                <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                    {if $hItem.url}
                        <a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
                    {/if}
                        <div class="banBox">
                            {if $hItem.image}
                                <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
                            {/if}
                            {if $hItem.title && $hItem.title_use == 1}
                                <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                            {/if}
                            {if $hItem.html}
                                <div class="item-html">
                                    {$hItem.html|escape:'UTF-8'}
                                </div>
                            {/if}
                        </div>
                    {if $hItem.url}
                        </a>
                    {/if}
                </li>
            {/foreach}
        </ul>
    </div>
    {/if}

{/if}
{elseif ($hookName == 'footer')}
{if isset($htmlitems) && $htmlitems}
<div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}">
	<ul class="htmlcontent-home clearfix row">
		{foreach name=items from=$htmlitems item=hItem}
			<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-3">
				{if $hItem.url}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
				{/if}
                	<div class="banBox">
                        {if $hItem.image}
                            <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
                        {/if}
                        {if $hItem.title && $hItem.title_use == 1}
                            <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                        {/if}
                        {if $hItem.html}
                            <div class="item-html">
                                {$hItem.html|escape:'UTF-8'}
                            </div>
                        {/if}
                    </div>
				{if $hItem.url}
					</a>
				{/if}
			</li>
		{/foreach}
	</ul>
</div>
{/if}

{/if}