<?php



class Produit extends Controller {

    public function __construct() {
        parent::__construct();

    }

    public function index() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Produit', 'url' => 'produit', 'active' => true
            )
        );

        $Produits = new Produits();
        $Taille = new Taille();
        $idProduit = $_GET[0];

        $myProduct = $Produits->getById($idProduit);
        $myProduct->taille = $Taille->getTailleByProductId($idProduit);
        $this->assigns['produit'] = $myProduct;
        $this->applyView('produit/produit', 'header', 'footer');
    }



}
