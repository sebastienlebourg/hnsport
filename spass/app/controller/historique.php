<?php



class Historique extends Controller {

    public function __construct() {
        parent::__construct();

    }

    public function index() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Commandes', 'url' => 'commandes', 'active' => true
            )
        );

        $commandeModel = new Commandes();
        $produitModel = new Produits();
        $arrCommandes = $commandeModel->getCommandeByPiscineId($_SESSION["piscine"]->id_piscine);

        foreach($arrCommandes as $key=>$commande){
            $arrProduitCommande = $commandeModel->getProduitCommandeByCommandeId($commande->id_commande);

            $produitCommandeFinal = [];
            foreach($arrProduitCommande as $produit){
                $myProduit = $produitModel->getById($produit->id_produit);
                $produit->designation = $myProduit->designation;
                $produit->genre = $myProduit->genre;
                $produitCommandeFinal[$produit->type][$produit->salarie][] = $produit;
            }

            $arrCommandes[$key]->produits = $produitCommandeFinal;
        }

        $this->assigns['commandes'] = $arrCommandes;
        $this->applyView('commandes/history', 'header', 'footer');
    }

}
