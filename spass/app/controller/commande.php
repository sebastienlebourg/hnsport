<?php



class Commande extends Controller {

    public function __construct() {
        parent::__construct();

    }

    public function confirmation() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Panier', 'url' => 'panier', 'active' => true
            )
        );

        $commandeModel = new Commandes();
        $prixTotalPanier = 0;
        $nbproduit = 0;
        $directeur = $_SESSION['user_id'];
        $piscine = $_SESSION['piscine']->id_piscine;
        $statut = "en_attente_preparation";
        $idCommande = time() . '-' . $directeur;

        foreach($_SESSION["cart"] as $cart){
            $prixTotalPanier = $prixTotalPanier + $cart["prix"];
            foreach($cart["details"] as $details){
                $nbproduit = $nbproduit + $details["qte"];
            }
        }

        $commandeTrue = $commandeModel->insert($idCommande, $directeur, $piscine, $prixTotalPanier, $statut, $nbproduit);

        if($commandeTrue){

            $Produits = new Produits();
            foreach($_SESSION["cart"] as $cart){
                $type = $cart["type"];
                $salarie = $cart["salarie"];

                foreach($cart["details"] as $id_produit => $details){
                    $qte = $details["qte"];
                    $designation = $details["designation"];
                    $taille = $details["taille"];
                    $myProduct = $Produits->getById($id_produit);
                    $ref = $myProduct->ref;
                    $pu = $myProduct->prix;
                    $prix_total = $myProduct->prix * $qte;

                    $commandeModel->addProduit($idCommande, $id_produit, $ref, $pu, $prix_total, $type, $qte, $taille, $salarie);

                }
            }

            $enveloppe = $_SESSION['piscine']->enveloppe - $prixTotalPanier;
            $commandeModel->deductEnveloppe($piscine, $enveloppe);


        }else{
            header('Location: /panier');
            exit();
        }

        unset($_SESSION["cart"]);

        $this->applyView('commandes/confirmation', 'header', 'footer');
    }



}
