<?php



class Famille extends Controller {

    public function __construct() {
        parent::__construct();

    }

    public function liste() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Famille', 'url' => 'famille', 'active' => true
            )
        );


        $Produits = new Produits();
        $Taille = new Taille();
        $DOTATION_INITIAL = $Produits::DOTATION_INITIAL;
        $idFamille = $_GET[0];

        $arrSexeProducts = $DOTATION_INITIAL[$idFamille];
        $prixTotal["HOMME"] = 0;
        $prixTotal["FEMME"] = 0;

        foreach($arrSexeProducts as $sexe => $arrProduits){
            foreach($arrProduits as $id_product => $qte){
                $myProduct = $Produits->getById($id_product);
                $arrSexeProducts[$sexe][$id_product] = $myProduct;
                $arrSexeProducts[$sexe][$id_product]->qte = $qte;
                $arrSexeProducts[$sexe][$id_product]->taille = $Taille->getTailleByProductId($id_product);
                $prixTotal[$sexe] = $prixTotal[$sexe] + ($myProduct->prix * $myProduct->qte);

            }
        }

        $this->assigns['produits'] = $arrSexeProducts;
        $this->assigns['prixTotal'] = $prixTotal;
        if($_GET[1] == "initial"){
            $this->applyView('famille/liste_initial', 'header', 'footer');
        }else{
            $this->applyView('famille/liste_reassort', 'header', 'footer');
        }
    }



}
