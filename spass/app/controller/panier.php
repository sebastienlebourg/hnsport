<?php



class Panier extends Controller {

    public function __construct() {
        parent::__construct();

    }

    public function index() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Panier', 'url' => 'panier', 'active' => true
            )
        );

        $prixTotalPanier = 0;
        $produits = [];
        if(!empty($_SESSION["cart"])){

            foreach($_SESSION["cart"] as $cart){
                $prixTotalPanier = $prixTotalPanier + $cart["prix"];
            }   
            $produits = $_SESSION["cart"];
        }

        $this->assigns['prixTotalPanier'] = $prixTotalPanier;
        $this->assigns['produits'] = $produits;
        $this->applyView('panier/index', 'header', 'footer');
    }


    public function confirmation() {

        $this->assigns['breadcrumb'] = array(
            array(
                'label' => 'Panier', 'url' => 'panier', 'active' => true
            )
        );

        $prixTotalPanier = 0;
        foreach($_SESSION["cart"] as $cart){
            $prixTotalPanier = $prixTotalPanier + $cart["prix"];
        }


        $this->assigns['prixTotalPanier'] = $prixTotalPanier;
        $this->assigns['produits'] = $_SESSION["cart"];
        $this->applyView('panier/confirmation', 'header', 'footer');
    }

    public function addInitial() {

        if(!empty($_SESSION['cart'])){
            $panier = $_SESSION['cart'];
        }else{
            $_SESSION['cart'] = [];
            $panier = $_SESSION['cart'];
        }
        
        $addPanier = [];
        $addPanier['type'] = 'initial';
        if($_POST['sexe'] == 'HOMME'){
            $detailsProduits = $_POST["HOMME"];
            $addPanier['prix'] = $_POST['prixTotalHomme'];
        }else{
            $detailsProduits = $_POST["FEMME"];
            $addPanier['prix'] = $_POST['prixTotalFemme'];
        }

        $addPanier['salarie'] = $_POST["nom_salarie"];
        $addPanier['details'] = $detailsProduits;

        $_SESSION['cart'][] = $addPanier;



        $prixTotalPanier = 0;
        foreach($_SESSION["cart"] as $cart){
            $prixTotalPanier = $prixTotalPanier + $cart["prix"];
        }

        if($_SESSION['piscine']->enveloppe < $prixTotalPanier){
            unset($_SESSION["cart"]);
            $_SESSION['cart'] = [];
            echo "Enveloppe insuffisante";die;
        }



        echo "SUCCESS"; die;
    }

    public function addReassort() {

        if(!empty($_SESSION['cart'])){
            $panier = $_SESSION['cart'];
        }else{
            $_SESSION['cart'] = [];
            $panier = $_SESSION['cart'];
        }
        
        $addPanier = [];
        $addPanier['type'] = 'reassort';

        if($_POST['sexe'] == 'HOMME'){
            $detailsProduits = $_POST["HOMME"];
        }elseif($_POST['sexe'] == 'FEMME'){
            $detailsProduits = $_POST["FEMME"];
        }else{
            $detailsProduits = $_POST["UNISEXE"];
        }

        $addPanier['prix'] = $_POST['prix'];
        $addPanier['salarie'] = $_POST["nom_salarie"];
        $addPanier['details'] = $detailsProduits;

        $_SESSION['cart'][] = $addPanier;


        $prixTotalPanier = 0;
        foreach($_SESSION["cart"] as $cart){
            $prixTotalPanier = $prixTotalPanier + $cart["prix"];
        }

        if($_SESSION['piscine']->enveloppe < $prixTotalPanier){
            unset($_SESSION["cart"]);
            $_SESSION['cart'] = [];
            echo "Enveloppe insuffisante";die;
        }

        echo "SUCCESS"; die;
    }



    public function supprimer() {
    	unset($_SESSION['cart'][$_GET[0]]);
    	header("Location: /panier");
    	exit();
    }



}
