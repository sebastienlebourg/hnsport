<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Login extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function connexion() {
        /*         * **** RECUPERATION DES DONNEES ****** */
        $ClientModel = new Directeur();
        $piscineModel = new Piscine();
        $oClient = $ClientModel->getAll();

        /*         * **** VERIFICATION DE LA CONNEXION AVEC LES FICHIERS PLATS ****** */
        foreach ($oClient as $client) {
            if (empty($_POST['email_login']) || empty($_POST['password_login'])) die('ERROR');

            if ($client->piscine == $_POST['piscine_login'] && $client->utilisateur_email == $_POST['email_login'] && $client->utilisateur_password == md5($_POST['password_login'])) {
                $_SESSION['user_identifiant'] = $client->utilisateur_email;
                $_SESSION['user_login'] = $client->utilisateur_email;
                $_SESSION['user_password'] = $client->utilisateur_password;
                $_SESSION['user_nom'] = $client->utilisateur_name;
                $_SESSION['user_prenom'] = $client->utilisateur_firstname;
                $_SESSION['user_id'] = $client->id_directeur;
                $_SESSION['piscine'] = $piscineModel->getPiscineById($client->piscine);
                $_SESSION['cart'] = [];

                unset($_SESSION['lock']);

                echo "SUCCESS";
                die;
            }
        }

        echo "ERROR";
        die;
    }

    public function deconnexion() {
        unset($_SESSION);
        session_destroy();
        header('Location: ' . URL);
        exit;
    }

    public function lock() {
        $_SESSION['lock'] = true;
        $this->assigns['_menuVisible'] = false;
        $this->applyView('login/lock', 'header_login', 'footer_login');
    }

    public function index() {

        $this->assigns['_menuVisible'] = false;

        $piscineModel = new Piscine();
        $piscines = $piscineModel->getAll();
        $this->assigns['piscines'] = $piscines;
        $this->applyView('login/index', 'header_login', 'footer_login');
    }

}
