 <div class="row">

    <div class="col-md-12">
        <div class="card ">
            <div class="content">
                

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingTwo">
				      <h4 class="panel-title">
				          <center><b>Confirmation de la commande</b></center>
				      </h4>
				    </div>
				    <div class="panel" role="tabpanel" aria-labelledby="headingTwo">
				      <div class="panel-body">
				      <p>En validant cette commande, le prix de celle ci sera déduit de votre enveloppe.</p>
			      		<div class='footer'><hr></div>
				        
				      	<?php // FORMULAIRE DE SAISI DES PRODUITS

					        foreach($produits as $key => $cart){
					      		echo "<div class='ligne_panier'>";
						      		echo "<div class='dotation_panier'>";
							        	if($cart["type"] == "initial"){
							        		echo "Dotation Initial pour: " . $cart["salarie"];
							        	}else{
							        		echo "Réassort pour: " . $cart["salarie"];
							        	}
						        	echo "</div>";

						      		echo "<div class='designation_produit_panier'>";
								        foreach($cart["details"] as $idProduit => $details){
									      	echo "<div class='nom_produit_panier'>" . $details["qte"] . " * " . $details["designation"] . "  - Taille: " . $details["taille"] . "</div>";
								        }
						      		echo "</div>";
					        	echo "</div>";

					      		echo "<div class='puht_produit_panier'>Prix: ".$cart["prix"] . " €</div>";
					      		echo "<div class='footer'><hr></div>";
					        }
				      	?>

			      		<div class='prix_total_panier'><center>Prix Total: <?php echo $prixTotalPanier; ?> €</center></div>
			      		<div class='footer'><hr></div>
			      		<div class='prix_total_panier'><a href="/commande/confirmation" style="margin-left: 80%;" type="button" id="submit_initial_homme" class="btn btn-primary">Confirmer la commande</a></div>
			      		<div class='footer'><hr></div>
				      </div>
				    </div>
				  </div>
				</div>
                <div class="footer">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>