
            </div>
        </div>

        <footer class="footer">
            <div class="container">
        <hr>
                <div class="col-md-12">
                    <p class="copyright pull-right">
                        &copy; <?= date('Y'); ?> <a href="http://www.sl-creaweb.fr">SL Creaweb</a>
                    </p>
                </div>
            </div>
        </footer>


    </div>
</div>



</body>

    <script type="text/javascript">var URL_APP = "<?=URL?>";</script>

    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <script src="<?=JS_COMMUN_FILES?>jquery.min.js" type="text/javascript"></script>
    <script src="<?=JS_COMMUN_FILES?>jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?=JS_COMMUN_FILES?>bootstrap.min.js" type="text/javascript"></script>


    <!--  Forms Validations Plugin -->
    <script src="<?=JS_COMMUN_FILES?>jquery.validate.min.js"></script>

    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="<?=JS_COMMUN_FILES?>moment.min.js"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?=JS_COMMUN_FILES?>bootstrap-datetimepicker.js"></script>

    <!--  Select Picker Plugin -->
    <script src="<?=JS_COMMUN_FILES?>bootstrap-selectpicker.js"></script>

    <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
    <script src="<?=JS_COMMUN_FILES?>bootstrap-checkbox-radio-switch-tags.js"></script>

    <!--  Charts Plugin -->
    <script src="<?=JS_COMMUN_FILES?>chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?=JS_COMMUN_FILES?>bootstrap-notify.js"></script>

    <!-- Sweet Alert 2 plugin -->
    <script src="<?=JS_COMMUN_FILES?>sweetalert2.js"></script>

    <!-- Vector Map plugin -->
    <script src="<?=JS_COMMUN_FILES?>jquery-jvectormap.js"></script>

    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Wizard Plugin    -->
    <script src="<?=JS_COMMUN_FILES?>jquery.bootstrap.wizard.min.js"></script>

    <!--  Bootstrap Table Plugin    -->
    <script src="<?=JS_COMMUN_FILES?>bootstrap-table.js"></script>

    <!--  Plugin for DataTables.net  -->
    <script src="<?=JS_COMMUN_FILES?>jquery.datatables.js"></script>

    <!--  Full Calendar Plugin    -->
    <script src="<?=JS_COMMUN_FILES?>fullcalendar.min.js"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
    <script src="<?=JS_COMMUN_FILES?>light-bootstrap-dashboard.js"></script>

    <script src="<?=JS_COMMUN_FILES?>hnsport.js"></script>


    <?php 
        if(!empty($js)){
            if(is_array($js) || is_object($js)){
                foreach($js as $j){
    ?>
    <script src="<?=JS_FILES.$j?>"></script>
    <?php
                }
            }else{ 
    ?>
    <script src="<?=JS_FILES.$js?>"></script>
    <?php
            }     
        }
    ?>

</html>
