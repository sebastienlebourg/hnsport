
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Admin - SPASS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link href="<?= CSS_COMMUN_FILES ?>bootstrap.min.css" rel="stylesheet">
        <link href="<?= CSS_COMMUN_FILES ?>style.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121712683-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121712683-1');
</script>

    </head>
    <body>

        <div class="container">
            <header>
                <div class="col-md-12">
                    <div class="col-md-3 text-left">
                        <p class=""><a href="<?= URL ?>"><img width="100%" src="<?= IMG_FILES ?>logo.png" /></a></p>
                    </div>
                    <div class="col-md-9 text-center" style="padding: 30px 0px;">
                        <div class="col-md-4 text-center">
                            <p class=""><?= ucfirst($_SESSION['user_prenom']) ?> <?= strtoupper($_SESSION['user_nom']) ?> - <?= $_SESSION['piscine']->nom_piscine ?></p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p class="">Enveloppe restante: <?= $_SESSION['piscine']->enveloppe ?> €</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p class=""><a href="/login/deconnexion">Quitter</a></p>
                        </div>
                    </div>
                </div>
            </header  >
        </div>   
        <div class="container body-back">
            <hr>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        

                        <ul class="nav navbar-nav">
                            <?php
                            foreach ($pages as $p_title => $p_page) {

                                if (!empty($p_page['url'])) {

                                    if(!empty($p_page["sous_pages"])){
                                        ?>
                                            <li class="dropdown"> 
                                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?= ucfirst($p_title) ?> <b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <?php foreach ($p_page["sous_pages"] as $sous_pages_title => $p_sous_pages) { ?>
                                                        <li><a href="<?= $p_sous_pages['url'] ?>"><?= ucfirst($sous_pages_title) ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php
                                    }else{
                                        ?>
                                            <li <?= (isset($active_page) && $active_page == $p_title) ? 'class="active"' : '' ?>> 
                                                <a href = "<?= $p_page['url'] ?>" <?= isset($p_page['target']) ? 'target="' . $p_page['target'] . '"' : null; ?>>
                                                    <?= ucfirst($p_title) ?>
                                                </a>
                                            </li>
                                        <?php
                                    }
                                }
                            }
                            ?>  
                        </ul>
                    </div>
                </div>
            </nav>

                <hr>
