<div class="row">

    <div class="col-md-12">
        <div class="card ">
            <div class="content liste_initial">
                
            <form method="#" action="#" class="j_initial">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingOne">
				      <h4 class="panel-title">
				        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          <center><b>Nom du Salarié(e)</b></center>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				      <div class="panel-body">
				        <center><input type="text" required="required" class="form-control" style="width:30%;" name="nom_salarie"></center>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingOne">
				      <h4 class="panel-title">
				        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          <center><b>Choix du sexe</b></center>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				      <div class="panel-body">
				        <center><label style="width:40%;"><input class="j_choix_sexe_dotation_initial" type="radio" name="sexe" value="HOMME"/> HOMME</label>
				        <label style="width:40%;"><input class="j_choix_sexe_dotation_initial" type="radio" name="sexe" value="FEMME"/> FEMME</label></center>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default hidden j_panel_HOMME j_panel_dotation_initial">
				    <div class="panel-heading" role="tab" id="headingTwo">
				      <h4 class="panel-title">
				        <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
				          <center><b>Dotation Initial - HOMME</b></center>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
				      <div class="panel-body">
				        
				      	<?php // FORMULAIRE DE SAISI DES PRODUITS
					      	
					      	foreach($produits['HOMME'] as $produit){
					      		echo "<div class='img_produit'><img height='100%' src='/dist/img/produits/".$produit->id_produit . ".jpg'></div>";
					      		echo "<div class='designation_produit'><input type='hidden' name='HOMME[".$produit->id_produit."][qte]' value='".$produit->qte."'>".$produit->qte . " " . $produit->designation . "</div>";
					      		echo "<input type='hidden' name='HOMME[".$produit->id_produit."][designation]' value='".$produit->designation."'>";
					      		echo "<input type='hidden' name='prixTotalHomme' value='".$prixTotal['HOMME']."'>";
					      		
					      		echo "<div class='details_produit'>";
						      		echo "<div class='choix_taille'>Taille: ";
							      		echo "<select name='HOMME[".$produit->id_produit."][taille]'>";
							      			foreach($produit->taille as $taille){
							      				echo "<option>".$taille->taille."</option>";
						      				}
							      		echo "</select>";
						      		echo "</div>";
						      		echo "<div class='puht_produit'>Prix Unitaire: ".$produit->prix . " €</div>";
						      		echo "<div class='total_produit'>Prix Total: ".($produit->prix * $produit->qte) . " €</div>";
					      		echo "</div>";
					      		echo "<div class='footer'><hr></div>";
				      		}

				      	?>

				      	<center><button type="submit" id="submit_initial_homme" class="btn btn-primary j_submit_initial">Valider</button></center>

				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default hidden j_panel_FEMME j_panel_dotation_initial">
				    <div class="panel-heading" role="tab" id="headingThree">
				      <h4 class="panel-title">
				        <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
				          <center><b>Dotation Initial - FEMME</b></center>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
				      <div class="panel-body">
				        
				        
				      	<?php // FORMULAIRE DE SAISI DES PRODUITS
					      	
					      	foreach($produits['FEMME'] as $produit){
					      		echo "<div class='img_produit'><img height='100%' src='/dist/img/produits/".$produit->id_produit . ".jpg'></div>";
					      		echo "<div class='designation_produit'><input type='hidden' name='FEMME[".$produit->id_produit."][qte]' value='".$produit->qte."'>".$produit->qte . " " . $produit->designation . "</div>";
					      		echo "<input type='hidden' name='FEMME[".$produit->id_produit."][designation]' value='".$produit->designation."'>";
					      		echo "<input type='hidden' name='prixTotalFemme' value='".$prixTotal['FEMME']."'>";
					      		echo "<div class='details_produit'>";
						      		echo "<div class='choix_taille'>Taille: ";
							      		echo "<select name='FEMME[".$produit->id_produit."][taille]'>";
							      			foreach($produit->taille as $taille){
							      				echo "<option>".$taille->taille."</option>";
						      				}
							      		echo "</select>";
						      		echo "</div>";

						      		echo "<div class='puht_produit'>Prix Unitaire: ".$produit->prix . " €</div>";
						      		echo "<div class='total_produit'>Prix Total: ".($produit->prix * $produit->qte) . " €</div>";
					      		echo "</div>";
					      		echo "<div class='footer'><hr></div>";
				      		}

				      	?>

				      	<center><button type="submit" id="myButton" class="btn btn-primary j_submit_initial">Valider</button></center>

				      </div>
				    </div>
				  </div>
				</div>






                <div class="footer">
                    <hr>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>