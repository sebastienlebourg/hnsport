
<div class="wrapper wrapper-full-page">
    
    <div class="full-page lock-page" data-color="blue" data-image="/public/dist/img/full-screen-image-2.jpg">   
        <div class="content">
            <div class="container">
                <div class="row">                   
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="#" action="#" class="j_login">
                            
                        <!--   if you want to have the card without animation please remove the ".card-hidden" class   -->
                            <div class="card card-hidden">
                                <div class="header text-center">Login</div>
                                <div class="content">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" placeholder="Enter email" class="form-control" name="email_login" >
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" placeholder="Password" class="form-control" name="password_login" >
                                    </div>   
                                    <div class="form-group">
                                        <label>Piscine</label>
                                        <select name="piscine_login" class="form-control">
                                            <?php foreach($piscines as $piscine){
                                                echo "<option value='".$piscine->id_piscine."'>".$piscine->nom_piscine."</option>";
                                            } ?>
                                        </select>
                                    </div>    
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-primary btn-wd">Login</button>
                                </div>
                            </div>
                                
                        </form>
                                
                    </div>                    
                </div>

	<div class="row" style="
	    display: block;
	    width: 100%;
	    text-align: center;
	    margin:0;
	">    
	                		<a href="http://siege-spass.hnsport.fr/" style="
	    font-weight: bold;
	    font-size: 19px;
	    width: 100%;
	    text-align: center;
	    color: white;
	">Accès Siège</a>
                </div>

            </div>
        </div>
    </div>
</div>