
<div class="wrapper wrapper-full-page">
    
    <div class="full-page lock-page" data-color="blue" data-image="/public/dist/img/full-screen-image-2.jpg"> 
        
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
        <div class="content">
            <form method="#" action="#" class="form-login j_login">
                <div class="user-profile">        
                    <div class="author">
                        <img src="/public/dist/img/avatar-<?= $_SESSION['user_id'] ?>.png" />
                    </div> 
                    <h4><?= ucfirst($_SESSION['user_prenom']) ?> <?= strtoupper($_SESSION['user_nom']) ?></h4> 
                    <div class="form-group">
                        <input class="form-control" autocomplete="off" placeholder="E-mail" name="email_login" type="hidden" id="email_login" value="<?= $_SESSION['user_login'] ?>">
                        <input name="lock" type="hidden" value="1">
                        <input type="password" placeholder="Enter Password" class="form-control" name="password_login">
                    </div>
                    <button type="submit" class="btn btn-neutral btn-round btn-fil btn-wd">Dévérouiller</button>
                </div>
            </form>
        </div>
        
        <footer class="footer footer-transparent">
            <div class="container">
                <p class="copyright pull-right">
                    &copy; <?= date('Y'); ?> <a href="http://www.creative-tim.com">SL Creaweb</a>, fait avec amour pour un web meilleur
                </p>
            </div>
        </footer>
    </div> 
      
</div>