 <div class="row">

    <div class="col-md-12">
        <div class="card ">
            <div class="content">
                

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingTwo">
				      <h4 class="panel-title">
				          <center><b>Historique de commande</b></center>
				      </h4>
				    </div>
				    <div class="panel" role="tabpanel" aria-labelledby="headingTwo">
				      <div class="panel-body">


						<?php // FORMULAIRE DE SAISI DES PRODUITS
						
					        foreach($commandes as $key => $commande){
					    ?>

					        	<div class="panel-heading" role="tab" id="headingOne">
								  <h4 class="panel-title">
								    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $commande->id_commande ?>" aria-expanded="true" aria-controls="<?php echo $commande->id_commande ?>">
								      <center><b><?php echo "Commande du " . $commande->date . " ( " . $commande->total . " € )" ?></b></center>
								    </a>
								  </h4>
								</div>
								<div id="<?php echo $commande->id_commande ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								  <div class="panel-body">
								    	<?php 
									    	foreach($commande->produits as $type => $salaries){
						    					foreach($salaries as $nom_salarie => $detailsProduct){
										      		echo "<div class='ligne_panier'>";
											      		echo "<div class='dotation_panier'>";
								    						if($type == "initial"){
										        				echo "Dotation Initial pour: " . $nom_salarie;
							    							}else{
										        				echo "Réassort pour: " . $nom_salarie;

							    							}
								        				echo "</div>";


											      		echo "<div class='designation_produit_panier'>";
													        foreach($detailsProduct as $idProduit => $details){
														      	echo "<div class='nom_produit_panier'>" . $details->qte . " * " . $details->designation . "  - Taille: " . $details->taille . "</div>";
													        }
											      		echo "</div>";
										        	echo "</div>";
										        	echo "<br/><br/>";

					    						}
									        }

								    	?>
								  </div>
								</div>
					      		
					        <?php }
				      	?>
							
			      		<div class='footer'><hr></div>
				      </div>
				    </div>
				  </div>
				</div>
                <div class="footer">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>




