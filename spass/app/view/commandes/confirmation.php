 <div class="row">

    <div class="col-md-12">
        <div class="card ">
            <div class="content">
                

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                          <center><b>Récapitulatif de commande</b></center>
                      </h4>
                    </div>
                    <div class="panel" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                      <p>La commande a bien été prise en compte, vous pouvez consulter votre espace cmmande pour en avoir tout les détails.</p>
                      <p>La commande sera expédiée sous 15 jours sous réserve des disponibilités fournisseur.</p>
                        <div class='footer'><hr></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="footer">
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>