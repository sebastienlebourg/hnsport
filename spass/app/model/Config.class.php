<?php

/**
 * Description of User
 *
 * @author Pinkpepper
 */

// SQL voir les droits SELECT * FROM `droits` LEFT JOIN `utilisateurs` ON `droits`.`utilisateur_id` = `utilisateurs`.`utilisateur_id` LEFT JOIN `permissions` ON  `droits`.`permission_id` = `permissions`.`permission_id` WHERE 1
class Config extends modelMySql {

    public function __construct() {
        $this->name = 'config';
        parent::__construct();
    }

    public function getConfig() {
        $sql = "SELECT * FROM config;";

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        $_SESSION['config'] = $query->fetchAll();
        return $_SESSION['config'];
    }

    public function getOneConfig($config_key) {
        $sql = "SELECT * FROM config WHERE config_key = :config_key;";

        $query = $this->db_admin->prepare($sql);
        $query->execute(array(':config_key' => $config_key));
        return $query->fetch();
    }

}
