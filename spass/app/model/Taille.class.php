<?php

    /**
     * Description of User
     */
    class Taille extends modelMySql {

        

        public function __construct() {
            $this->name = 'taille';
            parent::__construct();
        }


        public function getAll() {

            if (empty($this->_arrAll)) {
                $sql = "SELECT * FROM `taille`";

                $query = $this->db_admin->prepare($sql);
                $query->execute();
                $this->_arrAll = $query->fetchAll();
            }

            return $this->_arrAll;
        }

        public function getTailleByProductId($id) {

            $sql = "SELECT * FROM taille WHERE id_produit = :id;";
            $query = $this->db_admin->prepare($sql);

            $query->execute(array(':id' => $id));
            $result = $query->fetchAll();

            return $result;
        }

}
