<?php

class Controller {

    /**
     * @var null Database Connection
     */
    public $db = null;

    /**
     * @var null Transaction
     */
    public $transaction = null;
    public $assigns = array();
    public $droits = null;

    /**
     * Whenever controller is created, open a database connection too and load "the model".
     */
    function __construct() {
        $this->assigns['pages'] = array();
        $this->assigns['pages']['Maitre Nageur'] = array(
            'url' => 'maitre-nageur', 
            'sous_pages' => array(
                'Dotation Initiale' => array(
                    'url' => URL . 'maitre-nageur/initial',
                ),
                'Réassort' => array(
                    'url' => URL . 'maitre-nageur/reassort',
                )
            )
        );
        $this->assigns['pages']['Entretien'] = array(
            'url' => 'entretien', 
            'sous_pages' => array(
                'Dotation Initiale' => array(
                    'url' => URL . 'entretien/initial',
                ),
                'Réassort' => array(
                    'url' => URL . 'entretien/reassort',
                )
            )
        );
        $this->assigns['pages']['Technique'] = array(
            'url' => 'technique', 
            'sous_pages' => array(
                'Dotation Initiale' => array(
                    'url' => URL . 'technique/initial',
                ),
                'Réassort' => array(
                    'url' => URL . 'technique/reassort',
                )
            )
        );
        $this->assigns['pages']['Coach'] = array(
            'url' => 'coach', 
            'sous_pages' => array(
                'Dotation Initiale' => array(
                    'url' => URL . 'coach/initial',
                ),
                'Réassort' => array(
                    'url' => URL . 'coach/reassort',
                )
            )
        );
        $this->assigns['pages']['Panier'] = array('url' => URL . 'panier', 'icon' => "pe-7s-note2");
        $this->assigns['pages']['Historique'] = array('url' => URL . 'historique', 'icon' => "pe-7s-drawer");

    }

    public function applyView($view, $header = "header", $footer = "footer") {
        foreach ($this->assigns as $key => $val) {
            global $$key;
            $$key = $val;
        }
        require APP . 'view/_templates/' . $header . '.php';
        require APP . 'view/' . $view . '.php';
        require APP . 'view/_templates/' . $footer . '.php';
    }

    public function applyAjaxView($view) {
        foreach ($this->assigns as $key => $val) {
            global $$key;
            $$key = $val;
        }
        require APP . 'view/' . $view . '.php';
    }

}
