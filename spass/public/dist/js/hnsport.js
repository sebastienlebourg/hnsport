
$(document).ready(function () {


    type = ['','info','success','warning','danger'];
    
    $('.j_login').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: URL_APP+"login/connexion",
            method: "POST",
            data: $('.j_login').serialize(),
            success: function (msg) {
                if ($.trim(msg) != "SUCCESS") {
                    alert("Erreur dans vos identifiants");
                } else {
                    document.location.href=URL_APP+"home";
                }
            }
        });
    });


    $('.j_choix_sexe_dotation_initial').on( 'change', function () {
        console.log($(this).attr("value"));
        $('.j_panel_dotation_initial').removeClass("show");
        $('.j_panel_dotation_initial').addClass("hidden");
        $('.j_panel_'+$(this).attr("value")).addClass("show");
        $('.j_panel_'+$(this).attr("value")).removeClass("hidden");



    });


    
    $('.j_initial').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: URL_APP+"panier/addInitial",
            method: "POST",
            data: $('.j_initial').serialize(),
            success: function (msg) {
                if ($.trim(msg) != "SUCCESS") {
                    alert(msg);
                } else {
                    document.location.href=URL_APP+"panier";
                }
            }
        });
    });

    
    $('.j_reassort').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: URL_APP+"panier/addReassort",
            method: "POST",
            data: $('.j_reassort').serialize(),
            success: function (msg) {
                if ($.trim(msg) != "SUCCESS") {
                    alert(msg);
                } else {
                    document.location.href=URL_APP+"panier";
                }
            }
        });
    });


    /****************************************************************************/
    /****************************************************************************/
    /************************* GESTION DES COMMANDES ****************************/
    /****************************************************************************/
    /****************************************************************************/


    /************************* EN ATTENTE DE PAIEMENT ****************************/
    $('#datatable_commande_attente_paiement').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "deferRender": true,
        responsive: true
    });


    var datatable_commande_attente_paiement = $('#datatable_commande_attente_paiement').DataTable();

    // Edit record
    datatable_commande_attente_paiement.on( 'click', '.j_btn_commande_payee', function () {
        var $myMask =   '<div id="mask">\
                                    Traitement de la commande en cours.<br />\
                                    Veuillez patienter...\
                                </div>\
                                <style media="screen">\
                                    #mask {\
                                        width:  100%;\
                                        height: 100%;\
                                        background: none rgba(0,0,0,1);\
                                        top:    0;\
                                        left:   0;\
                                        position: absolute;\
                                        text-align: center;\
                                        color: white;\
                                        font-size: 300%;\
                                        line-height: 150%;\
                                        padding: 100px;\
                                        z-index: 10000;\
                                    }\
                                </style>';

        $('body').append($myMask);
        $('body').css('overflow', 'hidden');

        $tr = $(this).closest('tr');
        var data = datatable_commande_attente_paiement.row($tr).data();
        var data_post = {};
            data_post.ref_commande = data[1];
            data_post.boutique = data[6];
            data_post.id_commande = data[7];
            data_post.id_boutique = data[8];

            JSON.stringify(data_post);

        $.ajax({
            url: URL_APP+"commandes/paiement_commande",
            method: "POST",
            data: data_post,
            success: function (msg) {

                if ($.trim(msg) != "SUCCESS") {
                    $("#mask").remove();
                    alert(msg);
                } else {
                    location.reload();
                }
            }
        });
    } );

    /****************************************************************************/


    /************************* EN ATTENTE DE PAIEMENT ****************************/
    $('#datatable_commande_a_commander').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "deferRender": true,
        responsive: true
    });


    var datatable_commande_a_commander = $('#datatable_commande_a_commander').DataTable();
    /****************************************************************************/





    /************************* EN ATTENTE DE PAIEMENT ****************************/
    $('#datatable_commande_commandees').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "deferRender": true,
        responsive: true
    });


    var datatable_commande_commandees = $('#datatable_commande_commandees').DataTable();



    // Edit record
    datatable_commande_commandees.on( 'click', '.j_btn_commande_dispo', function () {
        var $myMask =   '<div id="mask">\
                                    Traitement de la commande en cours.<br />\
                                    Veuillez patienter...\
                                </div>\
                                <style media="screen">\
                                    #mask {\
                                        width:  100%;\
                                        height: 100%;\
                                        background: none rgba(0,0,0,1);\
                                        top:    0;\
                                        left:   0;\
                                        position: absolute;\
                                        text-align: center;\
                                        color: white;\
                                        font-size: 300%;\
                                        line-height: 150%;\
                                        padding: 100px;\
                                        z-index: 10000;\
                                    }\
                                </style>';

        $('body').append($myMask);
        $('body').css('overflow', 'hidden');

        $tr = $(this).closest('tr');
        var data = datatable_commande_commandees.row($tr).data();
        var data_post = {};
            data_post.id_commande = data[7];

            JSON.stringify(data_post);

        $.ajax({
            url: URL_APP+"commandes/dispo",
            method: "POST",
            data: data_post,
            success: function (msg) {

                if ($.trim(msg) != "SUCCESS") {
                    $("#mask").remove();
                    alert(msg);
                } else {
                    location.reload();
                }
            }
        });
    } );


    // Edit record
    datatable_commande_commandees.on( 'click', '.j_btn_commande_expediee', function () {
        var $myMask =   '<div id="mask">\
                                    Traitement de la commande en cours.<br />\
                                    Veuillez patienter...\
                                </div>\
                                <style media="screen">\
                                    #mask {\
                                        width:  100%;\
                                        height: 100%;\
                                        background: none rgba(0,0,0,1);\
                                        top:    0;\
                                        left:   0;\
                                        position: absolute;\
                                        text-align: center;\
                                        color: white;\
                                        font-size: 300%;\
                                        line-height: 150%;\
                                        padding: 100px;\
                                        z-index: 10000;\
                                    }\
                                </style>';

        $('body').append($myMask);
        $('body').css('overflow', 'hidden');

        $tr = $(this).closest('tr');
        var data = datatable_commande_commandees.row($tr).data();
        var data_post = {};
            data_post.ref_commande = data[1];
            data_post.boutique = data[6];
            data_post.id_commande = data[7];
            data_post.id_boutique = data[8];

            JSON.stringify(data_post);

        $.ajax({
            url: URL_APP+"commandes/expedition",
            method: "POST",
            data: data_post,
            success: function (msg) {

                if ($.trim(msg) != "SUCCESS") {
                    $("#mask").remove();
                    alert(msg);
                } else {
                    location.reload();
                }
            }
        });
    } );

    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/





    /****************************************************************************/
    /****************************************************************************/
    /*********************** GESTION DE LA LOGISTIQUE ***************************/
    /****************************************************************************/
    /****************************************************************************/

    $('#datatable_logistique').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "deferRender": true,
        responsive: true,
        createdRow: function (row, data, index) {
                console.log(data);
            if (data[7] == "") {
                console.log(data);
                $(row).addClass("isCommandee");
            }
        }
    } );
    

    var datatable_logistique = $('#datatable_logistique').DataTable();

    // Edit record
    datatable_logistique.on( 'click', '.j_btn_commande_produit', function () {
        var $myMask =   '<div id="mask">\
                                    Traitement de la commande en cours.<br />\
                                    Veuillez patienter...\
                                </div>\
                                <style media="screen">\
                                    #mask {\
                                        width:  100%;\
                                        height: 100%;\
                                        background: none rgba(0,0,0,1);\
                                        top:    0;\
                                        left:   0;\
                                        position: absolute;\
                                        text-align: center;\
                                        color: white;\
                                        font-size: 300%;\
                                        line-height: 150%;\
                                        padding: 100px;\
                                        z-index: 10000;\
                                    }\
                                </style>';

        $('body').append($myMask);
        $('body').css('overflow', 'hidden');

        $tr = $(this).closest('tr');
        var data = datatable_logistique.row($tr).data();
        var data_post = {};
            data_post.ref_article = data[1];
            data_post.designation = data[2];
            data_post.quantite = data[3];
            data_post.ref_commande = data[8];
            data_post.boutique = data[5];
            data_post.id_article_commande = data[6];

            JSON.stringify(data_post);

        $.ajax({
            url: URL_APP+"logistique/commande_article",
            method: "POST",
            data: data_post,
            success: function (msg) {

                if ($.trim(msg) != "SUCCESS") {
                    $("#mask").remove();
                    alert(msg);
                } else {
                    location.reload();
                }
            }
        });

    } );

    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/
    /****************************************************************************/


    /************************* COMMANDES CLUB ACCUEIL ****************************/
    $('#datatable_commande_club').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "deferRender": true,
        responsive: true
    });


    var datatable_commande_club = $('#datatable_commande_club').DataTable();
    /****************************************************************************/

});
