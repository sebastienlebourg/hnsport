<?php

    //header('Location: http://www.hnsport.fr');
    //exit();
    
session_start();

define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

require APP . 'config/config.inc.php';
require APP . 'core/application.php';
require APP . 'core/controller.php';

if(isset($_SESSION['lock']) && strpos($_GET['url'], 'login/connexion') === false){
	//$_GET['url'] = 'login/lock';
}


unset($_SESSION['lock']);

if (!Helper::isLogin())
    if ((isset($_GET['url']) && strpos($_GET['url'], 'login/connexion') === false && strpos($_GET['url'], 'login/autoconnect') === false) || !isset($_GET['url']))
        $_GET['url'] = 'login';


if(Helper::isLogin() && (isset($_SESSION['lock']) && strpos($_GET['url'], 'login/connexion') === false) && (!isset($_GET['url']) || (strpos($_GET['url'], 'login') !== false && strpos($_GET['url'], 'login/lock') === false && strpos($_GET['url'], 'login/deconnexion') === false))){

    header('Location: /home');
    exit();
}

if(empty($_GET["url"])){
	$_GET['url'] = 'home';
}
$app = new Application();
?>



