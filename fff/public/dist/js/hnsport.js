
$(document).ready(function () {


    type = ['','info','success','warning','danger'];
    
    $('.j_login').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: URL_APP+"login/connexion",
            method: "POST",
            data: $('.j_login').serialize(),
            success: function (msg) {
                if ($.trim(msg) != "SUCCESS") {
                    console.log("ERROR");
                } else {
                    document.location.href=URL_APP+"home";
                }
            }
        });
    });


    $('.j_commercial').on('change', function (e) {
        e.preventDefault();

        var $myMask =   '<div id="mask">\
                            Traitement en cours.<br />\
                            Veuillez patienter...\
                        </div>\
                        <style media="screen">\
                            #mask {\
                                width:  100%;\
                                height: 100%;\
                                background: none rgba(0,0,0,1);\
                                top:    0;\
                                left:   0;\
                                position: absolute;\
                                text-align: center;\
                                color: white;\
                                font-size: 300%;\
                                line-height: 150%;\
                                padding: 100px;\
                                z-index: 10000;\
                            }\
                        </style>';

        $('body').append($myMask);
        $('body').css('overflow', 'hidden');

        $tr = $(this).closest('tr');


        var data_post = {};
            data_post.commercial = $(this).val();
            data_post.clientid = $(this).attr("data-attr-clientid");
            
            JSON.stringify(data_post);

        $.ajax({
            url: URL_APP+"home/changeCommercial",
            method: "POST",
            data: data_post,
            success: function (msg) {

                if ($.trim(msg) != "SUCCESS") {
                    $("#mask").remove();
                    alert(msg);
                } else {
                    location.reload();
                }
            }
        });
    });



});
