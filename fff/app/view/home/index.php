
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="content text-center">
                                        CLIENTS
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="content">
                                            <?php foreach($clients as $client) { ?>

                                                <div class="row">
                                                    <div class="col-md-3"><?= $client->club ?> (<?= $client->affiliation ?>)</div>
                                                    <div class="col-md-2"><?= $client->nom?> <?= $client->prenom ?></div>
                                                    <div class="col-md-2"><?= $client->tel ?></div>
                                                    <div class="col-md-3"><?= $client->mail ?></div>
                                                    <div class="col-md-2 text-right">
                                                        <select class="j_commercial" data-attr-clientid="<?= $client->idclient ?>" name="commercial">
                                                            <option value="">---</option>
                                                            <option <?php if($client->commercial == "LUCIE"){ echo "selected";} ?> value="LUCIE">LUCIE</option>
                                                            <option <?php if($client->commercial == "VAL"){ echo "selected";} ?> value="VAL">VAL</option>
                                                            <option <?php if($client->commercial == "NICO"){ echo "selected";} ?> value="NICO">NICO</option>
                                                            <option <?php if($client->commercial == "ROMAIN"){ echo "selected";} ?> value="ROMAIN">ROMAIN</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
