<?php

class Controller {

    /**
     * @var null Database Connection
     */
    public $db = null;

    /**
     * @var null Transaction
     */
    public $transaction = null;
    public $assigns = array();
    public $droits = null;

    /**
     * Whenever controller is created, open a database connection too and load "the model".
     */
    function __construct() {
        $this->assigns['pages'] = array();
        $this->assigns['pages']['STATS'] = array('url' => URL . 'home', 'icon' => "pe-7s-graph");


    }

    public function applyView($view, $header = "header", $footer = "footer") {
        foreach ($this->assigns as $key => $val) {
            global $$key;
            $$key = $val;
        }
        require APP . 'view/_templates/' . $header . '.php';
        require APP . 'view/' . $view . '.php';
        require APP . 'view/_templates/' . $footer . '.php';
    }

    public function applyAjaxView($view) {
        foreach ($this->assigns as $key => $val) {
            global $$key;
            $$key = $val;
        }
        require APP . 'view/' . $view . '.php';
    }

}
