<?php

class EmailBuilder {

    //Mail struct
    private $_body, $_header;
    //Mail META
    private $_separator, $_start_part, $_end_part;
    //Contents
    private $_dest, $_subject, $_message, $_sndr;
    //Arrays
    private $_attachments, $_copies;

    const EOL = "\r\n";

    public function __construct() {
        //Required, each messages need an unique id
        //else it'll be considered as spam.
        $this->_separator = md5(uniqid(rand(), true));

        $this->_start_part = self::EOL . "--" . $this->_separator . self::EOL;
        $this->_end_part = self::EOL . "--" . $this->_separator . "--" . self::EOL;

        $this->_body = "";
        $this->_header = "";

        $this->_attachments = array();
        $this->_copies = array();
    }

    public function setDestinataire($dest) {
        $this->_dest = $dest;
    }

    public function setSubject($subj) {
        $this->_subject = $subj;
    }

    public function setSender($sndr) {
        $this->_sndr = $sndr;
    }

    public function setMessage($msg) {
        $this->_message = $msg;
    }

    public function addAttachment($filename, $filesize) {
        array_push($this->_attachments, array('filename' => $filename, 'filesize' => $filesize));
    }

    public function addCopy($cp) {
        if (strpos($cp, ',') !== false) {
            $cps = explode(",", $cp);
            foreach ($cps as $mail) {
                array_push($this->_copies, $mail);
            }
        } else {
            array_push($this->_copies, $cp);
        }
    }

    private function assembleCopy() {
        $this->_header .= "Cc: ";

        foreach ($this->_copies as $copy) {
            $this->_header .= $copy . ", ";
        }

        rtrim($this->_header, ",");
        $this->_header .= self::EOL;
    }

    private function assembleAttachments() {
        foreach ($this->_attachments as $attachment) {
            //Get the file
            $file = "upload_cache/" . $attachment['filename'];
            $handle = fopen($file, "r");
            $content = fread($handle, $attachment['filesize']);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            //Construct the attachment
            $this->_body .= $this->_start_part;
            $this->_body .= "Content-Type: application/octet-stream; name=\"" . $attachment['filename'] . "\"" . self::EOL;
            $this->_body .= "Content-Transfer-Encoding: base64" . self::EOL;
            $this->_body .= "Content-Disposition: attachment; filename=\"" . $attachment['filename'] . "\"" . self::EOL;
            $this->_body .= self::EOL . $content . self::EOL . self::EOL;
        }
    }

    private function assembleBody() {
        $separator = md5(uniqid(rand(), true));
        $this->_body .= $this->_start_part;
        $this->_body .= "Content-Type: multipart/alternative;" . self::EOL . " boundary=\"" . $separator . "\"" . self::EOL;
        $this->_body .= self::EOL . "--" . $separator . self::EOL;
        $this->_body .= "Content-Type: text/plain; charset=\"ISO-8859-1\"" . self::EOL;
        $this->_body .= "Content-Transfer-Encoding: 8bit" . self::EOL;
        $this->_body .= self::EOL . $this->_message . self::EOL;

        $this->_body .= self::EOL . "--" . $separator . self::EOL;

        $this->_body .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . self::EOL;
        $this->_body .= "Content-Transfer-Encoding: 8bit" . self::EOL;
        $this->_body .= self::EOL . $this->_message . self::EOL;

        $this->_body .= self::EOL . "--" . $separator . "--" . self::EOL;
    }

    private function assemble() {
        $this->_header .= "From: <" . $this->_sndr . ">" . self::EOL;

        if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {
            $this->_header .= "Bcc: commande-clients@pinkpepper-studio.com" . self::EOL;
            $this->_dest = "debug@pinkpepper-studio.com";
        } else {
            if (!empty($this->_copies)) {
               $this->assembleCopy();
            }
            $this->_header .= "Bcc: commande-clients@pinkpepper-studio.com, contact@discoverfrance.com" . self::EOL;
        }

        $this->_header .= "MIME-Version: 1.0" . self::EOL;
        $this->_header .= "Content-Type: multipart/mixed;" . self::EOL . " boundary=\"" . $this->_separator . "\"" . self::EOL;

        $this->assembleBody();

        if (!empty($this->_attachments)) {
            $this->assembleAttachments();
        }

        $this->_body .= $this->_end_part;
    }

    public function send() {
        if (isset($this->_message) && isset($this->_dest) && isset($this->_sndr) && isset($this->_subject)) {
            $this->assemble();
            return mail($this->_dest, $this->_subject, $this->_body, $this->_header);
        }
        print_r("ERROR: Missing element !");
        return false;
    }

}
