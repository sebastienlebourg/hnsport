<?php

    /**
     * Description of User
     */
    class Produits extends modelMySql {

        const FAMILLE_MAITRE_NAGEUR = 1;
        const FAMILLE_ENTRETIEN = 2;
        const FAMILLE_TECHNIQUE = 3;
        const FAMILLE_COACH = 4;

        const DOTATION_INITIAL = array(
            self::FAMILLE_MAITRE_NAGEUR => ARRAY(
                'HOMME' => ARRAY(
                    '1' => 5,
                    '2' => 5,
                    '5' => 1,
                    '7' => 2,
                    '8' => 2,
                    '10' => 1,
                    '11' => 1,
                ),
                'FEMME' => ARRAY(
                    '3' => 5,
                    '4' => 5,
                    '6' => 1,
                    '7' => 2,
                    '9' => 2,
                    '10' => 1,
                    '11' => 1,
                )
            ),
            self::FAMILLE_ENTRETIEN => ARRAY(
                'HOMME' => ARRAY(
                    '12' => 1,
                    '14' => 5,
                    '16' => 2,
                    '17' => 1,
                ),
                'FEMME' => ARRAY(
                    '13' => 1,
                    '15' => 5,
                    '16' => 2,
                    '17' => 1,
                )
            ),
            self::FAMILLE_TECHNIQUE => ARRAY(
                'HOMME' => ARRAY(
                    '18' => 1,
                    '19' => 1,
                    '20' => 5,
                    '22' => 1,
                ),
                'FEMME' => ARRAY(
                    '18' => 1,
                    '19' => 1,
                    '21' => 5,
                    '22' => 1,
                )
            ),
            self::FAMILLE_COACH => ARRAY(
                'HOMME' => ARRAY(
                    '23' => 5,
                    '25' => 2,
                ),
                'FEMME' => ARRAY(
                    '24' => 5,
                    '25' => 2,
                )
            ),
        );

        

        public function __construct() {
            $this->name = 'produits';
            parent::__construct();
        }


        public function getAll() {

            if (empty($this->_arrAll)) {
                $sql = "SELECT * FROM `produits`";

                $query = $this->db_admin->prepare($sql);
                $query->execute();
                $this->_arrAll = $query->fetchAll();
            }

            return $this->_arrAll;
        }

        public function getById($id) {

            $sql = "SELECT * FROM produits WHERE id_produit = :id;";
            $query = $this->db_admin->prepare($sql);

            $query->execute(array(':id' => $id));

            return $query->fetch();
        }

}
