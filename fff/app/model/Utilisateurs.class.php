<?php

/**
 * Description of User
 */
class Utilisateurs extends modelMySql {

    public function __construct() {
        $this->name = 'utilisateurs';
        parent::__construct();
    }

    public function getAll() {

        if (empty($this->_arrAll)) {
            $sql = "SELECT * FROM `utilisateurs` AS `u` GROUP BY `u`.`utilisateur_id`;";

            $query = $this->db_admin->prepare($sql);
            $query->execute();
            $this->_arrAll = $query->fetchAll();
        }

        return $this->_arrAll;
    }

}
