<?php

    /**
     * Description of User
     */
    class Article extends modelMySql {

        public function __construct() {
            $this->name = 'articles';
            parent::__construct();
        }

        public function insert($id_article_commande, $piscine, $ref_commande, $ref_article, $designation, $taille, $quantite, $quantite_commandee) {
                $sql = "INSERT INTO `articles`(`id_article_commande`, `piscine`, `ref_commande`, `ref_article`, `designation`, `taille`, `quantite`, `quantite_commandee`, `date_commande`, `uid`) VALUES ('".$id_article_commande."', '".$piscine."', '".$ref_commande."', '".$ref_article."', '".$designation."', '".$taille."', '".$quantite."', '".$quantite_commandee."', '".date('Y-m-d H:i:s')."', '".$_SESSION['user_id']."')";

                $query = $this->db_admin->prepare($sql);
                $query->execute();

                return true;
        }

        public function getAll() {

            if (empty($this->_arrAll)) {
                $sql = "SELECT * FROM `articles`";

                $query = $this->db_admin->prepare($sql);
                $query->execute();
                $this->_arrAll = $query->fetchAll();
            }

            return $this->_arrAll;
        }

        public function getAllByRefCommande($ref_commande) {

            $sql = "SELECT * FROM `articles` WHERE ref_commande = '".$ref_commande."'";

            $query = $this->db_admin->prepare($sql);
            $query->execute();
            $this->_arrAll = $query->fetchAll();

            return $this->_arrAll;
        }

        public function hasCommandee($id_article_commande) {
            $result = $this->getAllFromLike('id_article_commande', $id_article_commande);
            
            if(!empty($result)){
                return true;
            }

            return false;
        }

}
