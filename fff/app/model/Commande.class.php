<?php

class Commande extends modelMySql {

    const COMMANDE_TYPE_ADRESSE_DEPOT = 'depot';
    const COMMANDE_TYPE_BIKE = 'bike';
    const COMMANDE_TYPE_BIKE_TYPE = 'bike_type';
    const COMMANDE_TYPE_GPS = 'gps';
    const COMMANDE_TYPE_GUIDE = 'guide';
    const COMMANDE_TYPE_TRANSPORT = 'transport';
    const COMMANDE_TYPE_VEHICULE = 'vehicule';
    const COMMANDE_TYPE_AUTRE = 'autre';
    const STATUS_CREATION = 2;
    const STATUS_SUPPRESSION = 9;
    const STATUS_DEVIS = 10;
    const STATUS_FACTURE = 20;
    const STATUS_ANNULEE = 99;
    
    function __construct() {
        $this->name = 'commande';
        parent::__construct();
    }

    public function addCommande($datas, $user, $valide = 1) {

        $now = date('Y/m/d H:i:s');
        $sql = "INSERT INTO commande (
                    devis_id,
                    responsable_id,
                    responsable_name,
                    responsable_firstname,
                    responsable_email,
                    leader_name,
                    client_type,
                    client_id,
                    client_name,
                    client_firstname,
                    client_entreprise,
                    client_tel,
                    client_email,
                    commande_date_create,
                    commande_date_update,
                    commande_status,
                    commande_montant_ht,
                    commande_montant_tva,
                    commande_montant_ttc,
                    commande_valide,
                    commande_urgente,
                    utilisateur_id,
                    valide_logistique,
                    key_import,
                    date_import_modification
                ) VALUES (
                    :devis_id,
                    :responsable_id,
                    :responsable_name,
                    :responsable_firstname,
                    :responsable_email,
                    :leader_name,
                    :client_type,
                    :client_id,
                    :client_name,
                    :client_firstname,
                    :client_entreprise,
                    :client_tel,
                    :client_email,
                    :commande_date_create,
                    :commande_date_update,
                    :commande_status,
                    :commande_montant_ht,
                    :commande_montant_tva,
                    :commande_montant_ttc,
                    :commande_valide,
                    :commande_urgente,
                    :utilisateur, 
                    :valide_logistique,
                    :key_import,
                    :date_import_modification
                )";
        $query = $this->db_admin->prepare($sql);

        $sql_devis = 'SELECT (config_value + 1) as devis_id FROM config WHERE `config_key` = "DEVIS"';
        $query_devis = $this->db_admin->prepare($sql_devis);
        $query_devis->execute();
        $arrInfoFacture = $query_devis->fetchAll();
        $devis_id = $arrInfoFacture[0]->devis_id;
        
        $sql_devis = "UPDATE config SET config_value  = :devis_id WHERE `config_key` = 'DEVIS'";
        $query_devis = $this->db_admin->prepare($sql_devis);
        $my_parameters = array(
            ':devis_id' => $devis_id
        );
        $query_devis->execute($my_parameters);

        $parameters = array(
            ':devis_id' => 'D' . sprintf("%09s", $devis_id),
            ':responsable_id' => $datas['responsable']['id_responsable'],
            ':responsable_name' => $datas['responsable']['responsable_nom'],
            ':responsable_firstname' => (!empty($datas['responsable']['responsable_prenom']) ? $datas['responsable']['responsable_prenom'] : $datas['responsable']['responsable_nom']),
            ':responsable_email' => $datas['responsable']['responsable_email'],
            ':leader_name' => $datas['leader_name'],
            ':client_type' => $datas['type_client'],
            ':client_id' => $datas['client']['id_client'],
            ':client_name' => $datas['client']['client_nom'],
            ':client_firstname' => (!empty($datas['client']['client_prenom']) ? $datas['client']['client_prenom'] : '-'),
            ':client_entreprise' => (!empty($datas['client']['client_entreprise']) ? $datas['client']['client_entreprise'] : ''),
            ':client_tel' => $datas['client']['client_tel'],
            ':client_email' => $datas['client']['client_email'],
            ':commande_date_create' => $now,
            ':commande_date_update' => $now,
            ':commande_status' => self::STATUS_CREATION,
            ':commande_montant_ht' => round($datas['total']['ht'],2),
            ':commande_montant_tva' => round($datas['total']['tva'],2),
            ':commande_montant_ttc' => round($datas['total']['ttc'],2),
            ':commande_valide' => $valide,
            ':commande_urgente' => $datas['commande_urgente'],
            ':utilisateur' => (!empty($user) ? $user : $datas['responsable']['id_responsable']),
            ':valide_logistique' => (!empty($datas['valide_logistique']) ? $datas['valide_logistique'] : 0),
            ':key_import' => (!empty($datas['key_import']) ? $datas['key_import'] : ''),
            ':date_import_modification' => (!empty($datas['date_import_modification']) ? $datas['date_import_modification'] : null)
        );

        $query->execute($parameters);
        $commande_id = $this->db_admin->lastInsertId();

        if(empty($commande_id)){
            var_dump($sql);
            var_dump($parameters);die;
        }
        
        EmptyException::dieOnEmpty($commande_id, 'Erreur modèle insertion commande');

        $bOk = false;
        if (!empty($datas['bike']['liste'])) {
            $bOk = $this->_addBikesCommande($commande_id, $datas['bike']);
            EmptyException::dieOnEmpty($bOk, 'Insertion bike !');
        }

        if (!empty($datas['bike_type']['liste'])) {
            $bOk = $this->_addBikesTypeCommande($commande_id, $datas['bike_type']);
            EmptyException::dieOnEmpty($bOk, 'Insertion bike type !');
        }

        if (!empty($datas['guide']['liste'])) {
            $bOk = $this->_addGuidesCommande($commande_id, $datas['guide']);
            EmptyException::dieOnEmpty($bOk, 'Insertion guide !');
        }

        if (!empty($datas['gps']['liste'])) {
            $bOk = $this->_addGPSCommande($commande_id, $datas['gps']);
            EmptyException::dieOnEmpty($bOk, 'Insertion gps !');
        }

        if (!empty($datas['vehicule']['liste'])) {
            $bOk = $this->_addVehiculesCommande($commande_id, $datas['vehicule']);
            EmptyException::dieOnEmpty($bOk, 'Insertion vehicule !');
        }

        if (!empty($datas['transport']['liste'])) {
            $bOk = $this->_addTransportsCommande($commande_id, $datas['transport']);
            EmptyException::dieOnEmpty($bOk, 'Insertion transport !');
        }

        if (!empty($datas['autre']['liste'])) {
            $bOk = $this->_addAutrePrestaCommande($commande_id, $datas['autre']);
            EmptyException::dieOnEmpty($bOk, 'Insertion commande vide !');
        }

        EmptyException::dieOnEmpty($bOk, 'Insertion commande vide !');

        $bOk = $this->updateCommandeStatus($commande_id, self::STATUS_DEVIS);
        EmptyException::dieOnEmpty($bOk, 'Problème à l\'activation mais la commande est bonne !');

        return $commande_id;
    }

    public function createFacture($commande_id) {

        $sql = 'SELECT (config_value + 1) as facture_id FROM config WHERE `config_key` = "FACTURE"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        $arrInfoFacture = $query->fetchAll();
        $facture_id = $arrInfoFacture[0]->facture_id;
        
        $sql = "UPDATE config SET config_value  = :facture_id WHERE `config_key` = 'FACTURE'";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':facture_id' => $facture_id
        );
        $query->execute($parameters);
        
        $sql = "UPDATE commande SET facture_id  = :facture_id, commande_status  = :commande_status, commande_valide = 1 WHERE commande_id = :commande_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':facture_id' => 'F' . sprintf("%09s", $facture_id),
            ':commande_status' => self::STATUS_FACTURE,
            ':commande_id' => $commande_id
        );
        $query->execute($parameters);

        /** RECUPERATION DU NUMERO DE FACTURE POUR LE RETOUR * */
        $sql = 'SELECT facture_id FROM commande WHERE commande_id = "' . $commande_id . '"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        return $result[0]->facture_id;
    }

    public function getAllDevis() {
        $sql = "SELECT `devis_id`, `commande_id`, `utilisateur_id`, `commande_date_create`, `commande_montant_ht`, `commande_montant_tva`, `commande_montant_ttc`, `client_type`, `commande_valide`, `valide_logistique`, CONCAT(`client_name`,' ',`client_firstname`, ' ' ,`client_entreprise`) AS `client` FROM `commande` WHERE `commande_status` = :status";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':status' => self::STATUS_DEVIS));

        return $query->fetchAll();
    }

    public function getAllCommandeAnnulee() {
        $sql = "SELECT `devis_id`, `commande_id`, `utilisateur_id`, `commande_date_create`, `commande_montant_ht`, `commande_montant_tva`, `commande_montant_ttc`, `client_type`, `commande_valide`, `valide_logistique`, CONCAT(`client_name`,' ',`client_firstname`, ' ' ,`client_entreprise`) AS `client` FROM `commande` WHERE `commande_status` = :status";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':status' => self::STATUS_ANNULEE));

        return $query->fetchAll();
    }

    public function getAllFactures() {
        $sql = "
                SELECT 
                    c.`facture_id`, 
                    c.`commande_id`, 
                    c.`utilisateur_id`, 
                    c.`commande_date_create`, 
                    c.`commande_montant_ht`, 
                    c.`commande_montant_tva`, 
                    c.`commande_montant_ttc`, 
                    c.`client_type`,
                    c.`commande_valide`, 
                    CONCAT(c.`client_name`,' ',c.`client_firstname`, ' ' ,c.`client_entreprise`) AS `client` ,
                    t.statut_payment
                FROM 
                    `commande` c
                    LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice)
                WHERE 
                    c.`commande_status` = :status
                GROUP BY c.`facture_id`";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':status' => self::STATUS_FACTURE));

        return $query->fetchAll();
    }

    public function getAllFacturesByClientEmail($mail) {
        EmptyException::dieOnEmpty($mail, 'Recherche de facture d\'un client sans e-mail');
        $sql = "SELECT `facture_id` FROM `commande` WHERE `client_email` = :mail AND `facture_id` != '' AND `facture_id` != ' ' AND `commande_status` != :status";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':mail' => $mail, ':status' => self::STATUS_CREATION));

        return $query->fetchAll();
    }

    public function getFacture($facture_id) {
        $sql = "SELECT * FROM commande WHERE (facture_id = :fid OR commande_id = :fid) AND `commande_status` != :status;";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':fid' => $facture_id, ':status' => self::STATUS_CREATION));

        return $query->fetch();
    }

    public function getDevis($devis_id) {
        $sql = "SELECT * FROM commande WHERE (devis_id = :did OR commande_id = :did) AND `commande_status` != :status;";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':did' => $devis_id, ':status' => self::STATUS_CREATION));

        return $query->fetch();
    }

    public function updateCommandeStatus($commande_id, $status) {
        $sql = "UPDATE commande SET commande_status  = :commande_status WHERE commande_id = :commande_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':commande_status' => $status,
            ':commande_id' => $commande_id
        );
        return $query->execute($parameters);
    }

    public function updateCommandeValideLogistique($commande_id) {
        $sql = "UPDATE commande SET valide_logistique = 1 WHERE commande_id = :commande_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':commande_id' => $commande_id
        );
        return $query->execute($parameters);
    }

    public function updateConfirmCommande($commande_id) {
        $sql = "UPDATE commande SET commande_valide = 1 WHERE commande_id = :commande_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(':commande_id' => $commande_id);
        return $query->execute($parameters);
    }

    private function _addBikesCommande($commande_id, $bikes = array()) {

        $pedals = new PedalsCyrpeo();
        $hotels = new HotelsCyrpeo();
        $bikesType = new BikeTypeCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($bikes['id_hotel_velo_livraison']);
        $hotel_velo_livraison = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_livraison_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($bikes['id_hotel_velo_enlevement']);
        $hotel_velo_enlevement = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_enlevement_id = $hotel[0]->_id;

        $start_date = date('Y/m/d', strtotime($bikes['start_date']));
        $end_date = date('Y/m/d', strtotime($bikes['end_date']));
        $notes_logistique = $bikes['notes_logistique'];
        $notes_facture = $bikes['notes_facture'];

        $result = false;

        foreach ($bikes['liste'] as $bike) {

            $size = !empty($bike['size_bike']) ? $bike['size_bike'] : 0;


            /** RECUPERATION DES PEDALS * */
            $pedal = $pedals->getPedalsById($bike['pedals']);
            /** RECUPERATION DU BIKE TYPE * */
            $bikeType = $bikesType->getBikeTypeById($bike['type_bike']);

            $bstid = $bikesType->getSubtypeByParams($bikeType[0]->bike_type_name, $bike['genre'], $size);
            EmptyException::dieOnEmpty($bstid, 'Combinaison de type vélo incorrecte !');

            $sql = "INSERT INTO commande_bike (
                        commande_id,
                        start_date,
                        end_date,
                        genre,
                        first_name,
                        last_name,
                        type_id,
                        type_name,
                        size,
                        pedals_id,
                        pedals_name,
                        options,
                        hotel_velo_livraison_id,
                        hotel_velo_livraison,
                        hotel_velo_enlevement_id,
                        hotel_velo_enlevement,
                        notes_logistique,
                        notes_facture,
                        prix,
                        remise,
                        prix_final,
                        key_import
                    ) VALUES (
                        :commande_id,
                        :start_date,
                        :end_date,
                        :genre,
                        :first_name,
                        :last_name,
                        :type_id,
                        :type_name,
                        :size,
                        :pedals_id,
                        :pedals_name,
                        :options,
                        :hotel_velo_livraison_id,
                        :hotel_velo_livraison,
                        :hotel_velo_enlevement_id,
                        :hotel_velo_enlevement,
                        :notes_logistique,
                        :notes_facture,
                        :prix,
                        :remise,
                        :prix_final,
                        :key_import
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_id' => $commande_id,
                ':start_date' => $start_date,
                ':end_date' => $end_date,
                ':genre' => !empty($bike['genre']) ? $bike['genre'] : '',
                ':first_name' => !empty($bike['first_name_bike']) ? $bike['first_name_bike'] : '',
                ':last_name' => !empty($bike['last_name_bike']) ? $bike['last_name_bike'] : '',
                ':type_id' => !empty($bikeType[0]->bike_type_id) ? $bikeType[0]->bike_type_id : '',
                ':type_name' => !empty($bikeType[0]->bike_type_name) ? $bikeType[0]->bike_type_name : '',
                ':size' => $size,
                ':pedals_id' => !empty($pedal[0]->pedals_id) ? $pedal[0]->pedals_id : '',
                ':pedals_name' => !empty($pedal[0]->pedals_name) ? $pedal[0]->pedals_name : '',
                ':options' => !empty($bike['options_bike']) ? implode(',', $bike['options_bike']) : '',
                ':hotel_velo_livraison_id' => !empty($hotel_velo_livraison_id) ? $hotel_velo_livraison_id : '',
                ':hotel_velo_livraison' => !empty($hotel_velo_livraison) ? $hotel_velo_livraison : '',
                ':hotel_velo_enlevement_id' => !empty($hotel_velo_enlevement_id) ? $hotel_velo_enlevement_id : '',
                ':hotel_velo_enlevement' => !empty($hotel_velo_enlevement) ? $hotel_velo_enlevement : '',
                ':notes_logistique' => !empty($notes_logistique) ? $notes_logistique : '',
                ':notes_facture' => !empty($notes_facture) ? $notes_facture : '',
                ':prix' => round($bike['prix_bike'],2),
                ':remise' => round($bike['remise_bike'],2),
                ':prix_final' => (!empty($bike['prix_pourcentage_bike']) ? round($bike['prix_pourcentage_bike'],2) : round(($bike['prix_bike'] - ($bike['remise_bike'] / 100)),2)),
                ':key_import' => (!empty($bikes['key_import']) ? $bikes['key_import'] : '')
            );

            $result = $query->execute($parameters);
            $commande_bike_id = $this->db_admin->lastInsertId();
            EmptyException::dieOnEmpty($result, 'Insertion bike !');

            if (!empty($bike['options_bike']) && count($bike['options_bike']) > 0) {
                $result = $this->_addOptionsBikeCommande($commande_id, $commande_bike_id, $bike['options_bike']);
                EmptyException::dieOnEmpty($result, 'Insertion bike option !');
            }
        }
        return $result;
    }

    private function _addOptionsBikeCommande($commande_id, $commande_bike_id, $options_bike = array()) {

        $options = new OptionsCyrpeo();
        $result = false;
        foreach ($options_bike as $my_option_bike) {

            /** RECUPERATION DE L'OPTION * */
            $option = $options->getOptionsByID($my_option_bike);
            $sql = "INSERT INTO commande_bike_option (
                        commande_bike_id,
                        option_id,
                        option_name
                    ) VALUES (
                        :commande_bike_id,
                        :option_id,
                        :option_name
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_bike_id' => $commande_bike_id,
                ':option_id' => (int) $option[0]->option_id,
                ':option_name' => $option[0]->option_name
            );

            $result = $query->execute($parameters);
            EmptyException::dieOnEmpty($result, 'Insertion bike option !');
        }
        return $result;
    }

    private function _addOptionsBikeTypeCommande($commande_id, $commande_bike_type_id, $options_bike = array()) {

        $options = new OptionsCyrpeo();
        $result = false;

        foreach ($options_bike as $my_option_bike) {

            /** RECUPERATION DE L'OPTION * */
            $option = $options->getOptionsByID($my_option_bike);
            $sql = "INSERT INTO commande_bike_type_option (
                        commande_bike_type_id,
                        option_id,
                        option_name
                    ) VALUES (
                        :commande_bike_type_id,
                        :option_id,
                        :option_name
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_bike_type_id' => $commande_bike_type_id,
                ':option_id' => (int) $option[0]->option_id,
                ':option_name' => $option[0]->option_name
            );

            $result = $query->execute($parameters);
            EmptyException::dieOnEmpty($result, 'Insertion bike type option !');
        }
        return $result;
    }

    private function _addBikesTypeCommande($commande_id, $bikes_type = array()) {
        $pedals = new PedalsCyrpeo();
        $hotels = new HotelsCyrpeo();
        $bikesType = new BikeTypeCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($bikes_type['id_hotel_velo_type_livraison']);
        $hotel_velo_livraison = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_livraison_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($bikes_type['id_hotel_velo_type_enlevement']);
        $hotel_velo_enlevement = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_enlevement_id = $hotel[0]->_id;

        $start_date = date('Y/m/d', strtotime($bikes_type['start_date']));
        $end_date = date('Y/m/d', strtotime($bikes_type['end_date']));

        $notes_logistique = $bikes_type['notes_logistique'];
        $notes_facture = $bikes_type['notes_facture'];

        $result = false;

        foreach ($bikes_type['liste'] as $bike) {
            /** RECUPERATION DES PEDALS * */
            $pedal = $pedals->getPedalsById($bike['pedals']);
            /** RECUPERATION DU BIKE TYPE * */
            $bikeType = $bikesType->getBikeTypeById($bike['type_bike']);

            $sql = "INSERT INTO commande_bike_type (
                        commande_id,
                        start_date,
                        end_date,
                        quantite,
                        type_id,
                        type_name,
                        pedals_id,
                        pedals_name,
                        options,
                        hotel_velo_livraison_id,
                        hotel_velo_livraison,
                        hotel_velo_enlevement_id,
                        hotel_velo_enlevement,
                        notes_logistique,
                        notes_facture,
                        prix,
                        remise,
                        prix_final
                    )"
                    . "VALUES (
                        :commande_id,
                        :start_date,
                        :end_date,
                        :quantite,
                        :type_id,
                        :type_name,
                        :pedals_id,
                        :pedals_name,
                        :options,
                        :hotel_velo_livraison_id,
                        :hotel_velo_livraison,
                        :hotel_velo_enlevement_id,
                        :hotel_velo_enlevement,
                        :notes_logistique,
                        :notes_facture,
                        :prix,
                        :remise,
                        :prix_final
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_id' => $commande_id,
                ':start_date' => $start_date,
                ':end_date' => $end_date,
                ':quantite' => $bike['quantite'],
                ':type_id' => $bikeType[0]->bike_type_id,
                ':type_name' => $bikeType[0]->bike_type_name,
                ':pedals_id' => $pedal[0]->pedals_id,
                ':pedals_name' => $pedal[0]->pedals_name,
                ':options' => !empty($bike['options_bike']) ? implode(',', $bike['options_bike']) : '',
                ':hotel_velo_livraison_id' => $hotel_velo_livraison_id,
                ':hotel_velo_livraison' => $hotel_velo_livraison,
                ':hotel_velo_enlevement_id' => $hotel_velo_enlevement_id,
                ':hotel_velo_enlevement' => $hotel_velo_enlevement,
                ':notes_logistique' => $notes_logistique,
                ':notes_facture' => $notes_facture,
                ':prix' => round($bike['prix_bike'],2),
                ':remise' => round($bike['remise_bike'],2),
                ':prix_final' => (!empty($bike['prix_pourcentage_bike']) ? round($bike['prix_pourcentage_bike'],2) : round(($bike['prix_bike'] - ($bike['remise_bike'] / 100)),2))
            );

            $result = $query->execute($parameters);
            $commande_bike_type_id = $this->db_admin->lastInsertId();
            EmptyException::dieOnEmpty($result, 'Insertion bike type !');

            if (!empty($bike['options_bike']) && count($bike['options_bike']) > 0) {
                $result = $this->_addOptionsBikeTypeCommande($commande_id, $commande_bike_type_id, $bike['options_bike']);
                EmptyException::dieOnEmpty($result, 'Insertion bike type option !');
            }
        }
        return $result;
    }

    private function _addGuidesCommande($commande_id, $guides = array()) {
        $hotels = new HotelsCyrpeo();
        $oGuide = new GuidesCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($guides['id_hotel_guide_accueil']);
        $hotel_guide_accueil = $hotel[0]->Nom . ' ' . (!empty($hotel[0]->Ville) ? $hotel[0]->Ville : '');
        $hotel_guide_accueil_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($guides['id_hotel_guide_depart']);
        $hotel_guide_depart = $hotel[0]->Nom . ' ' . (!empty($hotel[0]->Ville) ? $hotel[0]->Ville : '');
        $hotel_guide_depart_id = $hotel[0]->_id;

        $notes_logistique = $guides['notes_logistique'];
        $notes_facture = $guides['notes_facture'];

        $result = false;

        foreach ($guides['liste'] as $guide) {

            /** RECUPERATION DU GUIDE * */
            $myGuide = $oGuide->getGuideById($guide['guide']);
            $guide_id = $myGuide[0]->guide_id;
            $guide_name = $myGuide[0]->name;
            $guide_firstname = $myGuide[0]->firstname;

            $sql = "INSERT INTO commande_guide (
                        commande_id,
                        type_presta,
                        guide_id,
                        guide_name,
                        guide_firstname,
                        date_debut,
                        heure_debut,
                        date_fin,
                        heure_fin,
                        prix,
                        remise,
                        prix_final,
                        hotel_accueil_id,
                        hotel_accueil,
                        hotel_depart_id,
                        hotel_depart,
                        notes_logistique,
                        notes_facture,
                        key_import
                    ) VALUES (
                        :commande_id,
                        :type_presta,
                        :guide_id,
                        :guide_name,
                        :guide_firstname,
                        :date_debut,
                        :heure_debut,
                        :date_fin,
                        :heure_fin,
                        :prix,
                        :remise,
                        :prix_final,
                        :hotel_accueil_id,
                        :hotel_accueil,
                        :hotel_depart_id,
                        :hotel_depart,
                        :notes_logistique,
                        :notes_facture,
                        :key_import
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_id' => $commande_id,
                ':type_presta' => $guide['presta'],
                ':guide_id' => $guide_id,
                ':guide_name' => $guide_name,
                ':guide_firstname' => $guide_firstname,
                ':date_debut' => date('Y-m-d', strtotime($guide['date_debut'])),
                ':heure_debut' => $guide['heure_debut'],
                ':date_fin' => date('Y-m-d', strtotime($guide['date_fin'])),
                ':heure_fin' => $guide['heure_fin'],
                ':prix' => round($guide['prix'],2),
                ':remise' => round($guide['remise'],2),
                ':prix_final' => (!empty($guide['prix_pourcentage']) ? round($guide['prix_pourcentage'],2) : round(($guide['prix'] - ($guide['remise'] / 100)),2)),
                ':hotel_accueil_id' => $hotel_guide_accueil_id,
                ':hotel_accueil' => $hotel_guide_accueil,
                ':hotel_depart_id' => $hotel_guide_depart_id,
                ':hotel_depart' => $hotel_guide_depart,
                ':notes_logistique' => $notes_logistique,
                ':notes_facture' => $notes_facture,
                ':key_import' => (!empty($guide['key_import']) ? $guide['key_import'] : '')
            );

            $result = $query->execute($parameters);
            EmptyException::dieOnEmpty($result, 'Insertion guide !');
        }
        return $result;
    }

    private function _addGPSCommande($commande_id, $gps = array()) {
        $hotels = new HotelsCyrpeo();
        $oGPS = new GPSCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($gps['id_hotel_gps_livraison']);
        $hotel_gps_livraison = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_gps_livraison_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($gps['id_hotel_gps_recup']);
        $hotel_gps_recup = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_gps_recup_id = $hotel[0]->_id;

        $notes_logistique = $gps['notes_logistique'];
        $notes_facture = $gps['notes_facture'];

        $result = false;

        foreach ($gps['liste'] as $one_gps) {
            

            /** RECUPERATION DU GUIDE * */
            $myGPS = $oGPS->getGPSById($one_gps['type']);
            $one_gps_type_id = $myGPS->gps_type_id;
            $one_gps_type_name = $oGPS->getGPSTypeNameByID($myGPS->gps_type_id);

            $sql = "INSERT INTO commande_gps (
                        commande_id,
                        gps_id,
                        gps_name,
                        commentaire,
                        date_debut,
                        date_fin,
                        prix,
                        remise,
                        prix_final,
                        hotel_gps_livraison_id,
                        hotel_gps_livraison,
                        hotel_gps_recup_id,
                        hotel_gps_recup,
                        notes_logistique,
                        notes_facture,
                        key_import
                    ) VALUES (
                        :commande_id,
                        :gps_id,
                        :gps_name,
                        :commentaire,
                        :date_debut,
                        :date_fin,
                        :prix,
                        :remise,
                        :prix_final,
                        :hotel_gps_livraison_id,
                        :hotel_gps_livraison,
                        :hotel_gps_recup_id,
                        :hotel_gps_recup,
                        :notes_logistique,
                        :notes_facture,
                        :key_import
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_id' => $commande_id,
                ':gps_id' => $one_gps_type_id,
                ':gps_name' => $one_gps_type_name,
                ':commentaire' => $one_gps['commentaires'],
                ':date_debut' => date('Y/m/d', strtotime($one_gps['debut'])),
                ':date_fin' => date('Y/m/d', strtotime($one_gps['fin'])),
                ':prix' => round($one_gps['prix'],2),
                ':remise' => round($one_gps['remise'],2),
                ':prix_final' => (!empty($one_gps['prix_pourcentage']) ? round($one_gps['prix_pourcentage'],2) : round(($one_gps['prix'] - ($one_gps['remise'] / 100)),2)),
                ':hotel_gps_livraison_id' => $hotel_gps_livraison_id,
                ':hotel_gps_livraison' => $hotel_gps_livraison,
                ':hotel_gps_recup_id' => $hotel_gps_recup_id,
                ':hotel_gps_recup' => $hotel_gps_recup,
                ':notes_logistique' => $notes_logistique,
                ':notes_facture' => $notes_facture,
                ':key_import' => (!empty($one_gps['key_import']) ? $one_gps['key_import'] : '')
            );

            $result = $query->execute($parameters);
            EmptyException::dieOnEmpty($result, 'Insertion GPS !');
        }

        return $result;
    }

    private function _addVehiculesCommande($commande_id, $vehicules = array()) {
        $oVehicules = new VehiculesCyrpeo();
        $notes_logistique = $vehicules['notes_logistique'];
        $notes_facture = $vehicules['notes_facture'];

        $result = false;

        foreach ($vehicules['liste'] as $vehicule) {

            /** RECUPERATION DU VEHICULE * */
            $myVehicules = $oVehicules->getVehiculesById($vehicule['type']);
            $myVehicules_id = $myVehicules[0]->vehicule_id;
            $myVehicules_name = $myVehicules[0]->vehicule_name;

            $sql = "INSERT INTO commande_vehicule (
                        commande_id,
                        vehicule_id,
                        vehicule_name,
                        date_debut,
                        date_fin,
                        commentaire,
                        prix,
                        remise,
                        prix_final,
                        notes_logistique,
                        notes_facture,
                        key_import
                    ) VALUES (
                        :commande_id,
                        :vehicule_id,
                        :vehicule_name,
                        :date_debut,
                        :date_fin,
                        :commentaire,
                        :prix,
                        :remise,
                        :prix_final,
                        :notes_logistique,
                        :notes_facture,
                        :key_import
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_id' => $commande_id,
                ':vehicule_id' => $myVehicules_id,
                ':vehicule_name' => $myVehicules_name,
                ':date_debut' => date('Y/m/d', strtotime($vehicule['debut'])),
                ':date_fin' => date('Y/m/d', strtotime($vehicule['fin'])),
                ':commentaire' => $vehicule['commentaires'],
                ':prix' => round($vehicule['prix'],2),
                ':remise' => round($vehicule['remise'],2),
                ':prix_final' => (!empty($vehicule['prix_pourcentage']) ? round($vehicule['prix_pourcentage'],2) : round(($vehicule['prix'] - ($vehicule['remise'] / 100)),2)),
                ':notes_logistique' => $notes_logistique,
                ':notes_facture' => $notes_facture,
                ':key_import' => (!empty($vehicule['key_import']) ? $vehicule['key_import'] : '')
            );

            $result = $query->execute($parameters);
            EmptyException::dieOnEmpty($result, 'Insertion vehicule !');
        }
        return $result;
    }

    private function _addTransportsCommande($commande_id, $arrTransports = array()) {
        $oTransport = new TransportsCyrpeo();
        $oHotels = new HotelsCyrpeo();
        if (!empty($arrTransports['liste'])){ // Pour Toogo
            $arrTransports = array($arrTransports);
        }
        foreach ($arrTransports as $key_groupe => $transports) {
            if(empty($transports['liste'])){
                continue;
            }
            $notes_logistique = $transports['notes_logistique'];
            $notes_facture = $transports['notes_facture'];

            /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
            $adresse_livraison_id = $transports['adresse_livraison_id'];
            $hotel_livraison = $oHotels->getHotelsById($adresse_livraison_id);
            $lieu_livraison = $hotel_livraison[0]->Nom . ' ' . $hotel_livraison[0]->Ville;
            EmptyException::dieOnEmpty($hotel_livraison, 'Hotel livraison non trouvé transport !');

            /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
            $adresse_enlevement_id = $transports['adresse_enlevement_id'];
            $hotel_enlevement = $oHotels->getHotelsById($adresse_enlevement_id);
            $lieu_enlevement = $hotel_enlevement[0]->Nom . ' ' . $hotel_enlevement[0]->Ville;
            EmptyException::dieOnEmpty($hotel_enlevement, 'Hotel enlevement non trouvé transport !');

            $date_debut = date('Y-m-d', strtotime($transports['date_debut']));
            $heure_debut = $transports['heure_debut'];
            $date_fin = date('Y-m-d', strtotime($transports['date_fin']));
            $heure_fin = $transports['heure_fin'];

            $result = false;

            foreach ($transports['liste'] as $key => $transport) {
                
                if($key == "iteration_replace"){
                    continue;
                }
                
                $lieu_livraison_bis = null;
                $lieu_enlevement_bis = null;
                
                if (!empty($transport['adresse_livraison_id'])){
                    $hotel_livraison = $oHotels->getHotelsById($transport['adresse_livraison_id']);
                    $lieu_livraison_bis = $hotel_livraison[0]->Nom . ' ' . $hotel_livraison[0]->Ville;
                    EmptyException::dieOnEmpty($hotel_livraison, 'Hotel livraison non trouvé transport !');
                }
                
                if (!empty($transport['adresse_enlevement_id'])){
                    $hotel_enlevement = $oHotels->getHotelsById($transport['adresse_enlevement_id']);
                    $lieu_enlevement_bis = $hotel_enlevement[0]->Nom . ' ' . $hotel_enlevement[0]->Ville;
                    EmptyException::dieOnEmpty($hotel_enlevement, 'Hotel enlevement non trouvé transport !');
                }

                /** RECUPERATION DU VEHICULE * */
                $myTransport = $oTransport->getOneFromLike('transport_type_id', $transport['type']);

                $sql = 'INSERT INTO `commande_transport` (
                            `commande_id`,
                            `date_debut`,
                            `heure_debut`,
                            `date_fin`,
                            `heure_fin`,
                            `transport_type_id`,
                            `transport_type_nom`,
                            `quantite`,
                            `designation`,
                            `carton`,
                            `adresse_livraison_id`,
                            `adresse_livraison_nom`,
                            `adresse_enlevement_id`,
                            `adresse_enlevement_nom`,
                            `notes_logistique`,
                            `notes_facture`,
                            `assurance`,
                            `prix`,
                            `remise`,
                            `prix_final`,
                            key_import
                        ) VALUES (
                            :commande_id,
                            :date_debut,
                            :heure_debut,
                            :date_fin,
                            :heure_fin,
                            :transport_type_id,
                            :transport_type_nom,
                            :quantite,
                            :designation,
                            :carton,
                            :adresse_livraison_id,
                            :adresse_livraison_nom,
                            :adresse_enlevement_id,
                            :adresse_enlevement_nom,
                            :notes_logistique,
                            :notes_facture,
                            :assurance,
                            :prix,
                            :remise,
                            :prix_final,
                            :key_import
                        );';
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':commande_id' => $commande_id,
                    ':date_debut' => !empty($transport['date_debut']) ? $transport['date_debut'] : $heure_debut,
                    ':heure_debut' => !empty($transport['heure_debut']) ? $transport['heure_debut'] : $heure_debut,
                    ':date_fin' => !empty($transport['date_fin']) ? $transport['date_fin'] : $date_fin,
                    ':heure_fin' => !empty($transport['heure_fin']) ? $transport['heure_fin'] : $heure_fin,
                    ':transport_type_id' => $myTransport->transport_type_id,
                    ':transport_type_nom' => $myTransport->transport_type_nom,
                    ':quantite' => $transport['quantite'],
                    ':designation' => $transport['designation'],
                    ':carton' => $transport['carton'],
                    ':adresse_livraison_id' => !empty($transport['adresse_livraison_id']) ? $transport['adresse_livraison_id'] : $adresse_livraison_id,
                    ':adresse_livraison_nom' => !empty($lieu_livraison_bis) ? $lieu_livraison_bis : $lieu_livraison,
                    ':adresse_enlevement_id' => !empty($transport['adresse_enlevement_id']) ? $transport['adresse_enlevement_id'] : $adresse_enlevement_id,
                    ':adresse_enlevement_nom' => !empty($lieu_enlevement_bis) ? $lieu_enlevement_bis : $lieu_enlevement,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':assurance' => (!empty($transport['assurance']) ? $transport['assurance'] : 0 ),
                    ':prix' => round($transport['prix'],2),
                    ':remise' => round($transport['remise'],2),
                    ':prix_final' => (!empty($transport['prix_pourcentage']) ? round($transport['prix_pourcentage'],2) : round(($transport['prix'] - ($transport['remise'] / 100)),2)),
                    ':key_import' => (!empty($transport['key_import']) ? $transport['key_import'] : '')
                );

                $result = $query->execute($parameters);
                EmptyException::dieOnEmpty($result, 'Insertion transport !');
            }
        }
        return $result;
    }

    private function _addAutrePrestaCommande($commande_id, $autres_presta = array()) {
        $notes_logistique = $autres_presta['notes_logistique'];
        $notes_facture = $autres_presta['notes_facture'];

        $result = false;

        foreach ($autres_presta['liste'] as $autre_presta) {

            $sql = "INSERT INTO commande_autre_presta (
                        commande_id,
                        date_debut,
                        date_fin,
                        nom_presta,
                        commentaire,
                        prix,
                        remise,
                        prix_final,
                        notes_logistique,
                        notes_facture,
                        key_import
                    ) VALUES (
                        :commande_id,
                        :date_debut,
                        :date_fin,
                        :nom_presta,
                        :commentaire,
                        :prix,
                        :remise,
                        :prix_final,
                        :notes_logistique,
                        :notes_facture,
                        :key_import
                    )";
            $query = $this->db_admin->prepare($sql);
            $parameters = array(
                ':commande_id' => $commande_id,
                ':date_debut' => '0000/00/00'/* date('Y/m/d', strtotime($autre_presta['debut'])) */,
                ':date_fin' => '0000/00/00'/* date('Y/m/d', strtotime($autre_presta['fin'])) */,
                ':nom_presta' => $autre_presta['nom_presta'],
                ':commentaire' => $autre_presta['commentaires'],
                ':prix' => round($autre_presta['prix'],2),
                ':remise' => round($autre_presta['remise'],2),
                ':prix_final' => round($autre_presta['prix_pourcentage'],2),
                ':notes_logistique' => $notes_logistique,
                ':notes_facture' => $notes_facture,
                ':key_import' => (!empty($autre_presta['key_import']) ? $autre_presta['key_import'] : '')
            );

            $result = $query->execute($parameters);
            EmptyException::dieOnEmpty($result, 'Insertion autre presta !');
        }
        return $result;
    }

    public function getCommandeByFactureID($facture_id) {
        $sql = "SELECT * FROM commande WHERE facture_id = :fid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':fid' => $facture_id));

        $result = $query->fetch();

        return $result;
    }

    public function getDetailsCommande($commande_id) {

        $allDetails = array();
        $detailsBike = array();
        $detailsBikeType = array();
        $detailsGPS = array();
        $detailsGuide = array();
        $detailsVehicule = array();
        $detailsTransport = array();
        $detailsAutre = array();
        $detailsInfos = array();

        $detailsBike = $this->_getBikeCommande($commande_id);
        $detailsBikeType = $this->_getBikeTypeCommande($commande_id);
        $detailsGPS = $this->_getGPSCommande($commande_id);
        $detailsGuide = $this->_getGuideCommande($commande_id);
        $detailsVehicule = $this->_getVehiculeCommande($commande_id);
        $detailsTransport = $this->_getTransportCommande($commande_id);
        $detailsAutre = $this->_getAutreCommande($commande_id);
        $detailsInfos = $this->_getInfosCommande($commande_id);

        $allDetails['bike'] = $detailsBike;
        $allDetails['bike_type'] = $detailsBikeType;
        $allDetails['gps'] = $detailsGPS;
        $allDetails['guide'] = $detailsGuide;
        $allDetails['vehicule'] = $detailsVehicule;
        $allDetails['transport'] = $detailsTransport;
        $allDetails['autre'] = $detailsAutre;
        $allDetails['infos'] = $detailsInfos;

        return $allDetails;
    }

    public function _getBikeCommande($commande_id) {
        $sql = "SELECT * FROM commande_bike WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getBikeTypeCommande($commande_id) {

        $sql = "SELECT * FROM commande_bike_type WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getGPSCommande($commande_id) {

        $sql = "SELECT * FROM commande_gps WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getGuideCommande($commande_id) {

        $sql = "SELECT * FROM commande_guide WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getVehiculeCommande($commande_id) {

        $sql = "SELECT * FROM commande_vehicule WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getTransportCommande($commande_id) {

        $sql = "SELECT * FROM commande_transport WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getAutreCommande($commande_id) {

        $sql = "SELECT * FROM commande_autre_presta WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function _getInfosCommande($commande_id) {

        $sql = "SELECT * FROM commande WHERE commande_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_id));
        $result = $query->fetchAll();

        return $result;
    }

    public function getOptionsByCommandeBike($commande_bike_id, $options_bike = '') {

        $options = new OptionsCyrpeo();
        $result = array();
        
        $sql = "SELECT * FROM commande_bike_option WHERE commande_bike_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_bike_id));
        $options_bike = $query->fetchAll();
        
        foreach($options_bike as $option){
            $result[] = $option->option_name;
        }

        return $result;
    }

    public function getOptionsByCommandeBikeType($commande_bike_type_id, $options_bike = '') {

        
        $options = new OptionsCyrpeo();
        $result = array();
        
        $sql = "SELECT * FROM commande_bike_type_option WHERE commande_bike_type_id = :cid";
        $query = $this->db_admin->prepare($sql);

        $query->execute(array(':cid' => $commande_bike_type_id));
        $options_bike = $query->fetchAll();
        
        foreach($options_bike as $option){
            $result[] = $option->option_name;
        }

        return $result;
    }

    public function updateCommande($datas) {
        $now = date('Y/m/d H:i:s');
        $sql = "UPDATE commande SET
                    responsable_id = :responsable_id,
                    responsable_name = :responsable_name,
                    responsable_firstname = :responsable_firstname,
                    responsable_email = :responsable_email,
                    leader_name = :leader_name,
                    client_type = :client_type,
                    client_id = :client_id,
                    client_name = :client_name,
                    client_firstname = :client_firstname,
                    client_entreprise = :client_entreprise,
                    client_tel = :client_tel,
                    client_email = :client_email,
                    commande_date_update = :commande_date_update,
                    commande_montant_ht = :commande_montant_ht,
                    commande_montant_tva = :commande_montant_tva,
                    commande_montant_ttc = :commande_montant_ttc
                WHERE commande_id = :commande_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':responsable_id' => $datas['responsable']['id_responsable'],
            ':responsable_name' => $datas['responsable']['responsable_nom'],
            ':responsable_firstname' => $datas['responsable']['responsable_prenom'],
            ':responsable_email' => $datas['responsable']['responsable_email'],
            ':leader_name' => $datas['leader_name'],
            ':client_type' => $datas['type_client'],
            ':client_id' => $datas['client']['id_client'],
            ':client_name' => $datas['client']['client_nom'],
            ':client_firstname' => $datas['client']['client_prenom'],
            ':client_entreprise' => $datas['client']['client_entreprise'],
            ':client_tel' => $datas['client']['client_tel'],
            ':client_email' => $datas['client']['client_email'],
            ':commande_date_update' => $now,
            ':commande_montant_ht' => $datas['total']['ht'],
            ':commande_montant_tva' => $datas['total']['tva'],
            ':commande_montant_ttc' => $datas['total']['ttc'],
            ':commande_id' => $datas['commande_id']
        );

        $query->execute($parameters);
        $commande_id = $datas['commande_id'];

        if (!empty($datas['bike']['liste'])) $this->_updateBikesCommande($commande_id, $datas['bike']);

        if (!empty($datas['bike_type']['liste'])) $this->_updateBikesTypeCommande($commande_id, $datas['bike_type']);

        if (!empty($datas['guide']['liste'])) $this->_updateGuidesCommande($commande_id, $datas['guide']);

        if (!empty($datas['gps']['liste'])) $this->_updateGPSCommande($commande_id, $datas['gps']);

        if (!empty($datas['vehicule']['liste'])) $this->_updateVehiculesCommande($commande_id, $datas['vehicule']);

        if (!empty($datas['autre']['liste'])) $this->_updateAutrePrestaCommande($commande_id, $datas['autre']);

        return $commande_id;
    }

    private function _updateBikesCommande($commande_id, $bikes = array()) {

        $pedals = new PedalsCyrpeo();
        $hotels = new HotelsCyrpeo();
        $bikesType = new BikeTypeCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($bikes['id_hotel_velo_livraison']);
        $hotel_velo_livraison = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_livraison_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($bikes['id_hotel_velo_enlevement']);
        $hotel_velo_enlevement = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_enlevement_id = $hotel[0]->_id;

        $start_date = date('Y/m/d', strtotime($bikes['start_date']));
        $end_date = date('Y/m/d', strtotime($bikes['end_date']));
        $notes_logistique = $bikes['notes_logistique'];
        $notes_facture = $bikes['notes_facture'];

        $this->_deleteOptionsBikeCommande($commande_id, $commande_bike_id);

        foreach ($bikes['liste'] as $bike) {

            /** RECUPERATION DES PEDALS * */
            $pedal = $pedals->getPedalsById($bike['pedals']);
            /** RECUPERATION DU BIKE TYPE * */
            $bikeType = $bikesType->getBikeTypeById($bike['type_bike']);

            if (!empty($bike['my_id'])) {
                $sql = "UPDATE commande_bike SET
                            start_date = :start_date,
                            end_date = :end_date,
                            genre = :genre,
                            first_name = :first_name,
                            last_name = :last_name,
                            type_id = :type_id,
                            type_name = :type_name,
                            size = :size,
                            pedals_id = :pedals_id,
                            pedals_name = :pedals_name,
                            options = :options,
                            hotel_velo_livraison_id = :hotel_velo_livraison_id,
                            hotel_velo_livraison = :hotel_velo_livraison,
                            hotel_velo_enlevement_id = :hotel_velo_enlevement_id,
                            hotel_velo_enlevement = :hotel_velo_enlevement,
                            notes_logistique = :notes_logistique,
                            notes_facture = :notes_facture,
                            prix = :prix,
                            remise = :remise,
                            prix_final = :prix_final
                        WHERE commande_bike_id = :commande_bike_id AND commande_id = :commande_id";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':start_date' => $start_date,
                    ':end_date' => $end_date,
                    ':genre' => !empty($bike['genre']) ? $bike['genre'] : '',
                    ':first_name' => !empty($bike['first_name_bike']) ? $bike['first_name_bike'] : '',
                    ':last_name' => !empty($bike['last_name_bike']) ? $bike['last_name_bike'] : '',
                    ':type_id' => !empty($bikeType[0]->bike_type_id) ? $bikeType[0]->bike_type_id : '',
                    ':type_name' => !empty($bikeType[0]->bike_type_name) ? $bikeType[0]->bike_type_name : '',
                    ':size' => !empty($bike['size_bike']) ? $bike['size_bike'] : '',
                    ':pedals_id' => !empty($pedal[0]->pedals_id) ? $pedal[0]->pedals_id : '',
                    ':pedals_name' => !empty($pedal[0]->pedals_name) ? $pedal[0]->pedals_name : '',
                    ':options' => !empty($bike['options_bike']) ? implode(',', $bike['options_bike']) : '',
                    ':hotel_velo_livraison_id' => !empty($hotel_velo_livraison_id) ? $hotel_velo_livraison_id : '',
                    ':hotel_velo_livraison' => !empty($hotel_velo_livraison) ? $hotel_velo_livraison : '',
                    ':hotel_velo_enlevement_id' => !empty($hotel_velo_enlevement_id) ? $hotel_velo_enlevement_id : '',
                    ':hotel_velo_enlevement' => !empty($hotel_velo_enlevement) ? $hotel_velo_enlevement : '',
                    ':notes_logistique' => !empty($notes_logistique) ? $notes_logistique : '',
                    ':notes_facture' => !empty($notes_facture) ? $notes_facture : '',
                    ':prix' => $bike['prix_bike'],
                    ':remise' => $bike['remise_bike'],
                    ':prix_final' => (!empty($bike['prix_pourcentage_bike']) ? $bike['prix_pourcentage_bike'] : ($bike['prix_bike'] - ($bike['remise_bike'] / 100))),
                    ':commande_bike_id' => $bike['my_id'],
                    ':commande_id' => $commande_id
                );
            } else {
                $sql = "INSERT INTO commande_bike (
                        commande_id,
                        start_date,
                        end_date,
                        genre,
                        first_name,
                        last_name,
                        type_id,
                        type_name,
                        size,
                        pedals_id,
                        pedals_name,
                        options,
                        hotel_velo_livraison_id,
                        hotel_velo_livraison,
                        hotel_velo_enlevement_id,
                        hotel_velo_enlevement,
                        notes_logistique,
                        notes_facture,
                        prix,
                        remise,
                        prix_final
                    ) VALUES (
                        :commande_id,
                        :start_date,
                        :end_date,
                        :genre,
                        :first_name,
                        :last_name,
                        :type_id,
                        :type_name,
                        :size,
                        :pedals_id,
                        :pedals_name,
                        :options,
                        :hotel_velo_livraison_id,
                        :hotel_velo_livraison,
                        :hotel_velo_enlevement_id,
                        :hotel_velo_enlevement,
                        :notes_logistique,
                        :notes_facture,
                        :prix,
                        :remise,
                        :prix_final
                    )";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':start_date' => $start_date,
                    ':end_date' => $end_date,
                    ':genre' => !empty($bike['genre']) ? $bike['genre'] : '',
                    ':first_name' => !empty($bike['first_name_bike']) ? $bike['first_name_bike'] : '',
                    ':last_name' => !empty($bike['last_name_bike']) ? $bike['last_name_bike'] : '',
                    ':type_id' => !empty($bikeType[0]->bike_type_id) ? $bikeType[0]->bike_type_id : '',
                    ':type_name' => !empty($bikeType[0]->bike_type_name) ? $bikeType[0]->bike_type_name : '',
                    ':size' => !empty($bike['size_bike']) ? $bike['size_bike'] : '',
                    ':pedals_id' => !empty($pedal[0]->pedals_id) ? $pedal[0]->pedals_id : '',
                    ':pedals_name' => !empty($pedal[0]->pedals_name) ? $pedal[0]->pedals_name : '',
                    ':options' => !empty($bike['options_bike']) ? implode(',', $bike['options_bike']) : '',
                    ':hotel_velo_livraison_id' => !empty($hotel_velo_livraison_id) ? $hotel_velo_livraison_id : '',
                    ':hotel_velo_livraison' => !empty($hotel_velo_livraison) ? $hotel_velo_livraison : '',
                    ':hotel_velo_enlevement_id' => !empty($hotel_velo_enlevement_id) ? $hotel_velo_enlevement_id : '',
                    ':hotel_velo_enlevement' => !empty($hotel_velo_enlevement) ? $hotel_velo_enlevement : '',
                    ':notes_logistique' => !empty($notes_logistique) ? $notes_logistique : '',
                    ':notes_facture' => !empty($notes_facture) ? $notes_facture : '',
                    ':prix' => floatval($bike['prix_bike']),
                    ':remise' => floatval($bike['remise_bike']),
                    ':prix_final' => floatval(!empty($bike['prix_pourcentage_bike']) ? $bike['prix_pourcentage_bike'] : ($bike['prix_bike'] - ($bike['remise_bike'] / 100))),
                    ':commande_id' => $commande_id
                );
            }

            $query->execute($parameters);
            $commande_bike_id = $bike['my_id'];


            if (!empty($bike['options_bike']) && count($bike['options_bike']) > 0) {
                $this->_addOptionsBikeCommande($commande_id, $commande_bike_id, $bike['options_bike']);
            }
        }
    }

    private function _updateBikesTypeCommande($commande_id, $bikes_type = array()) {
        $pedals = new PedalsCyrpeo();
        $hotels = new HotelsCyrpeo();
        $bikesType = new BikeTypeCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($bikes_type['id_hotel_velo_type_livraison']);
        $hotel_velo_livraison = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_livraison_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($bikes_type['id_hotel_velo_type_enlevement']);
        $hotel_velo_enlevement = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_velo_enlevement_id = $hotel[0]->_id;

        $start_date = date('Y/m/d', strtotime($bikes_type['start_date']));
        $end_date = date('Y/m/d', strtotime($bikes_type['end_date']));
        $notes_logistique = $bikes_type['notes_logistique'];
        $notes_facture = $bikes_type['notes_facture'];

        $this->_deleteOptionsBikeTypeCommande($commande_id, $commande_bike_type_id);

        foreach ($bikes_type['liste'] as $bike) {
            /** RECUPERATION DES PEDALS * */
            $pedal = $pedals->getPedalsById($bike['pedals']);
            /** RECUPERATION DU BIKE TYPE * */
            $bikeType = $bikesType->getBikeTypeById($bike['type_bike']);

            if (!empty($bike['my_id'])) {
                $sql = "UPDATE commande_bike_type SET
                            start_date = :start_date,
                            end_date = :end_date,
                            quantite = :quantite,
                            type_id = :type_id,
                            type_name = :type_name,
                            pedals_id = :pedals_id,
                            pedals_name = :pedals_name,
                            options = :options,
                            hotel_velo_livraison_id = :hotel_velo_livraison_id,
                            hotel_velo_livraison = :hotel_velo_livraison,
                            hotel_velo_enlevement_id = :hotel_velo_enlevement_id,
                            hotel_velo_enlevement = :hotel_velo_enlevement,
                            notes_logistique = :notes_logistique,
                            notes_facture = :notes_facture,
                            prix = :prix,
                            remise = :remise,
                            prix_final = :prix_final
                        WHERE commande_bike_type_id = :commande_bike_type_id AND commande_id = :commande_id";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':start_date' => $start_date,
                    ':end_date' => $end_date,
                    ':quantite' => $bike['quantite'],
                    ':type_id' => $bikeType[0]->bike_type_id,
                    ':type_name' => $bikeType[0]->bike_type_name,
                    ':pedals_id' => $pedal[0]->pedals_id,
                    ':pedals_name' => $pedal[0]->pedals_name,
                    ':options' => !empty($bike['options_bike']) ? implode(',', $bike['options_bike']) : '',
                    ':hotel_velo_livraison_id' => $hotel_velo_livraison_id,
                    ':hotel_velo_livraison' => $hotel_velo_livraison,
                    ':hotel_velo_enlevement_id' => $hotel_velo_enlevement_id,
                    ':hotel_velo_enlevement' => $hotel_velo_enlevement,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':prix' => $bike['prix_bike'],
                    ':remise' => $bike['remise_bike'],
                    ':prix_final' => (!empty($bike['prix_pourcentage_bike']) ? $bike['prix_pourcentage_bike'] : ($bike['prix_bike'] - ($bike['remise_bike'] / 100))),
                    ':commande_bike_type_id' => $bike['my_id'],
                    ':commande_id' => $commande_id
                );
            } else {
                $sql = "INSERT INTO commande_bike_type (
                        commande_id,
                        start_date,
                        end_date,
                        quantite,
                        type_id,
                        type_name,
                        pedals_id,
                        pedals_name,
                        options,
                        hotel_velo_livraison_id,
                        hotel_velo_livraison,
                        hotel_velo_enlevement_id,
                        hotel_velo_enlevement,
                        notes_logistique,
                        notes_facture,
                        prix,
                        remise,
                        prix_final
                    )"
                        . "VALUES (
                        :commande_id,
                        :start_date,
                        :end_date,
                        :quantite,
                        :type_id,
                        :type_name,
                        :pedals_id,
                        :pedals_name,
                        :options,
                        :hotel_velo_livraison_id,
                        :hotel_velo_livraison,
                        :hotel_velo_enlevement_id,
                        :hotel_velo_enlevement,
                        :notes_logistique,
                        :notes_facture,
                        :prix,
                        :remise,
                        :prix_final
                    )";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':start_date' => $start_date,
                    ':end_date' => $end_date,
                    ':quantite' => $bike['quantite'],
                    ':type_id' => $bikeType[0]->bike_type_id,
                    ':type_name' => $bikeType[0]->bike_type_name,
                    ':pedals_id' => $pedal[0]->pedals_id,
                    ':pedals_name' => $pedal[0]->pedals_name,
                    ':options' => !empty($bike['options_bike']) ? implode(',', $bike['options_bike']) : '',
                    ':hotel_velo_livraison_id' => $hotel_velo_livraison_id,
                    ':hotel_velo_livraison' => $hotel_velo_livraison,
                    ':hotel_velo_enlevement_id' => $hotel_velo_enlevement_id,
                    ':hotel_velo_enlevement' => $hotel_velo_enlevement,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':prix' => $bike['prix_bike'],
                    ':remise' => $bike['remise_bike'],
                    ':prix_final' => (!empty($bike['prix_pourcentage_bike']) ? $bike['prix_pourcentage_bike'] : ($bike['prix_bike'] - ($bike['remise_bike'] / 100))),
                    ':commande_id' => $commande_id
                );
            }

            $query->execute($parameters);
            $commande_bike_type_id = $this->db_admin->lastInsertId();


            if (!empty($bike['options_bike']) && count($bike['options_bike']) > 0) {
                $this->_addOptionsBikeTypeCommande($commande_id, $commande_bike_type_id, $bike['options_bike']);
            }
        }
    }

    private function _updateGuidesCommande($commande_id, $guides = array()) {
        $hotels = new HotelsCyrpeo();
        $oGuide = new GuidesCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($guides['id_hotel_guide_accueil']);
        $hotel_guide_accueil = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_guide_accueil_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($guides['id_hotel_guide_depart']);
        $hotel_guide_depart = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_guide_depart_id = $hotel[0]->_id;

        $notes_logistique = $guides['notes_logistique'];
        $notes_facture = $guides['notes_facture'];

        foreach ($guides['liste'] as $guide) {

            /** RECUPERATION DU GUIDE * */
            $myGuide = $oGuide->getGuideById($guide['guide']);
            $guide_id = $myGuide[0]->guide_id;
            $guide_name = $myGuide[0]->name;
            $guide_firstname = $myGuide[0]->firstname;

            if (!empty($guide['my_id'])) {
                $sql = "UPDATE commande_guide SET
                            type_presta = :type_presta,
                            guide_id = :guide_id,
                            guide_name = :guide_name,
                            guide_firstname = :guide_firstname,
                            date_debut = :date_debut,
                            heure_debut = :heure_debut,
                            date_fin = :date_fin,
                            heure_fin = :heure_fin,
                            prix = :prix,
                            remise = :remise,
                            prix_final = :prix_final,
                            hotel_accueil_id = :hotel_accueil_id,
                            hotel_accueil = :hotel_accueil,
                            hotel_depart_id = :hotel_depart_id,
                            hotel_depart = :hotel_depart,
                            notes_logistique = :notes_logistique,
                            notes_facture = :notes_facture
                        WHERE commande_guide_id = :commande_guide_id AND commande_id = :commande_id";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':type_presta' => $guide['presta'],
                    ':guide_id' => $guide_id,
                    ':guide_name' => $guide_name,
                    ':guide_firstname' => $guide_firstname,
                    ':date_debut' => date('Y/m/d', strtotime($guide['date_debut'])),
                    ':heure_debut' => $guide['heure_debut'],
                    ':date_fin' => date('Y/m/d', strtotime($guide['date_fin'])),
                    ':heure_fin' => $guide['heure_fin'],
                    ':prix' => $guide['prix'],
                    ':remise' => $guide['remise'],
                    ':prix_final' => (!empty($guide['prix_pourcentage']) ? $guide['prix_pourcentage'] : ($guide['prix'] - ($guide['remise'] / 100))),
                    ':hotel_accueil_id' => $hotel_guide_accueil_id,
                    ':hotel_accueil' => $hotel_guide_accueil,
                    ':hotel_depart_id' => $hotel_guide_depart_id,
                    ':hotel_depart' => $hotel_guide_depart,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id,
                    ':commande_guide_id' => $guide['my_id']
                );
            } else {
                $sql = "INSERT INTO commande_guide (
                        commande_id,
                        type_presta,
                        guide_id,
                        guide_name,
                        guide_firstname,
                        date_debut,
                        heure_debut,
                        date_fin,
                        heure_fin,
                        prix,
                        remise,
                        prix_final,
                        hotel_accueil_id,
                        hotel_accueil,
                        hotel_depart_id,
                        hotel_depart,
                        notes_logistique,
                        notes_facture
                    ) VALUES (
                        :commande_id,
                        :type_presta,
                        :guide_id,
                        :guide_name,
                        :guide_firstname,
                        :date_debut,
                        :heure_debut,
                        :date_fin,
                        :heure_fin,
                        :prix,
                        :remise,
                        :prix_final,
                        :hotel_accueil_id,
                        :hotel_accueil,
                        :hotel_depart_id,
                        :hotel_depart,
                        :notes_logistique,
                        :notes_facture
                    )";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':type_presta' => $guide['presta'],
                    ':guide_id' => $guide_id,
                    ':guide_name' => $guide_name,
                    ':guide_firstname' => $guide_firstname,
                    ':date_debut' => date('Y/m/d', strtotime($guide['date_debut'])),
                    ':heure_debut' => $guide['heure_debut'],
                    ':date_fin' => date('Y/m/d', strtotime($guide['date_fin'])),
                    ':heure_fin' => $guide['heure_fin'],
                    ':prix' => $guide['prix'],
                    ':remise' => $guide['remise'],
                    ':prix_final' => (!empty($guide['prix_pourcentage']) ? $guide['prix_pourcentage'] : ($guide['prix'] - ($guide['remise'] / 100))),
                    ':hotel_accueil_id' => $hotel_guide_accueil_id,
                    ':hotel_accueil' => $hotel_guide_accueil,
                    ':hotel_depart_id' => $hotel_guide_depart_id,
                    ':hotel_depart' => $hotel_guide_depart,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id,
                );
            }

            $query->execute($parameters);
        }
    }

    private function _updateGPSCommande($commande_id, $gps = array()) {
        $hotels = new HotelsCyrpeo();
        $oGPS = new GPSCyrpeo();

        /** RECUPERATION DE L'HOTEL DE LIVRAISON * */
        $hotel = $hotels->getHotelsById($gps['id_hotel_gps_livraison']);
        $hotel_gps_livraison = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_gps_livraison_id = $hotel[0]->_id;

        /** RECUPERATION DE L'HOTEL D'ENLEVEMENT * */
        $hotel = $hotels->getHotelsById($gps['id_hotel_gps_recup']);
        $hotel_gps_recup = $hotel[0]->Nom . ' ' . $hotel[0]->Ville;
        $hotel_gps_recup_id = $hotel[0]->_id;

        $notes_logistique = $gps['notes_logistique'];
        $notes_facture = $gps['notes_facture'];

        foreach ($gps['liste'] as $one_gps) {

            /** RECUPERATION DU GUIDE * */
            $myGPS = $oGPS->getGPSById($one_gps['type']);
            $one_gps_type_id = $myGPS->gps_type_id;
            $one_gps_type_name = $myGPS->gps_type_name;

            if (!empty($one_gps['my_id'])) {
                $sql = "UPDATE commande_gps SET
                            gps_id = :gps_id,
                            gps_name = :gps_name,
                            commentaire = :commentaire,
                            date_debut = :date_debut,
                            date_fin = :date_fin,
                            prix = :prix,
                            remise = :remise,
                            prix_final = :prix_final,
                            hotel_gps_livraison_id = :hotel_gps_livraison_id,
                            hotel_gps_livraison = :hotel_gps_livraison,
                            hotel_gps_recup_id = :hotel_gps_recup_id,
                            hotel_gps_recup = :hotel_gps_recup,
                            notes_logistique = :notes_logistique,
                            notes_facture = :notes_facture
                        WHERE commande_gps_id = :commande_gps_id AND commande_id = :commande_id";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':gps_id' => $one_gps_type_id,
                    ':gps_name' => $one_gps_type_name,
                    ':commentaire' => $one_gps['commentaires'],
                    ':date_debut' => date('Y/m/d', strtotime($one_gps['debut'])),
                    ':date_fin' => date('Y/m/d', strtotime($one_gps['fin'])),
                    ':prix' => $one_gps['prix'],
                    ':remise' => $one_gps['remise'],
                    ':prix_final' => (!empty($one_gps['prix_pourcentage']) ? $one_gps['prix_pourcentage'] : ($one_gps['prix'] - ($one_gps['remise'] / 100))),
                    ':hotel_gps_livraison_id' => $hotel_gps_livraison_id,
                    ':hotel_gps_livraison' => $hotel_gps_livraison,
                    ':hotel_gps_recup_id' => $hotel_gps_recup_id,
                    ':hotel_gps_recup' => $hotel_gps_recup,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id,
                    ':commande_gps_id' => $one_gps['my_id']
                );
            } else {
                $sql = "INSERT INTO commande_gps (
                        commande_id,
                        gps_id,
                        gps_name,
                        commentaire,
                        date_debut,
                        date_fin,
                        prix,
                        remise,
                        prix_final,
                        hotel_gps_livraison_id,
                        hotel_gps_livraison,
                        hotel_gps_recup_id,
                        hotel_gps_recup,
                        notes_logistique,
                        notes_facture
                    ) VALUES (
                        :commande_id,
                        :gps_id,
                        :gps_name,
                        :commentaire,
                        :date_debut,
                        :date_fin,
                        :prix,
                        :remise,
                        :prix_final,
                        :hotel_gps_livraison_id,
                        :hotel_gps_livraison,
                        :hotel_gps_recup_id,
                        :hotel_gps_recup,
                        :notes_logistique,
                        :notes_facture
                    )";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':gps_id' => $one_gps_type_id,
                    ':gps_name' => $one_gps_type_name,
                    ':commentaire' => $one_gps['commentaires'],
                    ':date_debut' => date('Y/m/d', strtotime($one_gps['debut'])),
                    ':date_fin' => date('Y/m/d', strtotime($one_gps['fin'])),
                    ':prix' => $one_gps['prix'],
                    ':remise' => $one_gps['remise'],
                    ':prix_final' => (!empty($one_gps['prix_pourcentage']) ? $one_gps['prix_pourcentage'] : ($one_gps['prix'] - ($one_gps['remise'] / 100))),
                    ':hotel_gps_livraison_id' => $hotel_gps_livraison_id,
                    ':hotel_gps_livraison' => $hotel_gps_livraison,
                    ':hotel_gps_recup_id' => $hotel_gps_recup_id,
                    ':hotel_gps_recup' => $hotel_gps_recup,
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id
                );
            }

            $query->execute($parameters);
        }
    }

    private function _updateVehiculesCommande($commande_id, $vehicules = array()) {
        $oVehicules = new VehiculesCyrpeo();
        $notes_logistique = $vehicules['notes_logistique'];
        $notes_facture = $vehicules['notes_facture'];

        foreach ($vehicules['liste'] as $vehicule) {

            /** RECUPERATION DU VEHICULE * */
            $myVehicules = $oVehicules->getVehiculesById($vehicule['type']);
            $myVehicules_id = $myVehicules[0]->vehicule_id;
            $myVehicules_name = $myVehicules[0]->vehicule_name;

            if (!empty($vehicule['my_id'])) {
                $sql = "UPDATE commande_vehicule SET
                            vehicule_id = :vehicule_id,
                            vehicule_name = :vehicule_name,
                            date_debut = :date_debut,
                            date_fin = :date_fin,
                            commentaire = :commentaire,
                            prix = :prix,
                            remise = :remise,
                            prix_final = :prix_final,
                            notes_logistique = :notes_logistique,
                            notes_facture = :notes_facture
                        WHERE commande_vehicule_id = :commande_vehicule_id AND commande_id = :commande_id";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':vehicule_id' => $myVehicules_id,
                    ':vehicule_name' => $myVehicules_name,
                    ':date_debut' => date('Y/m/d', strtotime($vehicule['debut'])),
                    ':date_fin' => date('Y/m/d', strtotime($vehicule['fin'])),
                    ':commentaire' => $vehicule['commentaires'],
                    ':prix' => $vehicule['prix'],
                    ':remise' => $vehicule['remise'],
                    ':prix_final' => (!empty($vehicule['prix_pourcentage']) ? $vehicule['prix_pourcentage'] : ($vehicule['prix'] - ($vehicule['remise'] / 100))),
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id,
                    ':commande_vehicule_id' => $vehicule['my_id']
                );
            } else {
                $sql = "INSERT INTO commande_vehicule (
                        commande_id,
                        vehicule_id,
                        vehicule_name,
                        date_debut,
                        date_fin,
                        commentaire,
                        prix,
                        remise,
                        prix_final,
                        notes_logistique,
                        notes_facture
                    ) VALUES (
                        :commande_id,
                        :vehicule_id,
                        :vehicule_name,
                        :date_debut,
                        :date_fin,
                        :commentaire,
                        :prix,
                        :remise,
                        :prix_final,
                        :notes_logistique,
                        :notes_facture
                    )";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':vehicule_id' => $myVehicules_id,
                    ':vehicule_name' => $myVehicules_name,
                    ':date_debut' => date('Y/m/d', strtotime($vehicule['debut'])),
                    ':date_fin' => date('Y/m/d', strtotime($vehicule['fin'])),
                    ':commentaire' => $vehicule['commentaires'],
                    ':prix' => $vehicule['prix'],
                    ':remise' => $vehicule['remise'],
                    ':prix_final' => (!empty($vehicule['prix_pourcentage']) ? $vehicule['prix_pourcentage'] : ($vehicule['prix'] - ($vehicule['remise'] / 100))),
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id
                );
            }

            $query->execute($parameters);
        }
    }

    private function _updateAutrePrestaCommande($commande_id, $autres_presta = array()) {
        $notes_logistique = $autres_presta['notes_logistique'];
        $notes_facture = $autres_presta['notes_facture'];

        foreach ($autres_presta['liste'] as $autre_presta) {

            if (!empty($autre_presta['my_id'])) {
                $sql = "UPDATE commande_autre_presta SET
                            date_debut = :date_debut,
                            date_fin = :date_fin,
                            commentaire = :commentaire,
                            nom_presta = :nom_presta,
                            prix = :prix,
                            remise = :remise,
                            prix_final = :prix_final,
                            notes_logistique = :notes_logistique,
                            notes_facture = :notes_facture
                        WHERE commande_autre_presta_id = :commande_autre_presta_id AND commande_id = :commande_id";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':date_debut' => '0000/00/00'/* date('Y/m/d', strtotime($autre_presta['debut'])) */,
                    ':date_fin' => '0000/00/00'/* date('Y/m/d', strtotime($autre_presta['fin'])) */,
                    ':commentaire' => $autre_presta['commentaires'],
                    ':nom_presta' => $autre_presta['nom_presta'],
                    ':prix' => $autre_presta['prix'],
                    ':remise' => $autre_presta['remise'],
                    ':prix_final' => $autre_presta['prix_pourcentage'],
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id,
                    ':commande_autre_presta_id' => $autre_presta['my_id']
                );
            } else {
                $sql = "INSERT INTO commande_autre_presta (
                        commande_id,
                        date_debut,
                        date_fin,
                        nom_presta,
                        commentaire,
                        prix,
                        remise,
                        prix_final,
                        notes_logistique,
                        notes_facture
                    ) VALUES (
                        :commande_id,
                        :date_debut,
                        :date_fin,
                        :nom_presta,
                        :commentaire,
                        :prix,
                        :remise,
                        :prix_final,
                        :notes_logistique,
                        :notes_facture
                    )";
                $query = $this->db_admin->prepare($sql);
                $parameters = array(
                    ':date_debut' => '0000/00/00'/* date('Y/m/d', strtotime($autre_presta['debut'])) */,
                    ':date_fin' => '0000/00/00'/* date('Y/m/d', strtotime($autre_presta['fin'])) */,
                    ':commentaire' => $autre_presta['commentaires'],
                    ':nom_presta' => $autre_presta['nom_presta'],
                    ':prix' => $autre_presta['prix'],
                    ':remise' => $autre_presta['remise'],
                    ':prix_final' => $autre_presta['prix_pourcentage'],
                    ':notes_logistique' => $notes_logistique,
                    ':notes_facture' => $notes_facture,
                    ':commande_id' => $commande_id
                );
            }

            $query->execute($parameters);
        }
    }

    private function _deleteOptionsBikeCommande($commande_id, $commande_bike_id) {
        $sql = "DELETE FROM commande_bike_option WHERE commande_bike_id = :commande_bike_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':commande_bike_id' => $commande_bike_id
        );

        $query->execute($parameters);
    }

    private function _deleteOptionsBikeTypeCommande($commande_id, $commande_bike_type_id, $options_bike = array()) {
        $sql = "DELETE FROM commande_bike_type_option WHERE commande_bike_type_id = :commande_bike_type_id";
        $query = $this->db_admin->prepare($sql);
        $parameters = array(
            ':commande_bike_type_id' => $commande_bike_type_id
        );

        $query->execute($parameters);
    }

    public function getCommandeGuide($actuel = null) {
        $sql = 'SELECT * FROM (`commande_guide` b, commande c) LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) WHERE b.commande_id = c.commande_id';
        if($actuel == '1')
            $sql .= ' AND date_fin >= CURDATE()';
        elseif($actuel == '0')
            $sql .= ' AND date_fin < CURDATE()';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function getCommandeGPS($actuel = null) {
        $sql = 'SELECT * FROM (`commande_gps` b, commande c) LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) WHERE b.commande_id = c.commande_id';
        if($actuel == '1')
            $sql .= ' AND date_fin >= CURDATE()';
        elseif($actuel == '0')
            $sql .= ' AND date_fin < CURDATE()';
        
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function getCommandeBike($actuel = null) {
        $sql  = '   SELECT s.*, b.*, c.*, o.option_name, t.statut_transaction 
                    FROM (`commande_bike` b, commande c)  
                        LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) 
                        LEFT JOIN commande_bike_option o ON (b.commande_bike_id = o.commande_bike_id) 
                        LEFT JOIN bikes_subtypes s ON s.bike_subtype_id = ( 
                        SELECT `bike_subtype_id` FROM `bikes_subtypes` WHERE 
                            (`bike_subtype_genre` LIKE b.genre OR `bike_subtype_genre` LIKE "Uni") AND
                            `bike_type_id` = b.`type_id` AND
                            `bike_subtype_size` < b.size
                            ORDER BY `bike_subtype_size` DESC
                            LIMIT 1)
                    WHERE b.commande_id = c.commande_id ';
        
        if($actuel == '1')
            $sql .= 'AND end_date >= CURDATE()';
        elseif($actuel == '0')
            $sql .= 'AND end_date < CURDATE()';
            
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        $arrbike = $query->fetchAll();
                    
        $sql  = 'SELECT b.*, c.*, o.option_name, t.statut_transaction ';
        $sql .= 'FROM (`commande_bike_type` b, commande c)  ';
            $sql .= 'LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) ';
            $sql .= 'LEFT JOIN commande_bike_type_option o ON (b.commande_bike_type_id = o.commande_bike_type_id) ';
        $sql .= 'WHERE b.commande_id = c.commande_id ';
        
        if($actuel == '1')
            $sql .= 'AND end_date >= CURDATE()';
        elseif($actuel == '0')
            $sql .= 'AND end_date < CURDATE()';
        
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        $arrbiketype = $query->fetchAll();
        
        foreach($arrbiketype as $biketype)
            $arrbike[] = $biketype;
        
        return $arrbike;
    }

    public function getCommandeVehicule($actuel = null) {
        $sql = 'SELECT * FROM (`commande_vehicule` b, commande c) LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) WHERE b.commande_id = c.commande_id';
        if($actuel == '1')
            $sql .= ' AND date_fin >= CURDATE()';
        elseif($actuel == '0')
            $sql .= ' AND date_fin < CURDATE()';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function getCommandeTransport($actuel = null) {
        $sql = 'SELECT * FROM (`commande_transport` b, commande c) LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) WHERE b.commande_id = c.commande_id';
        if($actuel == '1')
            $sql .= ' AND date_fin >= CURDATE()';
        elseif($actuel == '0')
            $sql .= ' AND date_fin < CURDATE()';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function getCommandeAutrePresta($actuel = null) {
        $sql = 'SELECT * FROM (`commande_autre_presta` b, commande c) LEFT JOIN transaction_cyrpeo t ON (c.facture_id = t.num_invoice) WHERE b.commande_id = c.commande_id';
        
        if($actuel == '1')
            $sql .= ' AND date_fin >= CURDATE()';
        elseif($actuel == '0')
            $sql .= ' AND date_fin < CURDATE()';
        
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function updateCommandeGuidePrestataire($id, $prestataire) {
        $sql = 'UPDATE `commande_guide` SET `prestataire` = :prestataire WHERE `commande_guide_id` = :id';
        $query = $this->db_admin->prepare($sql);
        return $query->execute(array(':id' => $id, ':prestataire' => $prestataire));
    }

    public function editRefDepartBike($id, $refDepart) {
        $sql = 'UPDATE `commande_bike` SET `ref_depart` = :ref_depart WHERE `commande_bike_id` = :id';
        $query = $this->db_admin->prepare($sql);
        return $query->execute(array(':id' => $id, ':ref_depart' => $refDepart));
    }

    public function editRefRetourBike($id, $refRetour) {
        $sql = 'UPDATE `commande_bike` SET `ref_retour` = :ref_retour WHERE `commande_bike_id` = :id';
        $query = $this->db_admin->prepare($sql);
        return $query->execute(array(':id' => $id, ':ref_retour' => $refRetour));
    }
    
    

    public function getCommandeGuideById($commande_guide_id) {
        $sql = 'SELECT * FROM `commande_guide` WHERE commande_guide_id = "'.$commande_guide_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function getCommandeGPSById($commande_gps_id) {
        $sql = 'SELECT * FROM `commande_gps` WHERE commande_gps_id = "'.$commande_gps_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function getCommandeVehiculeById($commande_vehicule_id) {
        $sql = 'SELECT * FROM `commande_vehicule` WHERE commande_vehicule_id = "'.$commande_vehicule_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function getCommandeTransportById($commande_transport_id) {
        $sql = 'SELECT * FROM `commande_transport` WHERE commande_transport_id = "'.$commande_transport_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function getCommandeBikeById($commande_bike_id) {
        $sql = 'SELECT * FROM `commande_bike` WHERE commande_bike_id = "'.$commande_bike_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }
    
    public function getCommandeBikeTypeById($commande_bike_type_id) {
        $sql = 'SELECT * FROM `commande_bike_type` WHERE commande_bike_type_id = "'.$commande_bike_type_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function getCommandeAutreById($commande_autre_id) {
        $sql = 'SELECT * FROM `commande_autre_presta` WHERE commande_autre_presta_id = "'.$commande_autre_id.'"';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function getCommandeByKeyImport($key_import) {
        $sql = 'SELECT * FROM `commande` WHERE key_import = "'.$key_import.'" ORDER BY commande_id DESC LIMIT 1';
        $query = $this->db_admin->prepare($sql);
        $query->execute();
        return $query->fetch();
    }
    
    
    
    

}
