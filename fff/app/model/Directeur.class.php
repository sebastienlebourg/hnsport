<?php

/**
 * Description of User
 */
class Directeur extends modelMySql {

    public function __construct() {
        $this->name = 'directeur';
        parent::__construct();
    }

    public function getAll() {

        if (empty($this->_arrAll)) {
            $sql = "SELECT * FROM `directeur` AS `d` GROUP BY `d`.`id_directeur`;";

            $query = $this->db_admin->prepare($sql);
            $query->execute();
            $this->_arrAll = $query->fetchAll();
        }

        return $this->_arrAll;
    }

}
