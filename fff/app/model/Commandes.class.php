<?php

class Commandes extends modelMySql {
    
    const STATUT_COMMANDE_A_COMMANDER = "en_attente_preparation";
    const STATUT_COMMANDE_COMMANDE_FOURNISSEUR_PASSEE = "en_cours_preparation";
    const STATUT_COMMANDE_EXPEDIEE = "expediee";

    
    function __construct() {
        $this->name = 'commande';
        parent::__construct();
    }

    
    public function insert($idCommande, $directeur, $piscine, $total, $statut, $nbproduit) {
            $sql = "INSERT INTO `commande`(id_commande, `date`, `directeur`, `piscine`, `total`, `statut`, `nbproduit`) VALUES ('".$idCommande."', '".date('d/m/Y')."', '".$directeur."', '".$piscine."', '".$total."', '".$statut."', '".$nbproduit."')";

            $query = $this->db_admin->prepare($sql);
            $query->execute();

            return true;
    }


    public function addProduit($idCommande, $id_produit, $ref, $pu, $prix_total, $type, $qte, $taille, $salarie){
        $sql = "INSERT INTO `commande_produit`(`id_commande`, `id_produit`, `ref`, `pu`, `prix_total`, `type`, `qte`, `taille`, `salarie`) VALUES ('".$idCommande."', '".$id_produit."', '".$ref."', '".$pu."', '".$prix_total."', '".$type."', '".$qte."', '".$taille."', '".$salarie."')";

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return true;
    }


    public function deductEnveloppe($piscine, $enveloppe){
        $sql = "UPDATE `piscine` SET `enveloppe`='".$enveloppe."' WHERE id_piscine = '".$piscine."';";

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        $_SESSION['piscine']->enveloppe = $enveloppe;

        return true;
    }

    public function getCommandeByPiscineId($piscine){
        $sql = "SELECT * FROM commande WHERE piscine = '".$piscine."';";


        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getCommandeById($id){
        $sql = "SELECT * FROM commande WHERE id_commande = '".$id."';";


        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }


    public function getProduitCommandeByCommandeId($id_commande){
        $sql = "SELECT * FROM commande_produit cp, produits p WHERE cp.id_produit = p.id_produit AND cp.id_commande = '".$id_commande."';";


        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }


    public function getAllOrdersByStatus($statut){
        $sql = "SELECT * FROM commande c, piscine pi, directeur d WHERE c.piscine = pi.id_piscine AND c.directeur = d.id_directeur AND statut = '".$statut."';";

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }



    public function commandee($id_commande){
        $sql = "UPDATE `commande` SET `statut`='".self::STATUT_COMMANDE_COMMANDE_FOURNISSEUR_PASSEE."' WHERE id_commande = '".$id_commande."';";

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return true;
    }

    


    public function expediee($id_commande){
        $sql = "UPDATE `commande` SET `statut`='".self::STATUT_COMMANDE_EXPEDIEE."' WHERE id_commande = '".$id_commande."';";

        $query = $this->db_admin->prepare($sql);
        $query->execute();

        return true;
    }

    

    

}
