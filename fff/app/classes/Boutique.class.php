<?php


require_once('../app/libs/PSWebServiceLibrary.php');

class Boutique {

    public $shops;

    public function __construct(){

        try
        {
            $webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);

            // Here we set the option array for the Webservice : we want customers resources
            $resource = 'shop_urls';
            
            // Call
            $xml_shops = $webService->get(array('resource' => $resource));

            foreach($xml_shops->shop_urls->shop_url as $shop_url){
                $resource = 'shops';
                $xml_shop = $webService->get(array('resource' => $resource.'/'.$shop_url['id']));
                $name = (string) $xml_shop->shop->name;

                $resource = 'shop_urls';
                $xml_shop_url = $webService->get(array('resource' => $resource.'/'.$shop_url['id']));
                $this->shops[(string) $xml_shop_url->shop_url->id] = array(
                    'url' => 'http://'.(string) $xml_shop_url->shop_url->domain.(string) $xml_shop_url->shop_url->physical_uri.(string) $xml_shop_url->shop_url->virtual_uri,
                    'name' => $name,
                    'abv' => str_replace("/", "", strtoupper((string) $xml_shop_url->shop_url->virtual_uri))
                );

            }
        }
        catch (PrestaShopWebserviceException $e){
            // Here we are dealing with errors
            $trace = $e->getTrace();
            if ($trace[0]['args'][0] == 404) echo 'Bad ID';
            else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
            else echo 'Other error';
        }

    }

    public function getAll() {
        return $this->shops;
    }

    public function getAllOrdersByStatus($status) {

        // Here we make the WebService Call
        try
        {
            $webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
            $orders_url = array();

            foreach($this->shops as $id_boutique => $boutique){

                if($_SESSION['one_boutique'] != "all" && $boutique['abv'] != $_SESSION['one_boutique']){
                    continue;
                }
            
                $opt['url'] = $boutique['url'].'api/orders';
                $opt['filter[current_state]'] = '['.$status.']';

                $xml_orders = $webService->get($opt);

                foreach($xml_orders->orders->order as $order){
                    $opt['url'] = $boutique['url'].'api/orders/'.(string) $order['id'];
                    $orders_url[(string)$order['id']] = $opt['url'];

                }
            }

        }
        catch (PrestaShopWebserviceException $e)
        {
            // Here we are dealing with errors
            $trace = $e->getTrace();
            if ($trace[0]['args'][0] == 404) echo 'Bad ID';
            else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
            else echo 'Other error';
        }


        return $orders_url;
    }


    public function getAllOrdersByBoutique($url_boutique) {

        // Here we make the WebService Call
        try
        {
            $webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
            $orders_url = array();

            $opt['url'] = $url_boutique.'api/orders';

            $xml_orders = $webService->get($opt);

            foreach($xml_orders->orders->order as $order){
                $opt['url'] = $url_boutique.'api/orders/'.(string) $order['id'];
                $orders_url[(string)$order['id']] = $opt['url'];

            }

        }
        catch (PrestaShopWebserviceException $e)
        {
            // Here we are dealing with errors
            $trace = $e->getTrace();
            if ($trace[0]['args'][0] == 404) echo 'Bad ID';
            else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
            else echo 'Other error';
        }


        return $orders_url;
    }

}
